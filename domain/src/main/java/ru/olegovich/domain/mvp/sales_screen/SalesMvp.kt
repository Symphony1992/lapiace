package ru.olegovich.domain.mvp.sales_screen

import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.mvp.base.BaseMvp

interface SalesMvp {
    interface View : BaseMvp.BaseView {
        fun showSales(locationDatum: List<LocationDatum>)
        fun showError()
        fun reOpen()
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated()
        fun onTryAgain()
    }
}