package ru.olegovich.domain.mvp.sales_details_screen

import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.mvp.base.BaseMvp

interface SalesDetailsMvp {
    interface View : BaseMvp.BaseView {
        fun showImageSales(link: String)
        fun showDetailsSales(rendered: String)
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated(it: LocationDatum)
    }
}