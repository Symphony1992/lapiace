package ru.olegovich.domain.mvp.final_screen

import ru.olegovich.domain.data.entity.coordinates.ZonePath
import ru.olegovich.domain.data.entity.enums.DeliveryCounter
import ru.olegovich.domain.data.entity.enums.PaymentCounter
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.streets.Street
import ru.olegovich.domain.mvp.base.BaseMvp

interface FinalMvp {
    interface View : BaseMvp.BaseView {
        fun setVisibility(payment: DeliveryCounter)
        fun setVisibilityInclude(isVisible: Boolean)
        fun showFinalToast()
        fun onLatLngZoom(lat: Double, lng: Double, zoom: Float)
        fun showCoordinate(zonePath: ZonePath, zone_color: Boolean)
        fun showMenuList(menu: List<Menu>)
        fun showTotalSum(sum: Int)
        fun showCity(cities: List<String>, pos: Int)
        fun setStreets(streets: List<Street>)
        fun showLatLngResult(lat: Double, lng: Double)
        fun backToCart()
        fun showErrorFindToast()
        fun showErrorZoneText()
        fun setVisibilityError()
        fun showErrorNoZoneText()
        fun onChooseTime()
        fun onChooseDate()
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated(menu: List<Menu>)
        fun paymentChoose(c: PaymentCounter)
        fun deliveryChoose(d: DeliveryCounter)
        fun applyButtonClick(): Unit?
        fun onCityChosen(town: String)
        fun findAddress(str: String, num: String)
        fun backButtonClick(): Unit?
        fun yellowZoneSumCheck()
        fun greenZoneSumCheck()
        fun noZoneChosen(): Unit?
        fun onTimeTextClick(): Unit?
        fun onDateTextClick(): Unit?
        fun onCallMeCheckedChange(isChecked: Boolean)
    }
}