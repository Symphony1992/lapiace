package ru.olegovich.domain.mvp.add_modifier

import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.mvp.base.BaseMvp

interface AddModifierMvp {

    interface View : BaseMvp.BaseView {
        fun showModifier(modifierList: List<Modifier>)
        fun setFragmentResult(newItem: List<Modifier>)
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated(modifiers: List<Modifier>)
        fun onItemClick(item: Modifier)
        fun onStop()
    }

}