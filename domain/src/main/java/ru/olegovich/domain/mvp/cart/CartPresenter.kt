package ru.olegovich.domain.mvp.cart

import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class CartPresenter : BasePresenterImpl<CartMvp.View>(), CartMvp.Presenter {

    private var menu = mutableListOf<Menu>()

    override fun menuListMenu(menu: List<Menu>) {
        this.menu.clear()
        this.menu.addAll(menu)
        view?.showMenuList(menu.toMutableList())
        view?.isIncludeCartVisible = menu.isNotEmpty()
        view?.showSum(sum)
    }

    override fun onDeleteMenuItem(item: Menu, position: Int) {
        menu.removeAt(position)
        view?.isIncludeCartVisible = menu.isNotEmpty()
        view?.showSum(sum)
    }

    override fun onBackClick() { view?.onBackToMain(menu) }

    override fun onBackImageButtonClick() { view?.goToMainActivity() }

    override fun onBuyButtonClick() { 
        if (menu.isNotEmpty()) view?.startFinalActivity(menu)
        else view?.showOpenErrorToast()
    }

    override fun onChangeCounter(item: Menu, isAdd: Boolean, position: Int) {
        menu[position].counter = if (isAdd) ++item.counter else --item.counter
        if (menu[position].counter == 0) onDeleteMenuItem(item, position)
        view?.changeItem(menu[position])
        view?.showSum(sum)
    }

    private val sum: Int get()  {
        var sum = 0
        menu.forEach { sum += it.counter * it.metaData.price + it.modifiers.additionsSum }
        return sum
    }

    private val List<Modifier>.additionsSum: Int get()  {
        var sum = 0
        forEach { sum += it.metaData.price }
        return sum
    }

}