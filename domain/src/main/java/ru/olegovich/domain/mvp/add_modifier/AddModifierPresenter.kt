package ru.olegovich.domain.mvp.add_modifier

import ru.olegovich.domain.data.backend.Api
import ru.olegovich.domain.data.entity.enums.FRAGMENT_TO_PAGER
import ru.olegovich.domain.data.entity.enums.MODIFIER_ERROR
import ru.olegovich.domain.data.entity.enums.STATUS_OF_VIEW
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class AddModifierPresenter : BasePresenterImpl<AddModifierMvp.View>(), AddModifierMvp.Presenter {

    private var newItem = mutableListOf<Modifier>()
    private val repository by lazy { dataRepository.modifier }

    override fun onCreated(modifiers: List<Modifier>) {
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, ADD_MODIFIER)
        newItem = modifiers.toMutableList()
//        modifiers?.let { newItem = it }
//        if (modifiers != null){
//            newItem = modifiers
//        }
        sendApiModifierRequest()
    }

    private fun sendApiModifierRequest() {
        repository
                .load()
                .observeOn(schedulers.ui)
                .subscribe(::sortAndShowResult, ::onError)
    }

    private fun sortAndShowResult(result: List<Modifier>) {
        result.forEach { if (isSelected(it.id)) it.isSelected = true }
        view?.showModifier(result)
    }

    private fun onError(t: Throwable) { println(MODIFIER_ERROR + t.message) }

    override fun onItemClick(item: Modifier) {
        when {
            !isSelected(item.id) -> {
                item.isSelected = true
                newItem.add(item)
            }
            else -> newItem.filter { !it.equals(item.id) }
        }
    }

    private fun isSelected(id: Int): Boolean {
        newItem.forEach { if (it.id == id) return true }
        return false
    }

    override fun onStop() { view?.setFragmentResult(newItem) }

}

