package ru.olegovich.domain.mvp.splash_screen

import ru.olegovich.domain.data.entity.enums.City.*
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class SplashPresenter : BasePresenterImpl<SplashMvp.View>(), SplashMvp.Presenter {

    override fun onCityChecked() {
        if (preference.city == NOT_SELECTED) view?.goToCityActivity()
        else view?.goToMainActivity()
    }
}