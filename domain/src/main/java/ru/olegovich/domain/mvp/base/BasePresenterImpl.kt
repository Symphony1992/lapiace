package ru.olegovich.domain.mvp.base

import ru.olegovich.domain.component.ComponentProvider
import java.lang.ref.WeakReference

abstract class BasePresenterImpl<V :  BaseMvp.BaseView> : BaseMvp.BasePresenter<V> {

    override val view: V? get() = viewRef?.get()
    private var viewRef: WeakReference<V>? = null
    private val componentProvider by lazy { ComponentProvider.instance }
    protected val preference by lazy { componentProvider.preference }
    protected val schedulers by lazy { componentProvider.schedulers }
    protected val dataRepository by lazy { componentProvider.dataRepository }

    override fun attachView(view: V) {
        viewRef = WeakReference(view)
    }

    override fun detachView() {
        viewRef?.clear()
    }
}