package ru.olegovich.domain.mvp.info_screen

import ru.olegovich.domain.data.entity.enums.FRAGMENT_TO_PAGER
import ru.olegovich.domain.data.entity.enums.STATUS_OF_VIEW
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class InfoPresenter : BasePresenterImpl<InfoMvp.View>(), InfoMvp.Presenter {
    override fun onCreated() {
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, INFO)
    }

    override fun onAboutUsCardViewClick() { view?.startAboutUsFragment() }

    override fun onContactsCardViewClick() { view?.startContactsFragment()}

    override fun onVacancyCardViewClick() { view?.startVacancyFragment() }

    override fun onPartnersCardViewClick() { view?.startPartnersFragment() }

    override fun onDeliveryCardViewClick() { view?.startDeliveryFragment() }
}