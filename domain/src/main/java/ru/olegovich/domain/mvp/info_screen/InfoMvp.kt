package ru.olegovich.domain.mvp.info_screen

import ru.olegovich.domain.mvp.base.BaseMvp

interface InfoMvp {
    interface View : BaseMvp.BaseView {
        fun startAboutUsFragment()
        fun startContactsFragment()
        fun startVacancyFragment()
        fun startPartnersFragment()
        fun startDeliveryFragment()
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated()
        fun onAboutUsCardViewClick()
        fun onContactsCardViewClick()
        fun onVacancyCardViewClick()
        fun onPartnersCardViewClick()
        fun onDeliveryCardViewClick()
    }
}