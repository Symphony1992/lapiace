package ru.olegovich.domain.mvp.city

import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.data.entity.enums.City.*
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class CityPresenter : BasePresenterImpl<CityMvp.View>(), CityMvp.Presenter {
    override fun onCitySelect(city: City) {
        preference.city = city
    }

    override fun onCityApplyClick() {
        if (preference.city == NOT_SELECTED) view?.showSelectedToast()
        else view?.goToMainActivity()
    }

    override fun onCreated() {
        view?.initRadioGroup(preference.city)
    }
}