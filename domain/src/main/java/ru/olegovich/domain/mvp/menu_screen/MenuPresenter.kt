package ru.olegovich.domain.mvp.menu_screen

import ru.olegovich.domain.data.entity.enums.FRAGMENT_TO_PAGER
import ru.olegovich.domain.data.entity.enums.STATUS_OF_VIEW
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class MenuPresenter : BasePresenterImpl<MenuMvp.View>(), MenuMvp.Presenter {
    override fun onCreated() {
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, MENU)
    }

    override fun onMenuPizzaImageViewClick() {
        view?.startPizzaFragment()
    }

    override fun onSaladPizzaImageViewClick() {
        view?.startSaladFragment()
    }

    override fun onDrinkPizzaImageViewClick() {
        view?.startDrinkFragment()
    }
}