package ru.olegovich.domain.mvp.main

import ru.olegovich.domain.data.entity.enums.Status
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BasePresenterImpl
import java.util.concurrent.atomic.AtomicBoolean

class MainPresenter : BasePresenterImpl<MainMvp.View>(), MainMvp.Presenter {

    val menu = mutableListOf<Menu>()

    override fun onCreated() {
        view?.initButton(preference.city)
        view?.animationQuantityCart()
    }

    override fun onStatusResult(status: Status, titleCart: Menu) {
        if (status == PIZZA) view?.onActivityResultToPizzaFragment(menu)
        view?.setVisibility(status, titleCart)
    }

    override fun menuList(menu: List<Menu>) {
        this.menu.clear()
        this.menu.addAll(menu)
        view?.onActivityResultToPizzaFragment(menu)
        calculateQuantity()
        calculateTotalSum()
    }

    override fun onResult(menuItem: Menu) {
        if (isSelectedPizza(menuItem.id)) checkPizza(menuItem)
        else menu.add(menuItem)
        calculateQuantity()
        calculateTotalSum()
    }

    override fun onCartButtonClick() { view?.showUpdatedList(menu) }

    override fun onCityButtonClick() { view?.onGoToCityActivity() }

    override fun onBackButtonClick() { view?.back() }

    private fun checkPizza(item: Menu) {
        if (item.counter == -1) removePizza(item)
        else addPizza(item)
    }

    private fun addPizza(item: Menu) {
        val isAddByModifierResult = AtomicBoolean(false)
        menu.forEach { if (it.id.equals(item.id) && checkModifier(it, item)) {
            isAddByModifierResult.set(true)
        }
        }
        if (!isAddByModifierResult.get()) menu.add(item)
    }

    private fun checkModifier(it: Menu, item: Menu): Boolean {
        if (it.modifiers.size == item.modifiers.size){
            val isEqualModifiers = AtomicBoolean(true)
            it.modifiers.forEachIndexed { index, modifier ->
                if (modifier.id.equals(item.modifiers[index].id)) {
                    isEqualModifiers.set(false)
                }
            }
            if (isEqualModifiers.get()) it.counter = it.counter + item.counter
            return isEqualModifiers.get()
        }
        return false
    }

    private fun removePizza(item: Menu) {
        menu.reverse()
        val listIterator = menu.listIterator()
        while (listIterator.hasNext()){
            val it = listIterator.next()
            if (it.id.equals(item.id)){
                if (it.counter -1 != 0) it.counter = it.counter - 1
                else listIterator.remove()
            }
        }
        menu.reverse()
    }

    private fun isSelectedPizza(id: Int): Boolean {
        menu.forEach { if (it.id == id) return true }
        return false
    }

    private fun calculateQuantity() {
        var quantity = 0
        menu.forEach { quantity += it.counter }
        view?.showAnimationCart(quantity)
    }

    private fun calculateTotalSum() {
        var sum = 0
        menu.forEach { sum += (it.counter*it.metaData.price) + modifiersSum(it.modifiers) }
        view?.showTotalSumCart(sum)
    }

    private fun modifiersSum(modifiers: MutableList<Modifier>): Int {
        var sum = 0
        modifiers.forEach { sum += it.metaData.price }
        return sum
    }

}