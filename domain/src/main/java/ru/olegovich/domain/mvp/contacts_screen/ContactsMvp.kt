package ru.olegovich.domain.mvp.contacts_screen

import ru.olegovich.domain.mvp.base.BaseMvp.BasePresenter
import ru.olegovich.domain.mvp.base.BaseMvp.BaseView

interface ContactsMvp {
    interface View : BaseView {
        fun startCallActivity()
        fun startMapActivity()
        fun startEmailActivity()
        fun startFacebookActivity()
        fun startInstagramActivity()
    }

    interface Presenter : BasePresenter<View> {
        fun onCreated()
    }
}