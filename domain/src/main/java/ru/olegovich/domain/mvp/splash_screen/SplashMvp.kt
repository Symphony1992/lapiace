package ru.olegovich.domain.mvp.splash_screen

import ru.olegovich.domain.mvp.base.BaseMvp

interface SplashMvp {
    interface View : BaseMvp.BaseView {
        fun goToCityActivity()
        fun goToMainActivity()
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCityChecked()
    }
}