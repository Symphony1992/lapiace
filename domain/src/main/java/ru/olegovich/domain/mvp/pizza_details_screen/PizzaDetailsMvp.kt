package ru.olegovich.domain.mvp.pizza_details_screen

import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BaseMvp

interface PizzaDetailsMvp {
    interface View : BaseMvp.BaseView {
        fun showPizzaComponent(words: Array<String>)
        fun showPizzaTitle(title: String)
        fun showPizzaWidth(width: String)
        fun showModifierAdapter(modifierItemTransfer: MutableList<Modifier>)
        fun showPizzaWeight(weight: String)
        fun showPizzaImage(pizzaImageLink: String)
        fun showAddModifierFragment(modifiers: List<Modifier>)
        fun showModifierSum(sum: Int)
        fun showTotalSum(sum: Int)
        fun removeItem(item: Modifier, position: Int)
        fun addMenuToCart(item: Menu)
        fun showCounter(counter: Int)
        fun setChosenAdditionsView(vis: Boolean)
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated(menuItem: Menu)
        fun onResult(modifierItemTransfer: List<Modifier>)
        fun onAddModifierButtonClick()
        val modifierSum: Int
        fun onItemClick(item: Modifier, position: Int)
        fun onAddToCartButtonClick()
        fun onMinusButtonClick()
        fun onPlusButtonClick()
    }
}
