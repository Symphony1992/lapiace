package ru.olegovich.domain.mvp.vacancy

import ru.olegovich.domain.data.entity.vacancies.Vacancy
import ru.olegovich.domain.mvp.base.BaseMvp

interface VacancyMvp {
    interface View : BaseMvp.BaseView {
        fun showVacancy(vacancyList: List<Vacancy>)
        fun scrollToVacancy()
        fun showError()
        fun reOpen()
        fun sendingStatusOfView()
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated()
        fun onMoveToVacancyClick()
        fun onTryAgainClick()
    }
}