package ru.olegovich.domain.mvp.delivery_screen

import ru.olegovich.domain.data.backend.Api
import ru.olegovich.domain.data.entity.enums.City.*
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.data.entity.coordinates.Delivery
import ru.olegovich.domain.data.entity.enums.COORDINATES_ERROR
import ru.olegovich.domain.data.entity.enums.FRAGMENT_TO_PAGER
import ru.olegovich.domain.data.entity.enums.PLACES_ERROR
import ru.olegovich.domain.data.entity.enums.STATUS_OF_VIEW
import ru.olegovich.domain.data.entity.places.Places
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class DeliveryPresenter : BasePresenterImpl<DeliveryMvp.View>(), DeliveryMvp.Presenter {
    override fun onCreated() {
        setLatLngCurrent()
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, DELIVERY)
    }

    private fun setLatLngCurrent() {
        var city = ""
        var lat = 0.0
        var lng = 0.0
        when (preference.city){
            LVIV -> {
                city = "mapv2_lviv.json"
                lat = 49.83931101832717
                lng = 24.025795160918733
            }
            VINNYTSIA -> {
                lat = 49.234040296991125
                lng = 28.471692181504597
            }
            IVANO_FRANKIVSK -> {
                lat = 48.91391127146098
                lng = 24.711807079409077
            }
            LUTSK -> {
                city = "mapv2_lutsk.json"
                lat = 50.748134106167115
                lng = 25.325763322996053
            }
            else -> city = "mapv2_lviv.json"
        }
        view?.setLatLng(lat, lng, 10F)
        sendApiCoordinateRequest(city)
    }

    private fun sendApiCoordinateRequest(city: String) {
        Api
                .client(preference.city)
                .coordinateResult(city)
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.ui)
                .subscribe(::showApiResult, ::onError)
    }

    private fun onError(t: Throwable) {
        println(COORDINATES_ERROR + t.message)
    }

    private fun showApiResult(delivery: Delivery) {
        view?.showCoordinate(delivery.greenZonePath)
        view?.showCoordinate(delivery.yellowZonePath)
    }

    override fun sendLatLngRequest(placeId: String) {
        Api
                .googleClient
                .streetResult(placeId)
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.ui)
                .subscribe(::showPlacesResult, ::onPlacesError)
    }

    private fun showPlacesResult(places: Places) {
        val lat: Double = places.results[0].geometry.location.lat
        val lng: Double = places.results[0].geometry.location.lng
        view?.setLatLng(lat, lng, 17F)
    }

    private fun onPlacesError(t: Throwable) { println(PLACES_ERROR + t.message) }
}