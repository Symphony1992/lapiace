package ru.olegovich.domain.mvp.final_screen

import ru.olegovich.domain.data.backend.Api
import ru.olegovich.domain.data.entity.coordinates.Delivery
import ru.olegovich.domain.data.entity.delivery_address.DeliveryAddress
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.DeliveryCounter.*
import ru.olegovich.domain.data.entity.enums.PaymentCounter.CARD
import ru.olegovich.domain.data.entity.enums.PaymentCounter.CASH
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.streets.Street
import ru.olegovich.domain.mvp.base.BasePresenterImpl
import java.util.*
import kotlin.collections.ArrayList

class FinalPresenter : BasePresenterImpl<FinalMvp.View>(), FinalMvp.Presenter {

    private lateinit var payment: String
    private lateinit var delivery: String
    private var streets: Map<String, MutableList<Street>> = HashMap()
    private var town: String = ""
    private val totalSum = 0
    private val repository by lazy { dataRepository.delivery }

    override fun onCreated(menu: List<Menu>) {
        setDefaultPaymentDelivery()
        setLatLngCurrent()
        sendApiStreetsRequest()
        view?.setVisibility(DELIVERY)
        view?.setVisibilityInclude(true)
        view?.showMenuList(menu)
        getTotalSum(menu.toMutableList())
    }

    override fun paymentChoose(c: PaymentCounter) {
        payment = when (c){
            CASH -> "cash"
            CARD -> "card"
        }
    }

    override fun deliveryChoose(d: DeliveryCounter) {
        when (d) {
            DELIVERY -> {
                delivery = "delivery"
                view?.setVisibility(DELIVERY)
            }
            PICKUP -> {
                delivery = "pickup"
                view?.setVisibility(PICKUP)
            }
            ADVANCE -> {
                delivery = "advance"
                view?.setVisibility(ADVANCE)
            }
        }
    }

    override fun onCallMeCheckedChange(isChecked: Boolean) {
        view?.setVisibilityInclude(!isChecked)
    }

    override fun applyButtonClick() = view?.showFinalToast()

    override fun onCityChosen(town: String) {
        this.town = town
        streets[town]?.let { view?.setStreets(it) }
    }

    override fun findAddress(str: String, num: String) {
        if (str == "" || num == "" || str.isEmpty() || num.isEmpty()) view?.showErrorFindToast()
        else {
            Api
                    .googleClient
                    .finalStreetResult("$town,$str,$num")
                    .subscribeOn(schedulers.io)
                    .observeOn(schedulers.ui)
                    .subscribe(::ofFocusCamera, ::onFindAddressError)
        }
    }

    private fun onFindAddressError(t: Throwable) = println("$FIND_ADDRESS_ERROR $t.message")

    private fun ofFocusCamera(place: DeliveryAddress) {
        if (place.status.equals("ZERO_RESULTS")){
            println("!!!" + (place.status == "ZERO_RESULTS"))
            view?.showErrorNoZoneText()
        } else {
            view?.showLatLngResult(
                    place.candidates[0].geometry.location.lat,
                    place.candidates[0].geometry.location.lat)
        }
    }

    override fun backButtonClick() = view?.backToCart()

    override fun yellowZoneSumCheck() {
        if (totalSum <= 250) view?.showErrorZoneText()
        else view?.setVisibilityError()
    }

    override fun greenZoneSumCheck() {
        if (totalSum <= 150) view?.showErrorZoneText()
        else view?.setVisibilityError()
    }

    override fun noZoneChosen() = view?.showErrorNoZoneText()

    override fun onTimeTextClick() = view?.onChooseTime()

    override fun onDateTextClick() = view?.onChooseDate()

    private fun setDefaultPaymentDelivery() {
        payment = "cash"
        delivery = "delivery"
    }

    private fun getTotalSum(menu: MutableList<Menu>) {
        var sum = 0
        menu.forEach { sum += (it.counter*it.metaData.price) + getModifiersSum(it.modifiers) }
        view?.showTotalSum(sum)
    }

    private fun getModifiersSum(modifiers: MutableList<Modifier>): Int {
        var sum = 0
        modifiers.forEach { sum += it.metaData.price }
        return sum
    }

    private fun setLatLngCurrent() {
        val city = preference.city
        sendApiCoordinateRequest()
        view?.onLatLngZoom(city.lat, city.lng, 10F)
    }

    private fun sendApiCoordinateRequest() {
        repository
                .load()
                .observeOn(schedulers.ui)
                .subscribe({ showApiCoordinateResult(it) }, ::onCoordinateError)
    }

    private fun onCoordinateError(t: Throwable) {
        println(COORDINATES_ERROR + t.message)
    }

    private fun showApiCoordinateResult(deliveryList: List<Delivery>) {
        view?.showCoordinate(deliveryList[0].greenZonePath, true)
        view?.showCoordinate(deliveryList[0].yellowZonePath, false)
    }

    private fun sendApiStreetsRequest() {
        Api
                .streetsClient
                .streetsResult()
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.ui)
                .subscribe({ streets: Map<String, ArrayList<Street>> ->
                    showApiStreetsResult(streets) }, ::onStreetsError)
    }

    private fun onStreetsError(t: Throwable) {
        println(STREETS_ERROR + t.message)
    }

    private fun showApiStreetsResult(streets: Map<String, MutableList<Street>>) {
        this.streets = streets
        var pos = 0
        val cities: ArrayList<String> = ArrayList(streets.keys)
        cities.forEachIndexed { index, s -> if (s == preference.city.textUA) pos = index }
        view?.showCity(cities, pos)
    }

}

