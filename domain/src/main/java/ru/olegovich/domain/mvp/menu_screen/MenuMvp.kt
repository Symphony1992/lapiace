package ru.olegovich.domain.mvp.menu_screen

import ru.olegovich.domain.mvp.base.BaseMvp

interface MenuMvp {
    interface View : BaseMvp.BaseView {
        fun startPizzaFragment()
        fun startSaladFragment()
        fun startDrinkFragment()
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated()
        fun onMenuPizzaImageViewClick()
        fun onSaladPizzaImageViewClick()
        fun onDrinkPizzaImageViewClick()
    }
}