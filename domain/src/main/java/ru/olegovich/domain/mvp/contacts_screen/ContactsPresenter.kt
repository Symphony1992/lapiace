package ru.olegovich.domain.mvp.contacts_screen

import ru.olegovich.domain.data.entity.enums.FRAGMENT_TO_PAGER
import ru.olegovich.domain.data.entity.enums.STATUS_OF_VIEW
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class ContactsPresenter : BasePresenterImpl<ContactsMvp.View>(), ContactsMvp.Presenter {
    override fun onCreated() {
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, CONTACTS)
        view?.startCallActivity()
        view?.startMapActivity()
        view?.startEmailActivity()
        view?.startFacebookActivity()
        view?.startInstagramActivity()
    }
}