package ru.olegovich.domain.mvp.main

import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.data.entity.enums.Status
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BaseMvp

interface MainMvp {
    interface View : BaseMvp.BaseView {
        fun initButton(city: City)
        fun setVisibility(status: Status, titleCart: Menu)
        fun showUpdatedList(menu: MutableList<Menu>)
        fun onGoToCityActivity()
        fun back()
        fun onActivityResultToPizzaFragment(menu: List<Menu>)
        fun showAnimationCart(quantity: Int)
        fun animationQuantityCart()
        fun showTotalSumCart(sum: Int)
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated()
        fun onStatusResult(status: Status, titleCart: Menu)
        fun menuList(menu: List<Menu>)
        fun onResult(menuItem: Menu)
        fun onCartButtonClick()
        fun onCityButtonClick()
        fun onBackButtonClick()
    }
}