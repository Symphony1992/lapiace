package ru.olegovich.domain.mvp.vacancy

import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.Status
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class VacancyPresenter : BasePresenterImpl<VacancyMvp.View>(), VacancyMvp.Presenter {

    private val repository by lazy { dataRepository.vacancy }

    override fun onCreated() {
        sendApiPizzaRequest()
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, Status.VACANCY)
    }

    private fun sendApiPizzaRequest() {
        repository
                .load()
                .observeOn(schedulers.ui)
                .subscribe({ view?.showVacancy(it) }, ::onError)
    }

    private fun onError(t: Throwable) {
        println(VACANCY_ERROR + t.message)
        view?.showError()
    }

    override fun onMoveToVacancyClick() { view?.scrollToVacancy() }

    override fun onTryAgainClick() { view?.reOpen() }

}