package ru.olegovich.domain.mvp.delivery_screen

import ru.olegovich.domain.data.entity.coordinates.ZonePath
import ru.olegovich.domain.mvp.base.BaseMvp.BasePresenter
import ru.olegovich.domain.mvp.base.BaseMvp.BaseView

interface DeliveryMvp {
    interface View : BaseView {
        fun showCoordinate(zonePath: ZonePath)
        fun setLatLng(lat: Double, lng: Double, zoom: Float)
    }

    interface Presenter : BasePresenter<View> {
        fun onCreated()
        fun sendLatLngRequest(placeId: String)
    }
}