package ru.olegovich.domain.mvp.pizza_details_screen

import ru.olegovich.domain.data.entity.enums.FRAGMENT_TO_PAGER
import ru.olegovich.domain.data.entity.enums.ITEM_TITLE_TRANSFER
import ru.olegovich.domain.data.entity.enums.STATUS_OF_VIEW
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class PizzaDetailsPresenter : BasePresenterImpl<PizzaDetailsMvp.View>(), PizzaDetailsMvp.Presenter {

    var modifiers = mutableListOf<Modifier>()
    lateinit var menu: Menu
    var counter = 1
        set(value) { field = if (value < 1) 1 else value }

    override fun onCreated(menuItem: Menu) {
        menu = menuItem
        view?.showPizzaTitle(menu.title.rendered)
        view?.showPizzaWidth(menu.metaData.width)
        splitText(menu.excerpt.rendered)
        view?.showPizzaWeight(menu.metaData.weight)
        view?.showPizzaImage(menu.metaData.gallery[0])
        view?.showTotalSum((modifierSum + menu.metaData.price) * counter)
        view?.showModifierSum(modifierSum)
        view?.setChosenAdditionsView(false)
        view?.setFragmentResultToActivity(
                STATUS_OF_VIEW, ITEM_TITLE_TRANSFER, FRAGMENT_TO_PAGER, PIZZA_DETAILS, menuItem)
        view?.showCounter(counter)
    }

    override fun onResult(modifierItemTransfer: List<Modifier>) {
        modifiers = (modifierItemTransfer).toMutableList()
        view?.showModifierAdapter(modifierItemTransfer.toMutableList())
    }

    override fun onAddModifierButtonClick() {
        view?.showAddModifierFragment(modifiers)
    }

    override fun onItemClick(item: Modifier, position: Int) {
        modifiers.remove(item)
        view?.removeItem(item, position)
        view?.showModifierSum(modifierSum)
        view?.showTotalSum((modifierSum + menu.metaData.price) * counter)
    }

    override fun onAddToCartButtonClick() {
        val cartMenu = Menu(
                menu.id,
                menu.title,
                menu.content,
                menu.excerpt,
                menu.metaData,
                counter)

        modifiers.sortWith { from, to -> from.id.compareTo(to.id) }
        modifiers.partition { it.isSelected }
        cartMenu.modifiers.addAll(modifiers)
        view?.addMenuToCart(cartMenu)
    }

    override fun onMinusButtonClick() {
//        if (counter > 1)
        counter--
        onChangedCounter()
    }

    override fun onPlusButtonClick() {
        counter++
        onChangedCounter()
    }

    private fun onChangedCounter() {
        view?.showCounter(counter)
        view?.showModifierSum(modifierSum * counter)
        view?.showTotalSum((modifierSum + menu.metaData.price) * counter)
    }

    override val modifierSum: Int get() {
        var sum = 0
        modifiers.forEach { sum += it.metaData.price }
        return sum
    }

    private fun splitText(text: String) {
        val words: Array<String> = text.split(", ").toTypedArray()
        for (i in words.indices) {
            words[i] = words[i].replace("[^\\w]".toRegex(), " ")
        }
        view?.showPizzaComponent(words)
    }

}