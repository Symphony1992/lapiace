package ru.olegovich.domain.mvp.pizza

import ru.olegovich.domain.data.entity.enums.Counter
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BaseMvp

interface PizzaMvp {
    interface View : BaseMvp.BaseView {
        fun showMenu(menuList: List<Menu>)
        fun showError()
        fun reOpen()
        fun sendData(cartMenu: Menu)
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun onCreated(counter: Counter)
        fun onTryAgainClick()
        fun onChangeItemClick(item: Menu, isAdd: Boolean)
        fun onActivityResult(menu: List<Menu>)
    }
}