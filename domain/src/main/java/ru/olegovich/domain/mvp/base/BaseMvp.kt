package ru.olegovich.domain.mvp.base

import java.io.Serializable

interface BaseMvp {

    interface BaseView {
        fun setFragmentResultToActivity(key: String, requestKey: String, value: Serializable)
        fun setFragmentResultToActivity(key: String,
                                        key2: String,
                                        requestKey: String,
                                        value: Serializable,
                                        value2: Serializable)
    }

    interface BasePresenter<V : BaseView> {
        val view: V?
        fun attachView(view: V)
        fun detachView()
    }
}