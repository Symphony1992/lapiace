package ru.olegovich.domain.mvp.cart

import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BaseMvp

interface CartMvp {
    interface View : BaseMvp.BaseView {
        var isIncludeCartVisible: Boolean

        fun removeItem(item: Menu, position: Int)
        fun showMenuList(menu: MutableList<Menu>)
        fun onBackToMain(menu: List<Menu>)
        fun goToMainActivity()
        fun changeItem(item: Menu)
        fun showSum(sum: Int)
        fun startFinalActivity(menu: List<Menu>)
        fun showOpenErrorToast()
    }

    interface Presenter : BaseMvp.BasePresenter<View> {
        fun menuListMenu(menu: List<Menu>)
        fun onDeleteMenuItem(item: Menu, position: Int)
        fun onBackClick()
        fun onBackImageButtonClick()
        fun onChangeCounter(item: Menu, isAdd: Boolean, position: Int)
        fun onBuyButtonClick()
    }
}