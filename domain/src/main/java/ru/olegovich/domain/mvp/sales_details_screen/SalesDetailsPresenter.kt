package ru.olegovich.domain.mvp.sales_details_screen

import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class SalesDetailsPresenter : BasePresenterImpl<SalesDetailsMvp.View>(), SalesDetailsMvp.Presenter {
    override fun onCreated(it: LocationDatum) {
        view?.showImageSales(it.metaData.mobImg)
        view?.showDetailsSales(it.content.rendered)
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, SALES_DETAILS)
    }

}