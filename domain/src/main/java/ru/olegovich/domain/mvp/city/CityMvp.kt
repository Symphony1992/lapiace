package ru.olegovich.domain.mvp.city

import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.mvp.base.BaseMvp.BasePresenter
import ru.olegovich.domain.mvp.base.BaseMvp.BaseView

interface CityMvp {
    interface View : BaseView {
        fun showSelectedToast()
        fun goToMainActivity()
        fun initRadioGroup(city: City)
    }

    interface Presenter : BasePresenter<View> {
        fun onCitySelect(city: City)
        fun onCityApplyClick()
        fun onCreated()
    }
}