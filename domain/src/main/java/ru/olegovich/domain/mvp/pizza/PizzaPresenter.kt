package ru.olegovich.domain.mvp.pizza

import ru.olegovich.domain.data.entity.enums.Counter
import ru.olegovich.domain.data.entity.enums.Counter.*
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.Status
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class PizzaPresenter : BasePresenterImpl<PizzaMvp.View>(), PizzaMvp.Presenter {

    private var sortedProduct = mutableListOf<Menu>()
    private val repository by lazy { dataRepository.menu }

    override fun onCreated(counter: Counter) {
        if (sortedProduct.isEmpty()) sendApiPizzaRequest(counter)
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, Status.PIZZA)
    }

    override fun onTryAgainClick() {
        view?.reOpen()
    }

    override fun onChangeItemClick(item: Menu, isAdd: Boolean) {
        view?.sendData(Menu(
                item.id,
                item.title,
                item.content,
                item.excerpt,
                item.metaData,
                if (isAdd) 1 else -1,
                item.modifiers
        ))
    }

    override fun onActivityResult(menu: List<Menu>) {
        sortedProduct.forEach { it.counter = countById(it.id, menu) }
        view?.showMenu(sortedProduct)
    }

    private fun countById(id: Int, menu: List<Menu>): Int {
        var count = 0
        menu.forEach { if (it.id == id) count += it.counter }
        return count
    }

    private fun sendApiPizzaRequest(counter: Counter) {
        repository
                .load()
                .observeOn(schedulers.ui)
                .subscribe({ sortMenu(it, counter) }, ::onError)
    }

    private fun sortMenu(item: List<Menu>, counter: Counter) {
        val product = mutableListOf<Menu>()
        when (counter) {
            PIZZA -> item.forEach { if (it.metaData.cat.equals("pizza")) product.add(it) }
            SALAD -> item.forEach { if (it.metaData.cat.equals("salad")) product.add(it) }
            DRINK -> item.forEach { if (it.metaData.cat.equals("drinks")) product.add(it) }
        }
        sortedProduct = product
        view?.showMenu(product)
    }

    private fun onError(t: Throwable) {
        println(PIZZA_ERROR + t.message)
        view?.showError()
    }
}