package ru.olegovich.domain.mvp.sales_screen

import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.mvp.base.BasePresenterImpl

class SalesPresenter : BasePresenterImpl<SalesMvp.View>(), SalesMvp.Presenter {

    private val repository by lazy { dataRepository.sale }

    override fun onCreated() {
        sendApiSalesRequest()
        view?.setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, SALES)
    }

    override fun onTryAgain() {
        view?.reOpen()
        onCreated()
    }

    private fun sendApiSalesRequest() {
        repository
                .load()
                .observeOn(schedulers.ui)
                .subscribe({ view?.showSales(it) }, ::onError)
    }

    private fun onError(t: Throwable) {
        println(SALES_ERROR + t.message)
        view?.showError()
    }

}

