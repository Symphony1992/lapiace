package ru.olegovich.domain.component

interface ComponentProvider {

    val preference: Preference
    val schedulers: AppSchedulers
    val dataRepository: DataRepository

    companion object {

        lateinit var instance: ComponentProvider

        fun inject(instance: ComponentProvider) {
            ComponentProvider.instance = instance
        }
    }
}