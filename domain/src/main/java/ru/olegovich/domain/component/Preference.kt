package ru.olegovich.domain.component

import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.data.entity.enums.UpdatableData
import java.util.*

interface Preference {
    var city: City
//    var lastUpdateTime: Calendar

    fun lastUpdateTime(type: UpdatableData):  Long
    fun lastUpdateTime(type: UpdatableData, value: Long)
}
