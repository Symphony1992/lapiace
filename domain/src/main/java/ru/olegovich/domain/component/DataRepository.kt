package ru.olegovich.domain.component

import ru.olegovich.domain.data.entity.coordinates.Delivery
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.data.entity.vacancies.Vacancy
import ru.olegovich.domain.data.repository.Repository

interface DataRepository {
    val vacancy: Repository<Vacancy>
    val modifier: Repository<Modifier>
    val menu: Repository<Menu>
    val sale: Repository<LocationDatum>
    val delivery: Repository<Delivery>
}