package ru.olegovich.domain.data.entity.streets;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

//@Entity(tableName = "street")
public class Street {

    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
