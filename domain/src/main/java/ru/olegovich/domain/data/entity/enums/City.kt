package ru.olegovich.domain.data.entity.enums

enum class City (
        var number: Int,
        var textUA: String,
        var url: String,
        var lat: Double,
        var lng: Double)
{
    NOT_SELECTED(-1, "", "", 0.0, 0.0),

    LVIV(0,
            "\u041b\u044c\u0432\u0456\u0432",
            "mapv2_lviv.json",
            49.83931101832717,
            24.025795160918733),

    VINNYTSIA(1,
            "\u0412\u0456\u043d\u043d\u0438\u0446\u044f",
            "mapv2_vinnytsya.json",
            49.234040296991125,
            28.471692181504597),

    IVANO_FRANKIVSK(2,
            "\u0406\u0432\u0430\u043d\u043e-\u0424\u0440\u0430\u043d\u043a\u0456\u0432\u0441\u044c\u043a",
            "mapv2_if.json",
            48.91391127146098,
            24.711807079409077),

    LUTSK (3,
            "\u041b\u0443\u0446\u044c\u043a",
            "mapv2_lutsk.json",
            50.748134106167115,
            25.325763322996053)
}