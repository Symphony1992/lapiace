package ru.olegovich.domain.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import ru.olegovich.domain.data.entity.pizza.Menu

@Dao
interface MenuDao : DataDao<Menu> {
    @Query("SELECT * FROM menu")
    override fun getAll (): List<Menu>
}