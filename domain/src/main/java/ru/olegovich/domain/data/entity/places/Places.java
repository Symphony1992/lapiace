
package ru.olegovich.domain.data.entity.places;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.local.converters.ResultConverter;

//@Entity(tableName = "places")
public class Places implements Serializable
{

    @SerializedName("results")
    @Expose
    @ColumnInfo(name = "results")
    private List<Result> results = null;

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
