package ru.olegovich.domain.data.backend

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.olegovich.domain.data.entity.streets.Street
import java.util.*

class ClientApiStreets {


    private val baseURL = "https://www.lapiec-pizza.com.ua/"

    private val clientApi = provideApi()

    fun streetsResult(): Single<Map<String, ArrayList<Street>>> {
        return clientApi.streetsResult()
    }

    private fun provideApi(): ClientApiStreet {
        return Retrofit.Builder()
                .baseUrl(baseURL)
                .client(provideHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ClientApiStreet::class.java)
    }

    private fun provideHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
    }

}