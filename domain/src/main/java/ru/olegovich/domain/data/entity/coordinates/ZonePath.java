
package ru.olegovich.domain.data.entity.coordinates;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.local.converters.CoordinateConverter;
import ru.olegovich.domain.data.local.converters.ModifierConverter;

public class ZonePath implements Serializable
{

    @ColumnInfo(name = "color")
    @SerializedName("color")
    private String color;

    @ColumnInfo(name = "coordinates")
    @SerializedName("coordinates")
    private List<Coordinate> coordinates = null;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }
}
