
package ru.olegovich.domain.data.entity.coordinates;

import androidx.room.ColumnInfo;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coordinate implements Serializable
{

    @SerializedName("lat")
    @Expose
    @ColumnInfo(name = "lat")
    private Double lat;

    @SerializedName("lng")
    @Expose
    @ColumnInfo (name = "lng")
    private Double lng;

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
