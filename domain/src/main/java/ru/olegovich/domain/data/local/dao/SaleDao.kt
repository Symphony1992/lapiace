package ru.olegovich.domain.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import ru.olegovich.domain.data.entity.sales.LocationDatum

@Dao
interface SaleDao : DataDao<LocationDatum> {
    @Query("SELECT * FROM sale")
    override fun getAll (): List<LocationDatum>
}