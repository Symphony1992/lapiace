package ru.olegovich.domain.data.entity.enums

enum class UpdatableData {
    SALES,
    VACANCIES,
    MENU,
    MODIFIERS,
    COORDINATES,
    DISTRICTS
}