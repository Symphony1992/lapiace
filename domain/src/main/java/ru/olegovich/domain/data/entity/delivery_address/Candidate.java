
package ru.olegovich.domain.data.entity.delivery_address;

import androidx.room.Embedded;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.entity.Geometry;

public class Candidate implements Serializable
{

    @SerializedName("geometry")
    @Expose
    @Embedded (prefix = "geometry_")
    private Geometry geometry;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
