package ru.olegovich.domain.data.repository

import ru.olegovich.domain.data.backend.Api
import ru.olegovich.domain.data.backend.ClientApiBase
import ru.olegovich.domain.component.AppSchedulers
import ru.olegovich.domain.component.Preference
import io.reactivex.Single as Single

class RemoteRepository<T>(private val preference: Preference,
                          private var schedulers: AppSchedulers,
                          private val loadData: ClientApiBase.() -> Single<List<T>>) : Repository<T> {

    private val city get() = preference.city

    override fun load(): Single<List<T>> = loadData(Api.client(city)).subscribeOn(schedulers.io)

}