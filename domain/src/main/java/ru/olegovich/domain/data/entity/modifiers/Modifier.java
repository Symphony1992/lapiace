
package ru.olegovich.domain.data.entity.modifiers;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.entity.Title;

@Entity(tableName = "modifier")
public class Modifier implements Serializable
{
    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @Embedded(prefix = "title_")
    @SerializedName("title")
    private Title title;

    @Embedded (prefix = "meta_data_")
    @SerializedName("meta_data")
    private MetaData metaData;

    @ColumnInfo(name = "isSelected")
    @SerializedName("isSelected")
    boolean isSelected = false;

    public boolean isSelected() { return isSelected; }

    public void setSelected(boolean selected) { isSelected = selected; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Title getTitle() {
        return title;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setTitle(Title title) { this.title = title; }

    public void setMetaData(MetaData metaData) { this.metaData = metaData; }

}
