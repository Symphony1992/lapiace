package ru.olegovich.domain.data.entity.enums

enum class Months(var text: String) {
    JANUARY("\u0421\u0456\u0447\u0435\u043d\u044c"),
    FEBRUARY("\u041b\u044e\u0442\u0438\u0439"),
    MARCH("\u0411\u0435\u0440\u0435\u0437\u0435\u043d\u044c"),
    APRIL("\u041a\u0432\u0456\u0442\u0435\u043d\u044c"),
    MAY("\u0422\u0440\u0430\u0432\u0435\u043d\u044c"),
    JUNE("\u0427\u0435\u0440\u0432\u0435\u043d\u044c"),
    JULY("\u041b\u0438\u043f\u0435\u043d\u044c"),
    AUGUST("\u0421\u0435\u0440\u043f\u0435\u043d\u044c"),
    SEPTEMBER("\u0412\u0435\u0440\u0435\u0441\u0435\u043d\u044c"),
    OCTOBER("\u0416\u043e\u0432\u0442\u0435\u043d\u044c"),
    NOVEMBER("\u041b\u0438\u0441\u0442\u043e\u043f\u0430\u0434"),
    DECEMBER("\u0413\u0440\u0443\u0434\u0435\u043d\u044c");
}
