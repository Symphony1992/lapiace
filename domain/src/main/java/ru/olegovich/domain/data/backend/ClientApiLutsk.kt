package ru.olegovich.domain.data.backend

class ClientApiLutsk : ClientApiBase() {

    override fun getUrl(): String = "https://www.lapiec-pizza.lt.ua"

    val city: String = "mapv2_lutsk.json"

}
