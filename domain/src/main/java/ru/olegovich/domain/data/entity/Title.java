
package ru.olegovich.domain.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

public class Title implements Serializable
{

    @ColumnInfo(name = "rendered")
    @SerializedName("rendered")
    private String rendered;

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }

}
