package ru.olegovich.domain.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import ru.olegovich.domain.data.entity.coordinates.Delivery

@Dao
interface CoordinateDao : DataDao<Delivery> {

    @Query("SELECT * FROM delivery")
    override fun getAll (): List<Delivery>

}