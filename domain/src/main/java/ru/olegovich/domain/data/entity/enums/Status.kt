package ru.olegovich.domain.data.entity.enums

import java.io.Serializable

enum class Status : Serializable {
    ABOUT,
    ADD_MODIFIER,
    CONTACTS,
    DELIVERY,
    INFO,
    MENU,
    PARTNERS,
    PIZZA_DETAILS,
    PIZZA,
    SALES_DETAILS,
    SALES,
    VACANCY
}