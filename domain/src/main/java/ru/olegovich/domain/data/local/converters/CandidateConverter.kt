package ru.olegovich.domain.data.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.olegovich.domain.data.entity.delivery_address.Candidate

class CandidateConverter {
    @TypeConverter
    fun fromString(value: String): List<Candidate> = Gson()
            .fromJson(value, object : TypeToken<List<Candidate>>() {}.type)

    @TypeConverter
    fun fromList(list: List<Candidate>): String = Gson().toJson(list)

}