package ru.olegovich.domain.data.entity.enums

enum class PagerCounter {
    PAGER_INFO,
    PAGER_MENU,
    PAGER_SALES
}