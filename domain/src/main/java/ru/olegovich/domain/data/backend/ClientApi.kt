package ru.olegovich.domain.data.backend

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import ru.olegovich.domain.data.entity.coordinates.Delivery
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.data.entity.vacancies.Vacancy

interface ClientApi {

    @GET("/wp-json/wp/v2/action")
    fun salesResult(): Single<List<LocationDatum>>

    @GET("/wp-json/wp/v2/product?per_page=100")
    fun pizzaResult(): Single<List<Menu>>

    @GET("/wp-json/wp/v2/vacancies")
    fun vacancyResult(): Single<List<Vacancy>>

    @GET("/wp-json/wp/v2/modifier?per_page=100")
    fun modifierResult(): Single<List<Modifier>>

    @GET("/wp-content/themes/lapiec/inc/{city}")
    fun coordinateResult(@Path("city") city: String): Single<Delivery>

}