package ru.olegovich.domain.data.local.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

interface DataDao<T> {

    fun getAll (): List<T>

    @Insert
    fun insert(value: List<T>)

    @Delete
    fun delete(value: List<T>)

    @Insert
    fun insert(value: T)

    @Delete
    fun delete(value: T)
}