package ru.olegovich.domain.data.local.cache

import io.reactivex.Completable
import io.reactivex.Single
import ru.olegovich.domain.component.AppSchedulers
import ru.olegovich.domain.data.local.dao.DataDao

class DiskCache<T>(
        private val dataDao: DataDao<T>,
        private val schedulers: AppSchedulers
) : Cache<T> {

    override fun data(): Single<List<T>> = Single
            .fromCallable { dataDao.getAll() }
            .subscribeOn(schedulers.db)

    override fun updateData(data: List<T>): Completable = Completable
            .fromCallable {
                dataDao.delete(data)
                dataDao.insert(data)
            }.subscribeOn(schedulers.db)

    override fun deleteData(data: List<T>): Completable = Completable
            .fromCallable { dataDao.delete(data) }
            .subscribeOn(schedulers.db)

    override fun updateData(data: T): Completable = Completable
            .fromCallable {
                dataDao.delete(data)
                dataDao.insert(data)
            }.subscribeOn(schedulers.db)

    override fun deleteData(data: T): Completable = Completable
            .fromCallable { dataDao.delete(data) }
            .subscribeOn(schedulers.db)

}