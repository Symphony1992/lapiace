package ru.olegovich.domain.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import ru.olegovich.domain.data.entity.vacancies.Vacancy

@Dao
interface VacancyDao : DataDao<Vacancy>{

    @Query ("SELECT * FROM vacancy")
    override fun getAll (): List<Vacancy>

}