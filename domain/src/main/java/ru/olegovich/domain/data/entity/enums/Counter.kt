package ru.olegovich.domain.data.entity.enums

enum class Counter {
    PIZZA, SALAD, DRINK
}
