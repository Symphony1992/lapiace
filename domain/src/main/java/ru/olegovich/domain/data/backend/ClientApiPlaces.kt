package ru.olegovich.domain.data.backend

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.olegovich.domain.data.entity.delivery_address.DeliveryAddress
import ru.olegovich.domain.data.entity.places.Places


interface ClientApiPlaces {

    @GET("geocode/json")
    fun streetResult(@Query("key") api_key: String,
                     @Query("place_id") placeId: String): Single<Places>

    @GET("place/findplacefromtext/json")
    fun finalStreetResult(@Query("key") api_key: String,
                          @Query("input") address: String,
                          @Query("inputtype") input_type: String,
                          @Query("fields") fields: String): Single<DeliveryAddress>

}
