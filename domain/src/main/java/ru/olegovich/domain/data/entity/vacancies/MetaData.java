
package ru.olegovich.domain.data.entity.vacancies;

import androidx.room.ColumnInfo;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

public class MetaData implements Serializable
{
    @ColumnInfo(name = "image_url")
    @SerializedName("image_url")
    private String imageUrl;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}
