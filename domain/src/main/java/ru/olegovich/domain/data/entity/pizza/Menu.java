
package ru.olegovich.domain.data.entity.pizza;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.entity.Content;
import ru.olegovich.domain.data.entity.Title;
import ru.olegovich.domain.data.entity.modifiers.Modifier;
import ru.olegovich.domain.data.local.converters.ModifierConverter;

@Entity(tableName = "menu")
public class Menu implements Serializable
{

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @Embedded(prefix = "title_")
    @SerializedName("title")
    private Title title;

    @Embedded (prefix = "content_")
    @SerializedName("content")
    private Content content;

    @Embedded (prefix = "excerpt_")
    @SerializedName("excerpt")
    private Excerpt excerpt;

    @Embedded (prefix = "meta_data_")
    @SerializedName("meta_data")
    private MetaData metaData;

    @ColumnInfo(name = "counter")
    private int counter = 0;

    @ColumnInfo(name = "modifiers")
    private List<Modifier> modifiers = new ArrayList<>();

    public Menu(int id, Title title, Content content, Excerpt excerpt,
                MetaData metaData, int counter) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.excerpt = excerpt;
        this.metaData = metaData;
        this.counter = counter;
    }
    public Menu(int id, Title title, Content content, Excerpt excerpt,
                MetaData metaData, int counter, List<Modifier> modifiers) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.excerpt = excerpt;
        this.metaData = metaData;
        this.counter = counter;
        this.modifiers = modifiers;
    }

    public Menu() {
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public List<Modifier> getModifiers() {
        return modifiers;
    }

    public void setModifiers(@NonNull List<Modifier> modifiers) { this.modifiers = modifiers; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Title getTitle() {
        return title;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Excerpt getExcerpt() {
        return excerpt;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public void setExcerpt(Excerpt excerpt) {
        this.excerpt = excerpt;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
