package ru.olegovich.domain.data.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.olegovich.domain.data.entity.modifiers.Modifier

class ModifierConverter {

    @TypeConverter
    fun fromString(value: String): List<Modifier> = Gson()
                .fromJson(value, object : TypeToken<List<Modifier>>() {}.type)

    @TypeConverter
    fun fromList(list: List<Modifier>): String  = Gson().toJson(list)

}