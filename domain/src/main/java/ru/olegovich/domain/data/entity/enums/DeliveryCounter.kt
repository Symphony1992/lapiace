package ru.olegovich.domain.data.entity.enums

enum class DeliveryCounter {
    DELIVERY, PICKUP, ADVANCE
}