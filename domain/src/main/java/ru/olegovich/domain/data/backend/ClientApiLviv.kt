package ru.olegovich.domain.data.backend

class ClientApiLviv : ClientApiBase() {

    override fun getUrl(): String = "https://www.lapiec-pizza.com.ua"

    val city: String = "mapv2_lviv.json"

}