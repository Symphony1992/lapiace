
package ru.olegovich.domain.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.entity.Location;

public class Geometry implements Serializable
{

    @SerializedName("location")
    @Expose
    @Embedded(prefix = "location_")
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
