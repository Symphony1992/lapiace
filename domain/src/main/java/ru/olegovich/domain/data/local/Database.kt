package ru.olegovich.domain.data.local

import ru.olegovich.domain.data.local.dao.*

interface Database{
    val vacancyDao: VacancyDao
    val modifierDao: ModifierDao
    val menuDao: MenuDao
    val saleDao: SaleDao
    val coordinateDao: CoordinateDao
}