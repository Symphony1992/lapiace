
package ru.olegovich.domain.data.entity.sales;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.entity.Content;
import ru.olegovich.domain.data.entity.Title;

@Entity(tableName = "sale")
public class LocationDatum implements Serializable
{

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @Embedded(prefix = "title_")
    @SerializedName("title")
    private Title title;

    @Embedded (prefix = "content_")
    @SerializedName("content")
    private Content content;

    @Embedded (prefix = "meta_data_")
    @SerializedName("meta_data")
    private MetaData metaData;

    public int getId() {
        return id;
    }

    public Title getTitle() {
        return title;
    }

    public Content getContent() {
        return content;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}