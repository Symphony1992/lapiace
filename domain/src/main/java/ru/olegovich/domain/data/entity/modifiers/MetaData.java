package ru.olegovich.domain.data.entity.modifiers;

import androidx.room.ColumnInfo;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

public class MetaData implements Serializable
{

    @ColumnInfo(name = "weight")
    @SerializedName("weight")
    private String weight;

    @ColumnInfo(name = "price")
    @SerializedName("price")
    private int price;

    @ColumnInfo(name = "mob_img")
    @SerializedName("mob_img")
    private String mobImg;

    public String getWeight() {
        return weight;
    }

    public int getPrice() {
        return price;
    }

    public String getMobImg() {
        return mobImg;
    }

    public void setWeight(String weight) { this.weight = weight; }

    public void setPrice(int price) { this.price = price; }

    public void setMobImg(String mobImg) { this.mobImg = mobImg; }
}
