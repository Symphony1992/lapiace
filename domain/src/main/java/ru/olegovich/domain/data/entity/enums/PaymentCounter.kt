package ru.olegovich.domain.data.entity.enums

enum class PaymentCounter {
    CASH, CARD
}