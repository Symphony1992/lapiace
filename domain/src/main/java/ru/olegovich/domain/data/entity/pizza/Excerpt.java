
package ru.olegovich.domain.data.entity.pizza;

import androidx.room.ColumnInfo;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

public class Excerpt implements Serializable
{

    @ColumnInfo(name = "rendered")
    @SerializedName("rendered")
    private String rendered;

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }
}
