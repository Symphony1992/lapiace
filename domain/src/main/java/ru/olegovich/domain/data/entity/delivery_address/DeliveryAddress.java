
package ru.olegovich.domain.data.entity.delivery_address;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.local.converters.CandidateConverter;
import ru.olegovich.domain.data.local.converters.ModifierConverter;

//@Entity(tableName = "deliveryAddress")
public class DeliveryAddress implements Serializable
{

    @SerializedName("candidates")
    @Expose
    @ColumnInfo(name = "candidates")
    List<Candidate> candidates = null;

    @ColumnInfo(name = "status")
    @SerializedName("status")
    @Expose
    String status = null;

    public String getStatus() { return status; }

    public List<Candidate> getCandidates() { return candidates; }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
