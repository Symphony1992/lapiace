package ru.olegovich.domain.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import ru.olegovich.domain.data.entity.modifiers.Modifier

@Dao
interface ModifierDao : DataDao<Modifier> {

    @Query("SELECT * FROM modifier")
    override fun getAll (): List<Modifier>

}