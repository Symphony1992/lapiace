
package ru.olegovich.domain.data.entity.sales;

import androidx.room.ColumnInfo;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

public class MetaData implements Serializable
{

    @ColumnInfo(name = "mob_img")
    @SerializedName("mob_img")
    private String mobImg;

    public String getMobImg() {
        return mobImg;
    }

    public void setMobImg(String mobImg) {
        this.mobImg = mobImg;
    }
}
