package ru.olegovich.domain.data.backend

import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.data.entity.enums.City.*

object Api {

    private lateinit var client: ClientApiBase
    private lateinit var clientLviv: ClientApiLviv
    private lateinit var clientVinnitsa: ClientApiVinnitsa
    private lateinit var clientIvanoFrankivsk: ClientApiIvFr
    private lateinit var clientLutsk: ClientApiLutsk
    val googleClient by lazy { ClientApiGoogle() }
    val streetsClient by lazy { ClientApiStreets() } // TODO: 16.04.2021 критует

    fun client(city: City): ClientApiBase {
        when (city){
            LVIV -> {
                clientLviv = ClientApiLviv()
                client = clientLviv
            }
            VINNYTSIA -> {
                clientVinnitsa = ClientApiVinnitsa()
                client = clientVinnitsa
            }
            IVANO_FRANKIVSK -> {
                clientIvanoFrankivsk = ClientApiIvFr()
                client = clientIvanoFrankivsk
            }
            LUTSK -> {
                clientLutsk = ClientApiLutsk()
                client = clientLutsk
            }
            else -> Unit
        }
        return client
    }

}