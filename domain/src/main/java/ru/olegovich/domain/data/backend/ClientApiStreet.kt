package ru.olegovich.domain.data.backend

import io.reactivex.Single
import retrofit2.http.GET
import ru.olegovich.domain.data.entity.streets.Street
import java.util.*

interface ClientApiStreet {

    @GET("wp-content/themes/lapiec/inc/city_streets.json")
    fun streetsResult(): Single<Map<String, ArrayList<Street>>>

}
