package ru.olegovich.domain.data.backend

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.olegovich.domain.data.entity.coordinates.Delivery
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.data.entity.vacancies.Vacancy

abstract class ClientApiBase {

    abstract fun getUrl(): String

    private val clientApi = provideApi()

    open fun salesResult(): Single<List<LocationDatum>> = clientApi.salesResult()

    open fun pizzaResult(): Single<List<Menu>> = clientApi.pizzaResult()

    open fun vacancyResult(): Single<List<Vacancy>> = clientApi.vacancyResult()

    open fun modifierResult(): Single<List<Modifier>> = clientApi.modifierResult()

    open fun coordinateResult(city: String): Single<Delivery> = clientApi.coordinateResult(city)

    private fun provideApi(): ClientApi {
        return Retrofit.Builder()
                .baseUrl(getUrl())
                .client(provideHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ClientApi::class.java)
    }

    private fun provideHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
    }

}