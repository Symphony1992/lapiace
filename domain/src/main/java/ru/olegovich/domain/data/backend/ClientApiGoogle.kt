package ru.olegovich.domain.data.backend

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.olegovich.domain.data.entity.delivery_address.DeliveryAddress
import ru.olegovich.domain.data.entity.places.Places

class ClientApiGoogle {

    private val apiKey = "AIzaSyCnrdiYnz155j0qknmFqeW1aqLOvpsLLwQ"
    private val baseURL = "https://maps.googleapis.com/maps/api/"
    private val inputTypeURL = "textquery"
    private val fieldsURL = "geometry"

    private val clientApi = provideApi()

    fun streetResult(placeId: String): Single<Places> {
        return clientApi.streetResult(apiKey, placeId)
    }

    fun finalStreetResult(address: String): Single<DeliveryAddress> {
        return clientApi.finalStreetResult(apiKey, address, inputTypeURL, fieldsURL)
    }

    private fun provideApi(): ClientApiPlaces {
        return Retrofit.Builder()
                .baseUrl(baseURL)
                .client(provideHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ClientApiPlaces::class.java)
    }

    private fun provideHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
    }
}