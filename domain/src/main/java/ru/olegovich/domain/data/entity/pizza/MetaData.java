
package ru.olegovich.domain.data.entity.pizza;

import androidx.room.ColumnInfo;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MetaData implements Serializable
{
    @ColumnInfo(name = "price")
    @SerializedName("price")
    private int price;

    @ColumnInfo(name = "width")
    @SerializedName("width")
    private String width;

    @ColumnInfo(name = "weight")
    @SerializedName("weight")
    private String weight;

    @ColumnInfo(name = "cat")
    @SerializedName("cat")
    private String cat;

    @ColumnInfo(name = "gallery")
    @SerializedName("gallery")
    private List<String> gallery = null;

    public int getPrice() {
        return price;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getWeight() {
        return weight;
    }

    public String getCat() {
        return cat;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }
}
