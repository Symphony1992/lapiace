
package ru.olegovich.domain.data.entity.places;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.olegovich.domain.data.entity.Geometry;

public class Result implements Serializable
{

    @SerializedName("geometry")
    @Expose
    @Embedded(prefix = "geometry_")
    private Geometry geometry;
    @SerializedName("place_id")
    @Expose
    @ColumnInfo(name = "place_id")
    private String placeId;

    public void setGeometry(Geometry geometry) { this.geometry = geometry; }

    public String getPlaceId() { return placeId; }

    public void setPlaceId(String placeId) { this.placeId = placeId; }

    public Geometry getGeometry() {
        return geometry;
    }

}
