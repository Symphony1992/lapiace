package ru.olegovich.domain.data.local.cache

import io.reactivex.Completable
import io.reactivex.Single

interface Cache<T> {

    fun data(): Single<List<T>>
    fun updateData(data: List<T>): Completable
    fun deleteData(data: List<T>): Completable
    fun updateData(data: T): Completable
    fun deleteData(data: T): Completable

}