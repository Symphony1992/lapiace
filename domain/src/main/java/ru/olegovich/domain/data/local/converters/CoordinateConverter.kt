package ru.olegovich.domain.data.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.olegovich.domain.data.entity.coordinates.Coordinate

class CoordinateConverter{
    @TypeConverter
    fun fromString(value: String): List<Coordinate> = Gson()
            .fromJson(value, object : TypeToken<List<Coordinate>>() {}.type)

    @TypeConverter
    fun fromList(list: List<Coordinate>): String = Gson().toJson(list)
}