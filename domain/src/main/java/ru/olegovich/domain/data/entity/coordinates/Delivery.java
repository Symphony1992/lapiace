
package ru.olegovich.domain.data.entity.coordinates;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "delivery")
public class Delivery implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @Embedded(prefix = "yellowZonePath_")
    @SerializedName("yellowZonePath")
    private ZonePath yellowZonePath;

    @Embedded(prefix = "greenZonePath_")
    @SerializedName("greenZonePath")
    private ZonePath greenZonePath;

    public ZonePath getYellowZonePath() {
        return yellowZonePath;
    }

    public ZonePath getGreenZonePath() {
        return greenZonePath;
    }

    public void setYellowZonePath(ZonePath yellowZonePath) {
        this.yellowZonePath = yellowZonePath;
    }

    public void setGreenZonePath(ZonePath greenZonePath) {
        this.greenZonePath = greenZonePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
