package ru.olegovich.domain.data.repository

import io.reactivex.Single
import ru.olegovich.domain.component.Preference
import ru.olegovich.domain.data.entity.enums.DAY_IN_MILLIS
import ru.olegovich.domain.data.entity.enums.UpdatableData
import ru.olegovich.domain.data.local.cache.Cache
import java.util.*

class CacheRepository<T> (
        private val preference: Preference,
        private val type: UpdatableData,
        private val cache: Cache<T>,
        private val repository: Repository<T>
) : Repository<T> {

    private val isOutdated
        get() = System.currentTimeMillis() - preference.lastUpdateTime(type) > DAY_IN_MILLIS

    override fun load(): Single<List<T>> = if (isOutdated) loadRemote() else loadCache()

    private fun loadCache() = cache.data()

    private fun loadRemote() = repository
            .load()
            .flatMap { updateCache(it) }
            .onErrorResumeNext { t -> loadCache().map { if (!it.isNullOrEmpty()) it else throw t } }

    private fun updateCache(data: List<T>) = cache
            .updateData(data)
            .doOnComplete { preference.lastUpdateTime(type, System.currentTimeMillis())}
            .toSingleDefault(data)
}