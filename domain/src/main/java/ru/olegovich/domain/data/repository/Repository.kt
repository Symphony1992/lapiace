package ru.olegovich.domain.data.repository

import io.reactivex.Single

interface Repository<T> {
    fun load(): Single<List<T>>
}