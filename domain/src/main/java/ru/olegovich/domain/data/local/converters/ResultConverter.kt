package ru.olegovich.domain.data.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.olegovich.domain.data.entity.places.Result

class ResultConverter {
    @TypeConverter
    fun fromString(value: String): List<Result> = Gson()
            .fromJson(value, object : TypeToken<List<Result>>() {}.type)

    @TypeConverter
    fun fromList(list: List<Result>): String = Gson().toJson(list)
}