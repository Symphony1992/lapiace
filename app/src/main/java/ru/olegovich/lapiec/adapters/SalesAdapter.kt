package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.lapiec.ui.databinding.ItemSalesBinding
import ru.olegovich.lapiec.util.*

class SalesAdapter(var items: List<LocationDatum>)
    : RecyclerView.Adapter<SalesAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    private var listener: ((LocationDatum) -> Unit)? = null

    fun onItemClickListener(listener: (LocationDatum) -> Unit) { this.listener = listener }

    class ViewHolder(val binding: ItemSalesBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(item: LocationDatum){
            with(binding){
                salesItemTextView.text = item.title.rendered.fromHtml()
                salesItemImageView.load(item.metaData.mobImg)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSalesBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
        holder.binding.salesItemButton.setOnClickListener { listener?.invoke(item) }
    }
}
//class SalesAdapter(var items: List<LocationDatum>)
//    : BaseRecyclerAdapter() {
//
//    override fun layoutRes(viewType: Int): Int = R.layout.item_sales
//
//    private var listener: ((LocationDatum) -> Unit)? = null
//
//    override fun ViewHolder.onCreateView(position: Int) {
//        val item = items[position]
//        sales_item_text_view.text = item.title.rendered.fromHtml()
//        sales_item_image_view.load(item.metaData.mobImg)
//        sales_item_button.setOnClickListener { listener?.invoke(item) }
//    }
//
//    fun onItemClickListener(listener: (LocationDatum) -> Unit) {
//        this.listener = listener
//    }
//
//    override fun getItemCount(): Int = items.size
//
//}