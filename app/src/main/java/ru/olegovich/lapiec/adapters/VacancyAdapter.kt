package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.olegovich.domain.data.entity.vacancies.Vacancy
import ru.olegovich.lapiec.ui.R
import ru.olegovich.lapiec.ui.databinding.ItemVacancyBinding
import ru.olegovich.lapiec.util.fromHtml
import ru.olegovich.lapiec.util.load

class VacancyAdapter(var items: List<Vacancy>) : RecyclerView.Adapter<VacancyAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    class ViewHolder(private val binding: ItemVacancyBinding) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Vacancy){
            with(binding){
                vacancyItemImageView.load(item.metaData.imageUrl)
                vacancyNameItemTextView.text = item.title.rendered.fromHtml()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemVacancyBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }
}