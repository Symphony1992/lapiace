package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.lapiec.ui.R
import ru.olegovich.lapiec.util.*
import ru.olegovich.lapiec.ui.databinding.ItemPizzaModifierRecyclerviewBinding as ViewBinding

class AddedModifierAdapter(private val listener: OnItemClickListener)
    : RecyclerView.Adapter<AddedModifierAdapter.ViewHolder>() {

    private val items = mutableListOf<Modifier>()

    override fun getItemCount(): Int = items.size

    fun isEmpty(): Boolean = items.isEmpty()

    fun updateItem(modifiers: MutableList<Modifier>) {
        items.clear()
        items.addAll(modifiers)
        notifyDataSetChanged()
    }

    fun removeItem(item: Modifier, position: Int) {
        items.remove(item)
        notifyItemRemoved(position)
    }

    interface OnItemClickListener{
        fun onItemClick(item: Modifier, position: Int)
    }

    class ViewHolder(private val binding: ViewBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: Modifier, listener: OnItemClickListener){
            if (item.isSelected) {
                with(binding){
                    pizzaModifierTitleTextView.text = item.title.rendered
                    pizzaModifierWeightTextView.text(R.string.gram, item.metaData.weight)
                    pizzaModifierPriceTextView.text = item.metaData.price.toString()
                    pizzaModifierImageView.load(item.metaData.mobImg)
                    pizzaModifierCancelButton.setOnClickListener {
                        listener.onItemClick(item, bindingAdapterPosition)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ViewBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

}