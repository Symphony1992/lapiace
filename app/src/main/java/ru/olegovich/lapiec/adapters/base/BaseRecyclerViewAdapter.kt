package ru.olegovich.lapiec.adapters.base

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

abstract class BaseRecyclerViewAdapter<T, V : ViewBinding>(val items: Array<T>) :
        RecyclerView.Adapter<BaseRecyclerViewAdapter.ViewHolder<V>>() {

    class ViewHolder<VB : ViewBinding>(val binding: VB) :
            RecyclerView.ViewHolder(binding.root){
    }

    override fun getItemCount(): Int = items.size

}