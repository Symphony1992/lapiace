package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.olegovich.lapiec.ui.databinding.ItemPizzaDetailsRecycleviewBinding
import java.util.*

class PizzaDetailsAdapter(val items: Array<String>) :
        RecyclerView.Adapter<PizzaDetailsAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    class ViewHolder(private val binding: ItemPizzaDetailsRecycleviewBinding) :
            RecyclerView.ViewHolder(binding.root){

        fun bind(item: String){
            binding.itemTextView.text = firstUpperCase(item)
        }

        private fun firstUpperCase(s: String): String =
                s.substring(0, 1).toUpperCase(Locale.ROOT) + s.substring(1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPizzaDetailsRecycleviewBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }
}