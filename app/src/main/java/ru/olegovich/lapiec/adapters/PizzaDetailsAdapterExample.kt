//package ru.olegovich.lapiec.adapters
//
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import ru.olegovich.lapiec.adapters.base.BaseRecyclerViewAdapter
//import ru.olegovich.lapiec.ui.databinding.ItemPizzaDetailsRecycleviewBinding
//
//class PizzaDetailsAdapterExample(items: Array<String>)
//    : BaseRecyclerViewAdapter<String, ItemPizzaDetailsRecycleviewBinding>(items) {
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
//    : ViewHolder<ItemPizzaDetailsRecycleviewBinding> {
//
//        val binding = ItemPizzaDetailsRecycleviewBinding
//                .inflate(LayoutInflater.from(parent.context), parent, false)
//        return ViewHolder(binding)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder<ItemPizzaDetailsRecycleviewBinding>, position: Int) {
//        super.onBindViewHolder(holder, position)
//    }
//}