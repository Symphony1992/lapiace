package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.lapiec.ui.databinding.ItemFinalBinding
import ru.olegovich.lapiec.util.load

class FinalAdapter(var items: List<Menu>)
    : RecyclerView.Adapter<FinalAdapter.ViewHolder>() {

    fun isEmpty(): Boolean = items.isEmpty()

    override fun getItemCount(): Int = items.size

    class ViewHolder(private val binding: ItemFinalBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: Menu){
            with(binding){
                itemFinalPizzaImageView.load(item.metaData.gallery[0])
                itemFinalTitleTextView.text = item.title.rendered
                itemFinalQuantityTextView.text = item.counter.toString()
                var sum = 0
                item.modifiers.forEach{ sum += it.metaData.price }
                itemFinalSumTextView.text = (item.counter * (item.metaData.price + sum)).toString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFinalBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }
}