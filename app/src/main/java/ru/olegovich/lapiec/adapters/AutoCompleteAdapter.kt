package ru.olegovich.lapiec.adapters

import android.content.Context
import android.graphics.Typeface
import android.text.style.CharacterStyle
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.PlacesClient
import ru.olegovich.domain.data.entity.enums.TAG

class AutoCompleteAdapter(
        context: Context,
        private var placesClient: PlacesClient,
        latLngBounds: LatLngBounds,
        resource: Int = android.R.layout.simple_expandable_list_item_1,
        textViewResourceId: Int = android.R.id.text1)
    : ArrayAdapter<AutocompletePrediction>(context, resource, textViewResourceId) {

    private val style: CharacterStyle = StyleSpan(Typeface.BOLD)
    lateinit var resultList: List<AutocompletePrediction>
    private val tempResult = mutableListOf<AutocompletePrediction>()
    private var latLngBounds: RectangularBounds = RectangularBounds.newInstance(latLngBounds)

    override fun getView(
            position: Int, convertView: View?, parent: ViewGroup)
    = super.getView(position, convertView, parent).apply {
        val item = getItem(position)
        val autoTextView: TextView = findViewById(android.R.id.text1)
        autoTextView.text = item.getPrimaryText(style)
    }

    override fun getCount(): Int = resultList.size

    override fun getItem(position: Int): AutocompletePrediction = resultList[position]

    override fun getFilter(): Filter = object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                constraint?.let {
                    resultList = getAutoComplete(constraint)
                    resultList.let {
                        results.values = resultList
                        results.count = resultList.size
                    }
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults) {
                if (results.count > 0) notifyDataSetChanged() else notifyDataSetInvalidated()
            }

            override fun convertResultToString(resultValue: Any): CharSequence? {
                return if (resultValue is AutocompletePrediction) resultValue.getPrimaryText(null)
                else super.convertResultToString(resultValue)
            }
        }

    /**
     * Сам запрос с нужными параметрами + логирование
     */

    private fun getAutoComplete(constraint: CharSequence): List<AutocompletePrediction> {
        val token = AutocompleteSessionToken.newInstance()
        val request = FindAutocompletePredictionsRequest
                .builder()
                .setTypeFilter(TypeFilter.ADDRESS)
                .setLocationBias(latLngBounds)
                .setCountry("UA")
                .setSessionToken(token)
                .setQuery(constraint.toString())
                .build()
        placesClient
                .findAutocompletePredictions(request)
                .addOnSuccessListener {
                    it.autocompletePredictions.forEach { item ->
                        Log.i(TAG, item.placeId)
                        Log.i(TAG, item.getPrimaryText(null).toString())
                    }
                    tempResult.clear()
                    tempResult.addAll(it.autocompletePredictions)
                }
                .addOnFailureListener {
                    if (it is ApiException) Log.e(TAG, "Place not found: " + it.statusCode)
                }
        return tempResult
    }

//    private val CharSequence.requestBuilder get() = FindAutocompletePredictionsRequest
//            .builder()
//            .setTypeFilter(TypeFilter.ADDRESS)
//            .setLocationBias(latLngBounds)
//            .setCountry("UA")
//            .setSessionToken(AutocompleteSessionToken.newInstance())
//            .setQuery(toString())
//            .build()

}
