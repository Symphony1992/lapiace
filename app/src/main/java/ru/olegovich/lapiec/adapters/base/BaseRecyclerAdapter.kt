package ru.olegovich.lapiec.adapters.base

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter : RecyclerView.Adapter<ViewHolder>() {

    abstract fun layoutRes(viewType: Int): Int
    abstract fun ViewHolder.onCreateView(position: Int)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int) = ViewHolder(parent, layoutRes(viewType))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onCreateView(position)

}