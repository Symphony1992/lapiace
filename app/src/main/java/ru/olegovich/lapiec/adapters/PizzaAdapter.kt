package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.lapiec.ui.R
import ru.olegovich.lapiec.ui.databinding.ItemPizzaRecycleviewBinding
import ru.olegovich.lapiec.util.fromHtml
import ru.olegovich.lapiec.util.load
import ru.olegovich.lapiec.util.text
import java.util.concurrent.atomic.AtomicInteger

class PizzaAdapter(val items: List<Menu>) : RecyclerView.Adapter<PizzaAdapter.ViewHolder>() {

    private var listener: OnItemClickListener? = null

    fun setListener (listener: OnItemClickListener){ this.listener = listener }

    class ViewHolder(private val binding: ItemPizzaRecycleviewBinding) :
            RecyclerView.ViewHolder(binding.root){

        fun bind(item: Menu, listener: OnItemClickListener){
            val count = AtomicInteger(item.counter)
            with(binding){
                quantityTextView.text = item.counter.toString()
                pizzaImageView.load(item.metaData.gallery[0])
                pizzaPriceTextView.text = item.metaData.price.toString()
                pizzaNameTextView.text = item.title.rendered.fromHtml()
                pizzaDetailsTextView.text = item.excerpt.rendered.fromHtml()
                when (item.metaData.cat){
                    "pizza" -> {
                        sizePizzaTextView.text(R.string.sm, item.metaData.width.fromHtml())
                        weightPizzaTextView.text(R.string.sm, item.metaData.weight.fromHtml())
                    }
                    "salad" -> {
                        sizePizzaTextView.text(R.string.gram, item.metaData.weight.fromHtml())
                    }
                }
                pizzaCardView.setOnClickListener { listener?.onItemClick(item) }
                addToCartImageButton.setOnClickListener {
                    listener.onChangeItemClick(item, true)
                    quantityTextView.text = count.incrementAndGet().toString()
                }
                removeFromCartImageButton.setOnClickListener {
                    if (count.get() > 0) {
                        listener.onChangeItemClick(item, false)
                        quantityTextView.text = count.decrementAndGet().toString()
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = items.size

    interface OnItemClickListener {
        fun onItemClick(item: Menu)
        fun onChangeItemClick(item: Menu, isAdd: Boolean)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPizzaRecycleviewBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        listener?.let { holder.bind(item, it) } // TODO: 20.04.2021 check
    }

}