package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_add_modifier.*
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.lapiec.ui.R
import ru.olegovich.lapiec.ui.databinding.ItemAddModifierBinding
import ru.olegovich.lapiec.util.load
import ru.olegovich.lapiec.util.text
import java.util.concurrent.atomic.AtomicBoolean

class AddModifierAdapter(private val items: List<Modifier>,
                         private val listener: OnItemClickListener
) : RecyclerView.Adapter<AddModifierAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    interface OnItemClickListener {
        fun onItemClick(item: Modifier)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemAddModifierBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }


    class ViewHolder(private val binding: ItemAddModifierBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: Modifier, listener: OnItemClickListener){
            val isCheckedAB = AtomicBoolean(item.isSelected)
            with(binding){
                modifierTitleTextView.text = item.title.rendered.toString()
                modifierWeightTextView.text(R.string.gram, item.metaData.weight)
                modifierPriceTextView.text = item.metaData.price.toString()
                initCard(isCheckedAB.get())
                modifierImageView.load(item.metaData.mobImg)
                modifierCardview.setOnClickListener {
                    listener.onItemClick(item)
                    isCheckedAB.set(!isCheckedAB.get())
                    initCard(isCheckedAB.get())
                }
            }
        }

        private fun initCard(bool: Boolean) {
            with(binding){
                modifierCardview.cardElevation = if (bool) 10F else 2F
                modifierCancelButton.visibility = if (bool) View.VISIBLE else View.INVISIBLE
            }
        }

    }

}

