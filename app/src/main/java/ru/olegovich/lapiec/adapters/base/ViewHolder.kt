package ru.olegovich.lapiec.adapters.base

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

class ViewHolder(
        parent: ViewGroup,
        @LayoutRes layoutRes: Int
): RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)),
LayoutContainer{
    override val containerView = itemView
    val context: Context get() = itemView.context
}