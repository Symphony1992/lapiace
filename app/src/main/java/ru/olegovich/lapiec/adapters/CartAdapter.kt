package ru.olegovich.lapiec.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import kotlinx.android.synthetic.main.item_cart.*
import kotlinx.android.synthetic.main.item_cart.view.*
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.lapiec.ui.databinding.ItemCartBinding
import ru.olegovich.lapiec.util.load
import java.util.*

class CartAdapter(var listener: OnItemClickListener, var items: MutableList<Menu>) :
        RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    fun changeItem(item: Menu) = items.forEachIndexed { i, it ->
        if (it.id == item.id) {
            it.counter = item.counter
            notifyItemChanged(i)
        }
    }


    fun removeItem(item: Menu, position: Int) {
        items.remove(item)
        notifyItemRemoved(position)
    }

    fun isEmpty(): Boolean = items.isEmpty()

    override fun getItemCount(): Int = items.size

    interface OnItemClickListener {
        fun onDeleteClick(item: Menu, position: Int)
        fun onChangeItemClick(item: Menu, isAdd: Boolean, position: Int)
    }

    class ViewHolder(val binding: ItemCartBinding) : RecyclerView.ViewHolder(binding.root){

        private var isStart = false

        fun bind(item: Menu, listener: OnItemClickListener){
            var s = ""
            var sum = 0
            with(binding){
                cartNameTextView.text = item.title.rendered
                cartImageView.load(item.metaData.gallery[0])
                item.modifiers.forEachIndexed { i, modifier ->
                    val title = modifier.title.rendered.toLowerCase(Locale.getDefault())
                    s = if (i == 0) title else "$s, $title"
                    sum += modifier.metaData.price
                }
                modifiersToPizzaTextView.text = s

                cartPriceTextView.text = (item.counter * (item.metaData.price + sum)).toString()

                addInCartImageButton.setOnClickListener {
                    listener.onChangeItemClick(item, true, bindingAdapterPosition)
                }
                removeInCartImageButton.setOnClickListener {
                    listener.onChangeItemClick(item, false, bindingAdapterPosition)
                }
                cartQuantityTextView.text = item.counter.toString()

                if (item.modifiers.isEmpty()){
                    additionsTextView.isVisible = false
                    cartRotateArrow.isVisible = false
                }

                cartRotateArrow.setOnClickListener {
                    if (!isStart) rotateArrow(0, 90, this)
                    else rotateArrow(90,0, this)
                }
            }
        }

        private fun rotateArrow(start: Int, end: Int, binding: ItemCartBinding) {
            val animation: Animation = RotateAnimation(
                    start.toFloat(),
                    end.toFloat(),
                    (binding.cartRotateArrow.width / 2).toFloat(),
                    (binding.cartRotateArrow.width / 2).toFloat()).apply {
                duration = 250
                fillAfter = true
            }
            binding.cartRotateArrow.startAnimation(animation)
            binding.modifiersToPizzaTextView.isVisible = !isStart
            isStart = !isStart
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCartBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, listener)

        holder.binding.deleteImageButton.setOnClickListener {
            listener.onDeleteClick(item, position)
            removeItem(item, position)
        }

    }
}
