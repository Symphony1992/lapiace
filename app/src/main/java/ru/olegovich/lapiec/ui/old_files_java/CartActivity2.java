/**package ru.olegovich.lapiec.ui.old_files_java;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.domain.mvp.cart.*;
import ru.olegovich.lapiec.CartAdapter;
import ru.olegovich.lapiec.ui.FinalActivity;
import ru.olegovich.lapiec.ui.R;

import static ru.olegovich.domain.data.Constants.FINAL_ACTIVITY;

public class CartActivity2 extends BaseActivityMvp2<CartMvp.Presenter, CartMvp.View>
        implements CartMvp.View, CartAdapter.OnItemClickListener{

    private View include;
    private CartAdapter adapter;
    private RecyclerView recyclerView;
    private TextView sumTextView;

    @Override
    public CartMvp.Presenter getPresenter() {
        return new CartPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.activity_cart;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ImageButton backImageButton = findViewById(R.id.back_image_button);
        Button buyButton = findViewById(R.id.buy_button);
        include = findViewById(R.id.include_cart);
        recyclerView = findViewById(R.id.cart_recycler_view);
        sumTextView = findViewById(R.id.sum_text_view);

        backImageButton.setOnClickListener(v -> presenter.onBackImageButtonClick());
        buyButton.setOnClickListener(v -> presenter.onBuyButtonClick());

        presenter.menuListMenu((List<Menu>) getIntent().getSerializableExtra("Menu"));

    }

    @Override
    public void onBackPressed() {
        presenter.onBackClick();
        super.onBackPressed();
    }

    @Override
    public void onDeleteClick(Menu item, int position) {
        presenter.onDeleteMenuItem(item, position);
        presenter.onMenuChanged();
    }

    @Override
    public void onChangeItemClick(Menu item, boolean isAdd, int position) {
        presenter.onChangeCounter(item, isAdd, position);
    }

    @Override
    public void removeItem(Menu item, int position) {
        adapter.removeItem(item, position);
    }

    @Override
    public void showMenuList(List<Menu> menu) {
        adapter = new CartAdapter(menu, this);
        presenter.onMenuChanged();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackToMain(List<Menu> menu) {
        Intent intent = new Intent();
        intent.putExtra("Menu", (Serializable) menu);
        setResult(RESULT_OK, intent);
    }

    @Override
    public void goToMainActivity() {
        presenter.onBackClick();
        finish();
    }

    @Override
    public void setVisibilityInclude() {
        if(adapter == null || adapter.isEmpty()) include.setVisibility(View.VISIBLE);
        else include.setVisibility(View.GONE);
    }

    @Override
    public void changeItem(Menu item) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showSum(int sum) {
        sumTextView
                .setText(sumTextView.getContext().getString(R.string.sum_grn, String.valueOf(sum)));
    }

    @Override
    public void openFinalActivity(List<Menu> menu) {
        if (menu.size() > 0){
            startFinalActivity(menu);
        } else {
            showOpenErrorToast();
        }
    }

    private void showOpenErrorToast() {
        Toast toast = Toast.makeText(this, R.string.open_final_error, Toast.LENGTH_LONG);
        TextView textToast = toast.getView().findViewById(android.R.id.message);
        if (textToast != null){
            textToast.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    private void startFinalActivity(List<Menu> menu) {
        Intent intent = new Intent(this, FinalActivity.class);
        intent.putExtra(FINAL_ACTIVITY, (Serializable) menu);
        startActivity(intent);
    }

}*/
