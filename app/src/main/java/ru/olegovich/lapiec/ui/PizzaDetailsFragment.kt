package ru.olegovich.lapiec.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import com.hannesdorfmann.fragmentargs.annotation.Arg
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.pizza_details_screen.PizzaDetailsMvp
import ru.olegovich.domain.mvp.pizza_details_screen.PizzaDetailsPresenter
import ru.olegovich.lapiec.adapters.AddedModifierAdapter
import ru.olegovich.lapiec.adapters.PizzaDetailsAdapter
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.PizzaDetailsFragmentBinding
import ru.olegovich.lapiec.util.load
import ru.olegovich.lapiec.util.text

class PizzaDetailsFragment : BaseViewBindFragmentMvp<
            PizzaDetailsMvp.Presenter,
            PizzaDetailsMvp.View,
            PizzaDetailsFragmentBinding>(),
        PizzaDetailsMvp.View,
        AddedModifierAdapter.OnItemClickListener{

    @Arg lateinit var menuItem: Menu
    private val addedModifierAdapter = AddedModifierAdapter(this)
    private lateinit var sendDataListener: SendDataListener

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> PizzaDetailsFragmentBinding = PizzaDetailsFragmentBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(ADD_MODIFIER_TO_PIZZA_BUNDLE_KEY) { _, bundle: Bundle ->
            val result = bundle.getSerializable(MODIFIER_ITEM_TRANSFER)
            presenter.onResult(result as MutableList<Modifier>) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreated(menuItem)
        with(binding){
            addModifierButton.setOnClickListener { presenter.onAddModifierButtonClick() }
            pizzaAddToCartButton.setOnClickListener { presenter.onAddToCartButtonClick() }
            minusCounterImageButton.setOnClickListener { presenter.onMinusButtonClick() }
            plusCounterImageButton.setOnClickListener { presenter.onPlusButtonClick() }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) sendDataListener = context
    }

    override fun presenter(): PizzaDetailsMvp.Presenter = PizzaDetailsPresenter()

    override fun showPizzaComponent(words: Array<String>) {
        binding.pizzaDetailsItemRecycleview.adapter = PizzaDetailsAdapter(words)
    }

    override fun showPizzaTitle(title: String) {
        binding.pizzaDetailsTitleTextView.text = title
    }

    override fun showPizzaWidth(width: String) {
        binding.pizzaDetailsSizeTextView.text(R.string.sm, width)
    }

    override fun showModifierAdapter(modifierItemTransfer: MutableList<Modifier>) {
        addedModifierAdapter.updateItem(modifierItemTransfer)
        binding.pizzaModifierItemRecyclerview.adapter = addedModifierAdapter
        setChosenAdditionsView(modifierItemTransfer.isNotEmpty())
    }

    override fun showPizzaWeight(weight: String) = binding
            .pizzaDetailsWeightTextView.text(R.string.gram, weight)

    override fun showPizzaImage(pizzaImageLink: String) {
        binding.pizzaDetailsImageView.load(pizzaImageLink)
    }

    override fun showAddModifierFragment(modifiers: List<Modifier>) {
        parentFragmentManager
                .beginTransaction()
                .replace(R.id.pager_container, AddModifierFragmentBuilder(modifiers).build())
                .addToBackStack(null)
                .commit()
    }

    override fun showModifierSum(sum: Int) {
        binding.modifiersSumTextView.text = sum.toString()
    }

    override fun showTotalSum(sum: Int) {
        binding.totalSumTextView.text = sum.toString()
    }

    override fun removeItem(item: Modifier, position: Int) {
        addedModifierAdapter.removeItem(item, position)
        if (addedModifierAdapter.isEmpty()) setChosenAdditionsView(false)
    }

    override fun addMenuToCart(item: Menu) = sendDataListener.sendData(item)

    override fun showCounter(counter: Int) {
        binding.counterTextView.text = counter.toString()
    }

    override fun setChosenAdditionsView(vis: Boolean) {
        binding.chosenAdditionsTextView.isVisible = vis
    }

    override fun onItemClick(item: Modifier, position: Int) = presenter.onItemClick(item, position)

}