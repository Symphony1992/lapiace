/**package ru.olegovich.lapiec;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.lapiec.ui.R;

public class PizzaAdapter extends RecyclerView.Adapter<PizzaAdapter.ViewHolder> {

    private List<Menu> items;
    private OnItemClickListener listener;

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public PizzaAdapter(List<Menu> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public PizzaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_pizza_recycleview, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Menu item = items.get(position);
        AtomicInteger count = new AtomicInteger(item.getCounter());
        holder.quantityTextView.setText(String.valueOf(item.getCounter()));
        Glide
                .with(holder.pizzaImageView)
                .load(item.getMetaData().getGallery().get(0))
                .into(holder.pizzaImageView);
        holder.pizzaNameTextView.setText(Html.fromHtml(item.getTitle().getRendered()));
        holder.pizzaDetailsTextView.setText(Html.fromHtml(item.getExcerpt().getRendered()));
        holder.pizzaPriceTextView.setText(String.valueOf(item.getMetaData().getPrice()));
        if (item.getMetaData().getCat().equals("pizza")){
            holder.sizePizzaTextView
                    .setText(holder.sizePizzaTextView
                            .getContext()
                            .getString(R.string.sm, Html.fromHtml(item.getMetaData().getWidth())));
            holder.weightPizzaTextView
                    .setText(holder.weightPizzaTextView
                            .getContext()
                            .getString(R.string.sm, Html.fromHtml(item.getMetaData().getWeight())));
        }
        if (item.getMetaData().getCat().equals("salad")){
            holder.sizePizzaTextView
                    .setText(holder.sizePizzaTextView
                            .getContext()
                            .getString(R.string.gram, item.getMetaData().getWeight()));
        }
        holder.pizzaCardView.setOnClickListener(view -> {
            if (listener != null) listener.onItemClick(item);
        });
        holder.addToCartImageButton.setOnClickListener(v -> {
            if (listener != null) listener.onChangeItemClick(item, true);
            holder.quantityTextView.setText(String.valueOf(count.incrementAndGet()));
        });
        holder.removeFromCartImageButton.setOnClickListener(v -> {
            if (count.get() > 0) {
                if (listener != null) listener.onChangeItemClick(item, false);
                holder.quantityTextView.setText(String.valueOf(count.decrementAndGet()));
            }
        });
    }

    @Override
    public int getItemCount() { return items.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView pizzaImageView;
        ImageButton addToCartImageButton;
        ImageButton removeFromCartImageButton;
        TextView pizzaNameTextView;
        TextView pizzaDetailsTextView;
        TextView pizzaPriceTextView;
        TextView sizePizzaTextView;
        TextView weightPizzaTextView;
        TextView quantityTextView;
        CardView pizzaCardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pizzaImageView = itemView.findViewById(R.id.pizza_image_view);
            addToCartImageButton = itemView.findViewById(R.id.add_to_cart_image_button);
            removeFromCartImageButton = itemView.findViewById(R.id.remove_from_cart_image_button);
            pizzaNameTextView = itemView.findViewById(R.id.pizza_name_text_view);
            pizzaDetailsTextView = itemView.findViewById(R.id.pizza_details_text_view);
            pizzaPriceTextView = itemView.findViewById(R.id.pizza_price_text_view);
            sizePizzaTextView = itemView.findViewById(R.id.size_pizza_text_view);
            weightPizzaTextView = itemView.findViewById(R.id.weight_pizza_text_view);
            quantityTextView = itemView.findViewById(R.id.quantity_text_view);
            pizzaCardView = itemView.findViewById(R.id.pizza_card_view);
        }

    }

    public interface OnItemClickListener {
        void onItemClick(Menu item);
        void onChangeItemClick(Menu item, boolean isAdd);
    }

}*/