package ru.olegovich.lapiec.ui

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import ru.olegovich.domain.data.entity.enums.FINAL_ACTIVITY
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.cart.CartMvp
import ru.olegovich.domain.mvp.cart.CartPresenter
import ru.olegovich.lapiec.adapters.CartAdapter
import ru.olegovich.lapiec.ui.base.BaseViewBindActivityMvp
import ru.olegovich.lapiec.ui.databinding.ActivityCartBinding
import ru.olegovich.lapiec.util.intent
import ru.olegovich.lapiec.util.text
import ru.olegovich.lapiec.util.toast
import java.io.Serializable

class CartActivity : BaseViewBindActivityMvp<
            CartMvp.Presenter,
            CartMvp.View,
            ActivityCartBinding>(),
        CartMvp.View,
        CartAdapter.OnItemClickListener {

    private lateinit var adapter: CartAdapter

    override val bindingInflater: (LayoutInflater) -> ActivityCartBinding =
            ActivityCartBinding::inflate

    override var isIncludeCartVisible: Boolean
        get() = binding.includeCart.includeCart.isVisible
        set(value) { binding.includeCart.includeCart.isVisible = value }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(binding){
            backImageButton.setOnClickListener { presenter.onBackImageButtonClick() }
            buyButton.setOnClickListener { presenter.onBuyButtonClick() }
        }
        presenter.menuListMenu((intent.getSerializableExtra("Menu") as? List<Menu>).orEmpty())
    }

    override fun onBackPressed() {
        presenter.onBackClick()
        super.onBackPressed()
    }

    override fun presenter(): CartMvp.Presenter = CartPresenter()

    override fun removeItem(item: Menu, position: Int) = adapter.removeItem(item, position)

    override fun showMenuList(menu: MutableList<Menu>) {
        adapter = CartAdapter(this, menu)
        binding.cartRecyclerView.adapter = adapter
    }

    override fun onBackToMain(menu: List<Menu>) = setResult(
            RESULT_OK, Intent().putExtra("Menu", menu as Serializable)
    )

    override fun goToMainActivity() {
        presenter.onBackClick()
        finish()
    }

    override fun changeItem(item: Menu) = adapter.changeItem(item)

    override fun showSum(sum: Int) = binding.sumTextView.text(R.string.sum_grn, sum.toString())

    override fun showOpenErrorToast() { // TODO: 17.04.2021 custom toasts deprecated/make motion toast
       Toast.makeText(
               this,
               R.string.open_final_error,
               Toast.LENGTH_LONG
       ).let {
           it.view?.findViewById<TextView>(android.R.id.message)?.run { gravity = Gravity.CENTER }
           it.show()
       }
    }

    override fun startFinalActivity(menu: List<Menu>) = startActivity(
            intent(FinalActivity::class.java)
                    .putExtra(FINAL_ACTIVITY, menu as Serializable))

    override fun onDeleteClick(item: Menu, position: Int) = presenter
            .onDeleteMenuItem(item, position)

    override fun onChangeItemClick(item: Menu, isAdd: Boolean, position: Int) = presenter
            .onChangeCounter(item, isAdd, position)

}

