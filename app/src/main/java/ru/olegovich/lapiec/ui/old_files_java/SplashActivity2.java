/**package ru.olegovich.lapiec.ui.old_files_java;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import ru.olegovich.domain.mvp.splash_screen.SplashMvp;
import ru.olegovich.domain.mvp.splash_screen.SplashPresenter;
import ru.olegovich.lapiec.ui.CityActivity;
import ru.olegovich.lapiec.ui.MainActivity;
import ru.olegovich.lapiec.ui.R;

public class SplashActivity2 extends BaseActivityMvp2<SplashMvp.Presenter, SplashMvp.View> implements SplashMvp.View {

    @Override
    public SplashMvp.Presenter getPresenter() {
        return new SplashPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.onCityChecked();
        finish();
    }

    @Override
    public void goToCityActivity() {
        Intent intent = new Intent(this, CityActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}*/
