package ru.olegovich.lapiec.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.contacts_fragment.*
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.mvp.contacts_screen.ContactsMvp
import ru.olegovich.domain.mvp.contacts_screen.ContactsPresenter
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.AddModifierFragmentBinding
import ru.olegovich.lapiec.ui.databinding.ContactsFragmentBinding
import ru.olegovich.lapiec.util.MyUtil
import ru.olegovich.lapiec.util.MyUtil.*

class ContactsFragment : BaseViewBindFragmentMvp<
            ContactsMvp.Presenter,
            ContactsMvp.View,
            ContactsFragmentBinding>(),
        ContactsMvp.View {

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> ContactsFragmentBinding = ContactsFragmentBinding::inflate

    override fun presenter(): ContactsMvp.Presenter = ContactsPresenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.onCreated()
    }

    override fun startCallActivity() {
        val intent = Intent(Intent.ACTION_DIAL)
        Intent(Intent.ACTION_DIAL).data = Uri.parse(PHONE_NUMBER)
        binding.phoneImageButton.setOnClickListener { startActivity(intent) }
    }

    override fun startMapActivity() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(GEO_POSITION_OFFICE))
        intent.data = Uri.parse(MAPS_PACKAGE)
        binding.locationImageButton.setOnClickListener { startActivity(intent) }
    }

    override fun startEmailActivity() {
        val intent = openEmailIntent()
        intent.type = PLAIN_TEXT
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(EMAIL_ADDRESS))
        binding.emailImageButton.setOnClickListener { startActivity(intent) }
    }

    override fun startFacebookActivity() {
        binding.fbImageButton.setOnClickListener { startActivity(openFacebookIntent(context)) }
    }

    override fun startInstagramActivity() {
        binding.instImageButton.setOnClickListener { startActivity(openInstagramIntent()) }
    }

}