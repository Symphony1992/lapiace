package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.hannesdorfmann.fragmentargs.annotation.Arg
import ru.olegovich.domain.data.entity.enums.ADD_MODIFIER_TO_PIZZA_BUNDLE_KEY
import ru.olegovich.domain.data.entity.enums.MODIFIER_ITEM_TRANSFER
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.mvp.add_modifier.AddModifierMvp
import ru.olegovich.domain.mvp.add_modifier.AddModifierPresenter
import ru.olegovich.lapiec.adapters.AddModifierAdapter
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.AddModifierFragmentBinding
import ru.olegovich.lapiec.util.SerializableMutableListArgsBundle

class AddModifierFragment : BaseViewBindFragmentMvp<
            AddModifierMvp.Presenter,
            AddModifierMvp.View,
            AddModifierFragmentBinding>(),
        AddModifierMvp.View,
        AddModifierAdapter.OnItemClickListener {

    @Arg(bundler = SerializableMutableListArgsBundle::class)
    lateinit var modifiers: MutableList<Modifier>

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> AddModifierFragmentBinding = AddModifierFragmentBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreated(modifiers)
    }

    override fun presenter(): AddModifierMvp.Presenter = AddModifierPresenter()

    override fun showModifier(modifierList: List<Modifier>) {
        binding.addModifierRecyclerview.adapter = AddModifierAdapter(modifierList, this)
    }

    override fun setFragmentResult(newItem: List<Modifier>) {
        setFragmentResult(
                ADD_MODIFIER_TO_PIZZA_BUNDLE_KEY,
                bundleOf(MODIFIER_ITEM_TRANSFER to newItem))
    }

    override fun onItemClick(item: Modifier) = presenter.onItemClick(item)

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

}