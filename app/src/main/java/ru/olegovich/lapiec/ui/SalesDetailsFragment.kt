package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.fragmentargs.annotation.Arg
import kotlinx.android.synthetic.main.sales_details_fragment.*
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.mvp.sales_details_screen.SalesDetailsMvp
import ru.olegovich.domain.mvp.sales_details_screen.SalesDetailsPresenter
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.SalesDetailsFragmentBinding
import ru.olegovich.lapiec.util.fromHtml
import ru.olegovich.lapiec.util.load

class SalesDetailsFragment : BaseViewBindFragmentMvp<
            SalesDetailsMvp.Presenter,
            SalesDetailsMvp.View,
            SalesDetailsFragmentBinding>(),
        SalesDetailsMvp.View {

    @Arg lateinit var locationDatum: LocationDatum

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> SalesDetailsFragmentBinding = SalesDetailsFragmentBinding::inflate

    override fun presenter(): SalesDetailsMvp.Presenter = SalesDetailsPresenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreated(locationDatum)
    }

    override fun showImageSales(link: String) {
        binding.salesDetailsImageView.load(link)
    }

    override fun showDetailsSales(rendered: String) {
        binding.salesDetailsTextView.text = rendered.fromHtml()
    }
}