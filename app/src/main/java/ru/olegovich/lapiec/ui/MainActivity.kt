package ru.olegovich.lapiec.ui

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import androidx.activity.result.contract.ActivityResultContracts.*
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*
import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.data.entity.enums.City.*
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.PagerCounter.*
import ru.olegovich.domain.data.entity.enums.Status
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.main.MainMvp
import ru.olegovich.domain.mvp.main.MainPresenter
import ru.olegovich.lapiec.adapters.ViewPagerFragmentAdapter
import ru.olegovich.lapiec.ui.base.BaseViewBindActivityMvp
import ru.olegovich.lapiec.ui.databinding.ActivityMainBinding
import ru.olegovich.lapiec.util.hideKeyboard
import ru.olegovich.lapiec.util.*
import java.io.Serializable

class MainActivity : BaseViewBindActivityMvp<
            MainMvp.Presenter,
            MainMvp.View,
            ActivityMainBinding>(),
        MainMvp.View,
        SendDataListener {

    private var quantity: Int = 0
    private lateinit var shrink: ScaleAnimation

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding =
            ActivityMainBinding::inflate

    override fun presenter(): MainMvp.Presenter = MainPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val pagerAdapter = ViewPagerFragmentAdapter(this)
        pagerAdapter.addFragment(PagerFragmentBuilder(PAGER_SALES).build())
        pagerAdapter.addFragment(PagerFragmentBuilder(PAGER_MENU).build())
        pagerAdapter.addFragment(PagerFragmentBuilder(PAGER_INFO).build())

        with(binding){
            mainViewpager.apply {
                adapter = pagerAdapter
                isUserInputEnabled = false
                currentItem = 1
            }
            titleTextView.text(R.string.menu_button_item)
            bottomNavigationView.apply {
                selectedItemId = R.id.action_menu
                setOnNavigationItemSelectedListener {
                    when(it.itemId) {
                        R.id.action_sales -> {
                            mainViewpager.currentItem = 0
                            it.isChecked = true
                            titleTextView.text(R.string.sales_button_item)
                            true
                        }
                        R.id.action_menu -> {
                            mainViewpager.currentItem = 1
                            it.isChecked = true
                            titleTextView.text(R.string.menu_button_item)
                            true
                        }
                        R.id.action_info -> {
                            mainViewpager.currentItem = 2
                            it.isChecked = true
                            titleTextView.text(R.string.info_button_item)
                            true
                        }
                        else -> false
                    }
                }
            }

            cityButton.setOnClickListener {
                presenter.onCityButtonClick()
                it.hideKeyboard()
            }

            cartButton.setOnClickListener {
                presenter.onCartButtonClick()
                it.hideKeyboard()
            }

            backImageButton.setOnClickListener {
                presenter.onBackButtonClick()
                it.hideKeyboard()
            }
        }
        presenter.onCreated()

        supportFragmentManager.setFragmentResultListener(PAGER_TO_MAIN, this, { _, result ->
            presenter.onStatusResult(
                    result.getSerializable(STATUS_OF_VIEW) as Status,
                    result.getSerializable(ITEM_TITLE_TRANSFER) as Menu
            )
        })

    }

    override fun initButton(city: City) {
        with(binding){
            when(city) {
                LVIV -> cityButton.text(R.string.city_choose_lviv_radio_button)
                VINNYTSIA -> cityButton.text(R.string.city_choose_vinnitsa_radio_button)
                IVANO_FRANKIVSK -> cityButton.text(R.string.city_choose_ivanof_radio_button)
                LUTSK -> cityButton.text(R.string.city_choose_lutsk_radio_button)
                else -> Unit
            }
        }
    }

    override fun setVisibility(status: Status, titleCart: Menu) {
        with(binding){
            bottomNavigationView.isVisible = when (status) {
                INFO, MENU, PIZZA, SALES_DETAILS, SALES , VACANCY -> true
                else -> false
            }
            cityButton.isVisible = when (status) {
                INFO, MENU, SALES_DETAILS, SALES, VACANCY -> true
                else -> false
            }
            backImageButton.isVisible = when (status) {
                ABOUT, ADD_MODIFIER, CONTACTS, DELIVERY, PARTNERS, PIZZA -> true
                else -> false
            }
            cartButton.isVisible = when (status) {
                ABOUT, CONTACTS, DELIVERY, PARTNERS, SALES , VACANCY -> false
                else -> true
            }
            titleTextView.text = when (status) {
                ABOUT -> getString(R.string.title_info_about_us)
                ADD_MODIFIER -> getString(R.string.title_modifier)
                CONTACTS -> getString(R.string.title_info_contact)
                DELIVERY -> getString(R.string.title_info_delivery)
                INFO -> getString(R.string.info_button_item)
                MENU -> getString(R.string.menu_button_item)
                PARTNERS -> getString(R.string.title_info_partner)
                PIZZA_DETAILS -> titleCart.title.rendered
                PIZZA -> getString(R.string.pizza)
                SALES_DETAILS, SALES -> getString(R.string.sales_button_item)
                VACANCY -> getString(R.string.title_info_vacancy)
            }
        }
    }

    override fun showUpdatedList(menu: MutableList<Menu>) {
        startActivityResult.launch(intent(CartActivity::class.java).apply {
            putExtra("Menu", menu as Serializable)
        })
    }

    override fun onGoToCityActivity() {
        startActivity(intent(CityActivity::class.java))
        overridePendingTransition(R.anim.bottom_in, R.anim.top_out)
    }

    override fun back() = onBackPressed()

    override fun onBackPressed() {
        supportFragmentManager.fragments.forEach {
            if (it.isVisible && it.childFragmentManager.backStackEntryCount > 0) {
                it.childFragmentManager.popBackStack()
                return
            }
        }
        super.onBackPressed()
    }

    override fun onActivityResultToPizzaFragment(menu: List<Menu>) = updatePizzaFragment(menu)

    private fun updatePizzaFragment(menu: List<Menu>) {
        supportFragmentManager.fragments.forEach {
            if (it.isVisible && it is PagerFragment) it.updatePizzaFragment(menu)
        }
    }

    override fun showAnimationCart(quantity: Int) {
        if (shrink.isInitialized) binding.quantityCartImageView.startAnimation(shrink)
        this.quantity = quantity
    }

    override fun animationQuantityCart() {
        val grow = ScaleAnimation(0F, 1F, 1F, 1F,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5F,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5F)
        grow.duration = 200
        binding.quantityCartImageView.setImageResource(R.drawable.krug_cart)
        shrink = ScaleAnimation(1F, 0F, 1F, 1F,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5F,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5F)
        shrink.duration = 200
        shrink.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                if (quantity == 0) showVisibilityOfQuantity(false)
                else {
                    binding.apply {
                        quantityCartImageView.startAnimation(grow)
                        quantityCartTextView.text = quantity.toString()
                    }
                    showVisibilityOfQuantity(true)
                }
            }
        })
    }

    private fun showVisibilityOfQuantity(vis: Boolean) {
        with(binding){
            quantityCartImageView.isVisible= vis
            quantityCartTextView.isVisible = vis
        }
    }

    override fun showTotalSumCart(sum: Int) = cart_button.text(R.string.cart_total_sum, sum)

    override fun sendData(menuItem: Menu) = presenter.onResult(menuItem)

    private val startActivityResult = registerForActivityResult(StartActivityForResult()) { item ->
        if (item.resultCode == Activity.RESULT_OK) {
            (item.data?.getSerializableExtra("Menu") as? List<Menu>)?.let {
                presenter.menuList(it)
            }
        }
    }

}