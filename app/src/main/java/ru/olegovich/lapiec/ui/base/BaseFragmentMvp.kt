package ru.olegovich.lapiec.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.olegovich.domain.mvp.base.BaseMvp

abstract class BaseFragmentMvp<P : BaseMvp.BasePresenter<V>, V : BaseMvp.BaseView> : BaseFragment(),
        BaseMvp.BaseView {

    abstract fun presenter(): P
    protected lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = presenter()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = super.onCreateView(inflater, container, savedInstanceState)
        presenter.attachView(this as V)
        return v
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
}