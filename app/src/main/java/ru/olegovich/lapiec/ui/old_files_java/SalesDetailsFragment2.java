/**package ru.olegovich.lapiec.ui.old_files_java;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import ru.olegovich.domain.data.sales.LocationDatum;
import ru.olegovich.domain.mvp.sales_details_screen.SalesDetailsMvp;
import ru.olegovich.domain.mvp.sales_details_screen.SalesDetailsPresenter;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

@FragmentWithArgs
public class SalesDetailsFragment2 extends BaseFragmentMvp<SalesDetailsMvp.Presenter, SalesDetailsMvp.View>
        implements SalesDetailsMvp.View  {

    @Arg
    LocationDatum locationDatumItem;
    private ImageView salesDetailsImageView;
    private TextView salesDetailsTextView;

    @Override
    public SalesDetailsMvp.Presenter getPresenter() {
        return new SalesDetailsPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.sales_details_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        salesDetailsImageView = view.findViewById(R.id.sales_details_image_view);
        salesDetailsTextView = view.findViewById(R.id.sales_details_text_view);

        presenter.onCreated(locationDatumItem);
    }

    @Override
    public void showImageSales(String link) {
        Glide.with(this).load(link).into(salesDetailsImageView);
    }

    @Override
    public void showDetailsSales(String rendered) {
        salesDetailsTextView.setText(Html.fromHtml(rendered));
    }

}*/
