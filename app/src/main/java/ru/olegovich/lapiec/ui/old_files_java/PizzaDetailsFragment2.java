/**package ru.olegovich.lapiec.ui.old_files_java;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hannesdorfmann.fragmentargs.annotation.Arg;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import ru.olegovich.domain.data.modifiers.Modifier;
import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.domain.mvp.pizza_details_screen.PizzaDetailsMvp;
import ru.olegovich.domain.mvp.pizza_details_screen.PizzaDetailsPresenter;
import ru.olegovich.lapiec.AddedModifierAdapter;
import ru.olegovich.lapiec.PizzaDetailsAdapter;
import ru.olegovich.lapiec.ui.MainActivity;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.SendDataListener;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

import static ru.olegovich.domain.data.Constants.ADD_MODIFIER_TO_PIZZA_BUNDLE_KEY;
import static ru.olegovich.domain.data.Constants.MODIFIER_ITEM_TRANSFER;

public class PizzaDetailsFragment2 extends BaseFragmentMvp<PizzaDetailsMvp.Presenter, PizzaDetailsMvp.View>
        implements PizzaDetailsMvp.View, AddedModifierAdapter.OnItemClickListener {

    @Arg
    Menu menuItem;

    private TextView pizzaTitleTextView, pizzaSizeTextView, pizzaWeightTextView;
    private TextView modifiersSumTextView, totalSumTextView, chosenAdditions;
    private ImageView pizzaImageView;
    private RecyclerView pizzaDetailsRecyclerView;
    private RecyclerView addModifierRecyclerView;
    private final AddedModifierAdapter addedModifierAdapter = new AddedModifierAdapter(this);
    private TextView counterTextView;
    private SendDataListener sendDataListener;

    @Override
    public PizzaDetailsMvp.Presenter getPresenter() {
        return new PizzaDetailsPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.pizza_details_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getParentFragmentManager()
                .setFragmentResultListener(ADD_MODIFIER_TO_PIZZA_BUNDLE_KEY, this,(requestKey, result)
                        -> presenter.onResult(
                                (ArrayList<Modifier>) result.getSerializable(MODIFIER_ITEM_TRANSFER)));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pizzaTitleTextView = view.findViewById(R.id.pizza_details_title_text_view);
        pizzaSizeTextView = view.findViewById(R.id.pizza_details_size_text_view);
        pizzaWeightTextView = view.findViewById(R.id.pizza_details_weight_text_view);
        pizzaImageView = view.findViewById(R.id.pizza_details_image_view);
        pizzaDetailsRecyclerView = view.findViewById(R.id.pizza_details_item_recycleview);
        addModifierRecyclerView = view.findViewById(R.id.pizza_modifier_item_recyclerview);
        Button addModifierButton = view.findViewById(R.id.add_modifier_button);
        modifiersSumTextView = view.findViewById(R.id.modifiers_sum_text_view);
        totalSumTextView = view.findViewById(R.id.total_sum_text_view);
        chosenAdditions = view.findViewById(R.id.chosen_additions_text_view);
        Button pizzaAddToCartButton = view.findViewById(R.id.pizza_add_to_cart_button);
        counterTextView = view.findViewById(R.id.counterTextView);
        ImageButton minusCounterButton = view.findViewById(R.id.minusCounterImageButton);
        ImageButton plusCounterButton = view.findViewById(R.id.plusCounterImageButton);

        presenter.onCreated(menuItem);

        addModifierButton.setOnClickListener(v -> presenter.onAddModifierButtonClick());
        pizzaAddToCartButton.setOnClickListener(v -> presenter.onAddToCartButtonClick());
        minusCounterButton.setOnClickListener(v -> presenter.onMinusButtonClick());
        plusCounterButton.setOnClickListener(v -> presenter.onPlusButtonClick());

    }

    @Override
    public void showPizzaTitle(String rendered) {
        pizzaTitleTextView.setText(rendered);
    }

    @Override
    public void showPizzaWidth(String width) {
        pizzaSizeTextView.setText(getContext().getString(R.string.sm, width));
    }

    @Override
    public void showPizzaComponent(String[] words) {
        PizzaDetailsAdapter pizzaDetailsAdapter = new PizzaDetailsAdapter(words);
        pizzaDetailsRecyclerView.setAdapter(pizzaDetailsAdapter);
    }

    @Override
    public void showModifierAdapter(ArrayList<Modifier> modifierItemTransfer) {
        addedModifierAdapter.update(modifierItemTransfer);
        addModifierRecyclerView.setAdapter(addedModifierAdapter);
        setChosenAdditionsView(!modifierItemTransfer.isEmpty());
    }

    @Override
    public void showPizzaWeight(String weight) {
        pizzaWeightTextView.setText(getContext().getString(R.string.gram, weight));
    }

    @Override
    public void showPizzaImage(String s) {
        Glide.with(this).load(s).into(pizzaImageView);
    }

    @Override
    public void showModifierSum(int sum) {
        modifiersSumTextView.setText(String.valueOf(sum));
    }

    @Override
    public void showTotalSum(int sum) {
        totalSumTextView.setText(String.valueOf(sum));
    }

    @Override
    public void showAddModifierFragment(@NotNull List<? extends Menu> modifiers) {
        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.pager_container, new AddModifierFragmentBuilder(modifiers).build())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onItemClick(Modifier item, int position) {
        presenter.onItemClick(item, position);
    }

    @Override
    public void removeItem(Modifier item, int position) {
        addedModifierAdapter.removeItem(item, position);
        if (addedModifierAdapter.isEmpty()) setChosenAdditionsView(false);
    }

    @Override
    public void addMenuToCart(Menu item) {
        sendDataListener.sendData(item);
    }

    @Override
    public void showCounter(int counter) {
        counterTextView.setText(String.valueOf(counter));
    }

    @Override
    public void setChosenAdditionsView(boolean vis) {
        if (vis) chosenAdditions.setVisibility(View.VISIBLE);
        else chosenAdditions.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            sendDataListener = (SendDataListener) context;
        }
    }
}*/
