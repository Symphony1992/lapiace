/**package ru.olegovich.lapiec.ui.old_files_java;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import ru.olegovich.domain.mvp.menu_screen.MenuMvp;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

import static ru.olegovich.domain.data.enums.Counter.DRINK;
import static ru.olegovich.domain.data.enums.Counter.PIZZA;
import static ru.olegovich.domain.data.enums.Counter.SALAD;

public class MenuFragment2 extends BaseFragmentMvp<MenuMvp.Presenter, MenuMvp.View>
        implements MenuMvp.View {

    @Override
    public MenuMvp.Presenter getPresenter() {
        return new MenuPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.menu_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView menuPizzaImageView = view.findViewById(R.id.menu_pizza_image_view);
        ImageView menuSaladImageView = view.findViewById(R.id.menu_salad_image_view);
        ImageView menuDrinkImageView = view.findViewById(R.id.menu_drink_image_view);

        presenter.onCreated();

        menuPizzaImageView.setOnClickListener(v -> presenter.onMenuPizzaImageViewClick());
        menuSaladImageView.setOnClickListener(v -> presenter.onSaladPizzaImageViewClick());
        menuDrinkImageView.setOnClickListener(v -> presenter.onDrinkPizzaImageViewClick());
    }

    @Override
    public void startPizzaFragment() {
        start(new PizzaFragmentBuilder(PIZZA).build());
    }

    @Override
    public void startSaladFragment() {
        start(new PizzaFragmentBuilder(SALAD).build());
    }

    @Override
    public void startDrinkFragment() {
        start(new PizzaFragmentBuilder(DRINK).build());
    }

    private void start(Fragment fragment){
        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.pager_container, fragment)
                .addToBackStack(null)
                .commit();
    }

}*/
