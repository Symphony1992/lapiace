package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import com.hannesdorfmann.fragmentargs.annotation.Arg
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.PagerCounter
import ru.olegovich.domain.data.entity.enums.PagerCounter.*
import ru.olegovich.domain.data.entity.enums.Status
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.lapiec.ui.base.BaseViewBindFragment
import ru.olegovich.lapiec.ui.databinding.FragmentPagerBinding

class PagerFragment : BaseViewBindFragment<FragmentPagerBinding>() {

    @Arg lateinit var count: PagerCounter

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> FragmentPagerBinding = FragmentPagerBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(FRAGMENT_TO_PAGER) { _, bundle ->
            setFragmentResult(
                    bundle.getSerializable(STATUS_OF_VIEW) as Status,
                    bundle.getSerializable(ITEM_TITLE_TRANSFER) as Menu)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (count){
            PAGER_SALES -> start(SalesFragment())
            PAGER_MENU -> start(MenuFragment())
            PAGER_INFO -> start(InfoFragment())
        }
    }

    private fun start(fragment: Fragment) = childFragmentManager
            .beginTransaction()
            .replace(R.id.pager_container, fragment)
            .commit()

    private fun setFragmentResult(status: Status, item: Menu) {
        lateinit var bundle: Bundle
        bundle.putSerializable(STATUS_OF_VIEW, status)
        bundle.putSerializable(ITEM_TITLE_TRANSFER, item)
        parentFragmentManager.setFragmentResult(PAGER_TO_MAIN, bundle)
    }

    fun updatePizzaFragment(menu: List<Menu>) = childFragmentManager
            .fragments
            .forEach { if (it.isVisible && it is PizzaFragment) it.sendDataToPizzaFragment(menu) }

}
