/**package ru.olegovich.lapiec;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ru.olegovich.domain.data.vacancies.Vacancy;
import ru.olegovich.lapiec.ui.R;

public class VacancyAdapter extends RecyclerView.Adapter <VacancyAdapter.ViewHolder> {

    private List<Vacancy> items;

    public VacancyAdapter(List<Vacancy> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_vacancy, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Vacancy item = items.get(position);
        Glide
                .with(holder.vacancyItemImageView)
                .load(item.getMetaData().getImageUrl())
                .into(holder.vacancyItemImageView);
        holder.vacancyNameItemTextView.setText(Html.fromHtml(item.getTitle().getRendered()));

    }

    @Override
    public int getItemCount() {   return items.size();    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView vacancyNameItemTextView;
        ImageView vacancyItemImageView;
        Button vacancyDetailsItemButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            vacancyNameItemTextView = itemView.findViewById(R.id.vacancy_name_item_text_view);
            vacancyItemImageView = itemView.findViewById(R.id.vacancy_item_image_view);
            vacancyDetailsItemButton = itemView.findViewById(R.id.vacancy_details_item_button);
        }
    }
}*/
