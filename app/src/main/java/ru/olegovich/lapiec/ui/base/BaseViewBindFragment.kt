package ru.olegovich.lapiec.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.hannesdorfmann.fragmentargs.FragmentArgs
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import java.io.Serializable

@FragmentWithArgs
abstract class BaseViewBindFragment<VB : ViewBinding> : Fragment(){

    private var _binding: ViewBinding? = null
    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> VB

    protected val binding: VB
        get() = _binding as VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FragmentArgs.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = bindingInflater.invoke(inflater, container, false)
        return requireNotNull(_binding).root
    }

    open fun setFragmentResultToActivity(key: String, requestKey: String, value: Serializable) {
        val result = Bundle()
        result.putSerializable(key, value)
        parentFragmentManager.setFragmentResult(requestKey, result)
    }

    open fun setFragmentResultToActivity(key: String,
                                         key2: String,
                                         requestKey: String,
                                         value: Serializable,
                                         value2: Serializable) {
        val result = Bundle().apply {
            putSerializable(key, value)
            putSerializable(key2, value2)
        }
        parentFragmentManager.setFragmentResult(requestKey, result)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}