/**package ru.olegovich.lapiec.ui.old_files_java;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hannesdorfmann.fragmentargs.annotation.Arg;

import java.util.List;

import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.lapiec.ui.InfoFragment;
import ru.olegovich.lapiec.ui.MenuFragment;
import ru.olegovich.lapiec.ui.PizzaFragment;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.SalesFragment;
import ru.olegovich.lapiec.ui.base.BaseFragment;

import static ru.olegovich.domain.data.Constants.FRAGMENT_TO_PAGER;
import static ru.olegovich.domain.data.Constants.ITEM_TITLE_TRANSFER;
import static ru.olegovich.domain.data.Constants.PAGER_TO_MAIN;
import static ru.olegovich.domain.data.Constants.STATUS_OF_VIEW;

public class PagerFragment2 extends BaseFragment {

    @Arg
    Counter count;

    @Override
    public int layoutRes() {
        return R.layout.fragment_pager;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getChildFragmentManager().setFragmentResultListener(FRAGMENT_TO_PAGER,this,(requestKey, result)
                -> {
                    setFragmentResult(
                            (Status) result.getSerializable(STATUS_OF_VIEW),
                            (Menu) result.getSerializable(ITEM_TITLE_TRANSFER));
                });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        switch (count) {
            case PAGER_SALES:
                start(new SalesFragment());
                break;
            case PAGER_MENU:
                start(new MenuFragment());
                break;
            case PAGER_INFO:
                start(new InfoFragment());
                break;
        }
    }

    private void start(Fragment fragment) {
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.pager_container, fragment)
                .commit();
    }

    public void setFragmentResult(Status status, Menu itemTitle) {
        Bundle result = new Bundle();
        result.putSerializable(STATUS_OF_VIEW, status);
        result.putSerializable(ITEM_TITLE_TRANSFER, itemTitle);
        getParentFragmentManager().setFragmentResult(PAGER_TO_MAIN, result);
    }

    public void updatePizzaFragment(List<Menu> menu) {
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            if (fragment.isVisible() && fragment instanceof PizzaFragment) {
                ((PizzaFragment) fragment).sendDataToPizzaFragment(menu);
            }
        }
    }

    enum  Counter{
        PAGER_SALES, PAGER_MENU, PAGER_INFO;
    }

}*/
