package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.include_no_internet.*
import kotlinx.android.synthetic.main.sales_fragment.*
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.mvp.sales_screen.SalesMvp
import ru.olegovich.domain.mvp.sales_screen.SalesPresenter
import ru.olegovich.lapiec.adapters.SalesAdapter
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.SalesFragmentBinding

class SalesFragment : BaseViewBindFragmentMvp<
            SalesMvp.Presenter,
            SalesMvp.View,
            SalesFragmentBinding>(),
        SalesMvp.View {

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> SalesFragmentBinding = SalesFragmentBinding::inflate

    override fun presenter(): SalesMvp.Presenter = SalesPresenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.onCreated()
        binding.includeNoInternet.noInternetTryAgainButton
                .setOnClickListener { presenter.onTryAgain() }
    }

    override fun showSales(locationDatum: List<LocationDatum>) {
        val adapter = SalesAdapter(locationDatum)
        adapter.onItemClickListener {
            parentFragmentManager
                    .beginTransaction()
                    .replace(R.id.pager_container, SalesDetailsFragmentBuilder(it).build())
                    .addToBackStack(null)
                    .commit()
        }
        binding.salesRecyclerView.adapter = adapter
    }

    override fun showError() = setVisibilityOfViews(salesRV = false, include = true)

    override fun reOpen() = setVisibilityOfViews(salesRV = true, include = false)

    private fun setVisibilityOfViews(salesRV: Boolean, include: Boolean) {
        with(binding){
            salesRecyclerView.isVisible = salesRV
            includeNoInternet.noInternetIncludeLayout.isVisible = include
        }
    }

}