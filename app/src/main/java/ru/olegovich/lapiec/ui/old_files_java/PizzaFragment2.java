/**package ru.olegovich.lapiec.ui.old_files_java;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hannesdorfmann.fragmentargs.annotation.Arg;

import java.util.List;

import ru.olegovich.domain.data.enums.Counter;
import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.domain.mvp.pizza.PizzaMvp;
import ru.olegovich.domain.mvp.pizza.PizzaPresenter;
import ru.olegovich.lapiec.PizzaAdapter;
import ru.olegovich.lapiec.ui.MainActivity;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.SendDataListener;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

public class PizzaFragment2 extends BaseFragmentMvp<PizzaMvp.Presenter, PizzaMvp.View>
        implements PizzaMvp.View, PizzaAdapter.OnItemClickListener{

    @Arg
    Counter counter;
    private RecyclerView recyclerView;
    private View include;
    private SendDataListener sendDataListener;

    @Override
    public PizzaMvp.Presenter getPresenter() {
        return new PizzaPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.pizza_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.pizza_recycler_view);
        include = view.findViewById(R.id.no_internet_pizza_include);
        Button tryAgain = view.findViewById(R.id.no_internet_try_again_button);

        presenter.onCreated(counter);

        tryAgain.setOnClickListener(v -> presenter.onTryAgainClick());

    }

    @Override
    public void showMenu(List<Menu> menuList) {
        PizzaAdapter adapter = new PizzaAdapter(menuList);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError() {
        recyclerView.setVisibility(View.GONE);
        include.setVisibility(View.VISIBLE);
    }

    @Override
    public void reOpen() {
        presenter.onCreated(counter);
        recyclerView.setVisibility(View.VISIBLE);
        include.setVisibility(View.GONE);
    }

    @Override
    public void sendData(Menu cartMenu) {
        sendDataListener.sendData(cartMenu);
    }

    @Override
    public void onItemClick(Menu item) {
        if (item.getMetaData().getCat().equals("pizza")) {
            PizzaFragment2.this.getParentFragmentManager()
                    .beginTransaction()
                    .replace(R.id.pager_container, new PizzaDetailsFragmentBuilder(item).build())
                    .addToBackStack(null)
                    .commit();
        } else if (item.getMetaData().getCat().equals("salad")) {
            showDialog(item);
        }
    }

    @Override
    public void onChangeItemClick(Menu item, boolean isAdd) {
        presenter.onChangeItemClick(item, isAdd);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            sendDataListener = (SendDataListener) context;
        }
    }

    public void sendDataToPizzaFragment(List<Menu> menuList) {
        presenter.onActivityResult(menuList);
    }

    private void showDialog(Menu item) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
        View layout = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        adb.setView(layout);
        ImageView imageView = layout.findViewById(R.id.alert_dialog_image_view);
        TextView titleTextView = layout.findViewById(R.id.alert_dialog_title_text_view);
        TextView contentTextView = layout.findViewById(R.id.alert_dialog_content_text_view);
        Glide.with(imageView).load(item.getMetaData().getGallery().get(0)).into(imageView);
        titleTextView.setText(Html.fromHtml(item.getTitle().getRendered()));
        contentTextView.setText(Html.fromHtml(item.getExcerpt().getRendered()));
        adb.create();
        adb.show();
    }

}*/
