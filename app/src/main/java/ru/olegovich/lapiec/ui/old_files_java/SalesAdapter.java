/**package ru.olegovich.lapiec;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.olegovich.domain.data.sales.LocationDatum;
import ru.olegovich.lapiec.ui.R;

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.ViewHolder> {

    private List<LocationDatum> items;
    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public SalesAdapter(List<LocationDatum> items) {
        this.items = items;
    }


    @NonNull
    @Override
    public SalesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_sales, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LocationDatum item = items.get(position);
            holder.salesItemTextView.setText(Html.fromHtml(item.getTitle().getRendered()));
            Glide
                .with(holder.salesItemImageView)
                .load(item.getMetaData().getMobImg())
                .into(holder.salesItemImageView);
            holder.salesItemButton.setOnClickListener(view -> {
                if (listener != null) listener.onItemClick(item);
            });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView salesItemImageView;
        TextView salesItemTextView;
        Button salesItemButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            salesItemImageView = itemView.findViewById(R.id.sales_item_image_view);
            salesItemTextView = itemView.findViewById(R.id.sales_item_text_view);
            salesItemButton = itemView.findViewById(R.id.sales_item_button);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(LocationDatum item);
    }
}*/
