/**package ru.olegovich.lapiec.ui.old_files_java;

import androidx.annotation.Nullable;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import ru.olegovich.domain.data.enums.City;
import ru.olegovich.domain.mvp.city.CityMvp;
import ru.olegovich.domain.mvp.city.CityPresenter;
import ru.olegovich.lapiec.ui.MainActivity;
import ru.olegovich.lapiec.ui.R;

public class CityActivity2 extends BaseActivityMvp2<CityMvp.Presenter, CityMvp.View>
        implements CityMvp.View{

    private RadioGroup cityChoseRadioGroup;

    @Override
    public CityMvp.Presenter getPresenter() {
        return new CityPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.activity_city;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Button applyButton = findViewById(R.id.apply_button);
        cityChoseRadioGroup = findViewById(R.id.city_choose_radio_group);

        presenter.onCreated();

        cityChoseRadioGroup.setOnCheckedChangeListener((radioGroup, idView) -> {
            switch (idView) {
                case R.id.city_lviv_radio_button:
                    presenter.onCitySelect(City.LVIV);
                    break;
                case R.id.city_vinnitsa_radio_button:
                    presenter.onCitySelect(City.VINNYTSIA);
                    break;
                case R.id.city_ivanof_radio_button:
                    presenter.onCitySelect(City.IVANO_FRANKIVSK);
                    break;
                case R.id.city_lutsk_radio_button:
                    presenter.onCitySelect(City.LUTSK);
                    break;
            }
        });
        applyButton.setOnClickListener(view -> presenter.onCityApplyClick());
    }

    @Override
    public void initRadioGroup(City city) {
        switch (city) {
            case NOT_SELECTED:
                break;
            case LVIV:
                cityChoseRadioGroup.check(R.id.city_lviv_radio_button);
                break;
            case VINNYTSIA:
                cityChoseRadioGroup.check(R.id.city_vinnitsa_radio_button);
                break;
            case IVANO_FRANKIVSK:
                cityChoseRadioGroup.check(R.id.city_ivanof_radio_button);
                break;
            case LUTSK:
                cityChoseRadioGroup.check(R.id.city_lutsk_radio_button);
                break;
        }
    }

    @Override
    public void showSelectedToast() {
        Toast.makeText(this, R.string.error_city, Toast.LENGTH_LONG).show();
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.top_in, R.anim.bottom_out);
    }

}*/