package ru.olegovich.lapiec.ui

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolygonOptions
import com.google.android.libraries.places.api.Places
import kotlinx.android.synthetic.main.delivery_fragment.*
import ru.olegovich.domain.data.entity.enums.MAP_VIEW_BUNDLE_KEY
import ru.olegovich.domain.data.entity.coordinates.ZonePath
import ru.olegovich.domain.mvp.delivery_screen.DeliveryMvp
import ru.olegovich.domain.mvp.delivery_screen.DeliveryPresenter
import ru.olegovich.lapiec.adapters.AutoCompleteAdapter
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.ContactsFragmentBinding
import ru.olegovich.lapiec.ui.databinding.DeliveryFragmentBinding


class DeliveryFragment : BaseViewBindFragmentMvp<
            DeliveryMvp.Presenter,
            DeliveryMvp.View,
            DeliveryFragmentBinding>(),
        DeliveryMvp.View,
        OnMapReadyCallback {

    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> DeliveryFragmentBinding = DeliveryFragmentBinding::inflate

    override fun presenter(): DeliveryMvp.Presenter = DeliveryPresenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapViewBundle = savedInstanceState?.getBundle(MAP_VIEW_BUNDLE_KEY)
        mapView = view.findViewById(R.id.mapView)
        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)
    }

    override fun showCoordinate(zonePath: ZonePath) {
        val latLngBounds = LatLngBounds.Builder()
        val polygonOptions = PolygonOptions()
                .fillColor(Color.parseColor(zonePath.color.replace("#", "#6E")))
                .strokeColor(setColor(zonePath.color))
                .strokeWidth(2F)
        zonePath.coordinates.forEach {
            polygonOptions.add(LatLng(it.lat, it.lng))
            latLngBounds.include(LatLng(it.lat, it.lng))
        }
        googleMap.addPolygon(polygonOptions)

        context?.let {
            Places.initialize(it, resources.getString(R.string.google_maps_api_key))
            val placesClient = Places.createClient(it)
            val adapter = AutoCompleteAdapter(it, placesClient, latLngBounds.build())
            binding.addressEditText.apply {
                setAdapter(adapter)
                setOnItemClickListener { _, _, pos, _ ->
                    presenter.sendLatLngRequest(adapter.resultList[pos].placeId) }
            }
        }

    }

    private fun setColor(color: String): Int = Color.parseColor(color)

    override fun setLatLng(lat: Double, lng: Double, zoom: Float) = googleMap
            .moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat,lng), zoom))

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        presenter.onCreated()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY) ?: Bundle().apply {
            putBundle(MAP_VIEW_BUNDLE_KEY, this)
        }
        mapView.onSaveInstanceState(mapViewBundle)
    }

    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }

    override fun onPause() {
        mapView.onResume()
        super.onPause()
    }

    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

}