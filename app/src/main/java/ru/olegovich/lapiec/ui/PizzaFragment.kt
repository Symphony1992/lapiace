package ru.olegovich.lapiec.ui

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.hannesdorfmann.fragmentargs.annotation.Arg
import ru.olegovich.domain.data.entity.enums.Counter
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.mvp.pizza.PizzaMvp
import ru.olegovich.domain.mvp.pizza.PizzaPresenter
import ru.olegovich.lapiec.adapters.PizzaAdapter
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.AlertDialogBinding
import ru.olegovich.lapiec.ui.databinding.PizzaFragmentBinding
import ru.olegovich.lapiec.util.fromHtml
import ru.olegovich.lapiec.util.load

class PizzaFragment : BaseViewBindFragmentMvp<
            PizzaMvp.Presenter,
            PizzaMvp.View,
            PizzaFragmentBinding>(),
        PizzaMvp.View,
        PizzaAdapter.OnItemClickListener {

    @Arg lateinit var counter: Counter
    lateinit var listener: SendDataListener

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> PizzaFragmentBinding = PizzaFragmentBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreated(counter)
        binding.includeNoInternet.noInternetIncludeLayout
                .setOnClickListener { presenter.onTryAgainClick() }
    }

    override fun presenter(): PizzaMvp.Presenter = PizzaPresenter()

    override fun showMenu(menuList: List<Menu>) {
        val adapter = PizzaAdapter(menuList)
        adapter.setListener(this)
        binding.pizzaRecyclerView.adapter = adapter
    }

    override fun showError() {
        with(binding) {
            pizzaRecyclerView.isVisible = false
            includeNoInternet.noInternetIncludeLayout.isVisible = true
        }
    }

    override fun reOpen() {
        presenter.onCreated(counter)
        with(binding) {
            pizzaRecyclerView.isVisible = true
            includeNoInternet.noInternetIncludeLayout.isVisible = false
        }
    }

    override fun sendData(cartMenu: Menu) { listener.sendData(cartMenu) }

    override fun onItemClick(item: Menu) {
        when {
            item.metaData.cat.equals("salad") -> showDialog(item)
            item.metaData.cat.equals("pizza") -> parentFragmentManager
                    .beginTransaction()
                    .replace(R.id.pager_container, PizzaDetailsFragmentBuilder(item).build())
                    .addToBackStack(null)
                    .commit()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) listener = context
    }

    override fun onChangeItemClick(item: Menu, isAdd: Boolean) {
        presenter.onChangeItemClick(item, isAdd)
    }

    fun sendDataToPizzaFragment(menuList: List<Menu>) = presenter.onActivityResult(menuList)

    private fun showDialog(item: Menu) {
        val dialog = AlertDialog.Builder(context)
        dialog.setView(binding.root)
        with(AlertDialogBinding.inflate(layoutInflater)){
            alertDialogImageView.load(item.metaData.gallery[0])
            alertDialogTitleTextView.text = item.title.rendered.fromHtml()
            alertDialogContentTextView.text = item.excerpt.rendered.fromHtml()
        }
        dialog.create().show()
    }

}