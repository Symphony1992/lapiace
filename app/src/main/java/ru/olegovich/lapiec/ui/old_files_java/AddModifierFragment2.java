/**package ru.olegovich.lapiec.ui;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.hannesdorfmann.fragmentargs.annotation.Arg;

import java.util.ArrayList;
import java.util.List;

import ru.olegovich.domain.data.modifiers.Modifier;
import ru.olegovich.domain.mvp.add_modifier.*;
import ru.olegovich.lapiec.AddModifierAdapter;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

import static ru.olegovich.domain.data.Constants.ADD_MODIFIER_TO_PIZZA_BUNDLE_KEY;
import static ru.olegovich.domain.data.Constants.MODIFIER_ITEM_TRANSFER;

public class AddModifierFragment100
        extends BaseFragmentMvp<AddModifierMvp.Presenter, AddModifierMvp.View>
        implements AddModifierMvp.View, AddModifierAdapter.OnItemClickListener  {

    @Arg ArrayList<Modifier> modifiers;
    private RecyclerView recyclerView;

    @Override
    public AddModifierMvp.Presenter getPresenter() {
        return new AddModifierPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.add_modifier_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.add_modifier_recyclerview);
        presenter.onCreated(modifiers);
    }

    @Override
    public void showModifier(List<Modifier> modifierList) {
        AddModifierAdapter adapter = new AddModifierAdapter(modifierList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(Modifier item) {
        presenter.onItemClick(item);
    }

    @Override
    public void onStop() {
        presenter.onStop();
        super.onStop();
    }

    @Override
    public void setFragmentResult(ArrayList<Modifier> newItem) {
        Bundle result = new Bundle();
        result.putSerializable(MODIFIER_ITEM_TRANSFER, newItem);
        getParentFragmentManager().setFragmentResult(ADD_MODIFIER_TO_PIZZA_BUNDLE_KEY, result);
    }

}*/
