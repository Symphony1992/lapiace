package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.info_fragment.*
import ru.olegovich.domain.mvp.info_screen.InfoMvp
import ru.olegovich.domain.mvp.info_screen.InfoPresenter
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.InfoFragmentBinding

class InfoFragment : BaseViewBindFragmentMvp<
            InfoMvp.Presenter,
            InfoMvp.View,
            InfoFragmentBinding>(),
        InfoMvp.View {

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> InfoFragmentBinding = InfoFragmentBinding::inflate

    override fun presenter() = InfoPresenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreated()
        with(binding){
            infoAboutCardView.setOnClickListener{ presenter.onAboutUsCardViewClick() }
            infoContactCardView.setOnClickListener { presenter.onContactsCardViewClick() }
            infoVacancyCardView.setOnClickListener { presenter.onVacancyCardViewClick() }
            infoPartnerCardView.setOnClickListener { presenter.onPartnersCardViewClick() }
            infoDeliveryCardView.setOnClickListener { presenter.onDeliveryCardViewClick() }
        }
    }

    override fun startAboutUsFragment() { start(AboutFragment()) }

    override fun startContactsFragment() { start(ContactsFragment()) }

    override fun startVacancyFragment() { start(VacancyFragment()) }

    override fun startPartnersFragment() { start(PartnersFragment()) }

    override fun startDeliveryFragment() { start(DeliveryFragment()) }

    fun start(fragment: Fragment){
        parentFragmentManager.beginTransaction()
                .replace(R.id.pager_container, fragment).addToBackStack(null).commit()
    }

}