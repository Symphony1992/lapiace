/**package ru.olegovich.lapiec.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ru.olegovich.domain.data.coordinates.Coordinate;
import ru.olegovich.domain.data.coordinates.ZonePath;
import ru.olegovich.domain.data.enums.DeliveryCounter;
import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.domain.data.streets.Street;
import ru.olegovich.domain.mvp.final_screen.FinalMvp;
import ru.olegovich.domain.mvp.final_screen.FinalPresenter;
import ru.olegovich.lapiec.adapters.FinalAdapter;
import ru.olegovich.lapiec.adapters.FinalStreetsAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static ru.olegovich.domain.data.enums.DeliveryCounter.ADVANCE;
import static ru.olegovich.domain.data.enums.DeliveryCounter.DELIVERY;
import static ru.olegovich.domain.data.enums.DeliveryCounter.PICKUP;
import static ru.olegovich.domain.data.enums.PaymentCounter.CARD;
import static ru.olegovich.domain.data.enums.PaymentCounter.CASH;

public class FinalActivity extends BaseActivityMvp2<FinalMvp.Presenter, FinalMvp.View>
        implements FinalMvp.View, OnMapReadyCallback,
        AdapterView.OnItemSelectedListener,
        TimePickerDialog.OnTimeSetListener,
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {

    private EditText nameEditText, phoneEditText, numberEditText;
    private TextView dateTextView, timeTextView;
    private TextView attentionTextView, cafeAddressTextView, sumTextView, errorTextView;
    private AutoCompleteTextView streetView;
    private Spinner cityAutoView;
    private CheckBox isCallMeBox;
    private View includeFinal;
    private Button findButton;
    private RecyclerView recyclerView;
    private MapView mapView;
    private GoogleMap googleMap;
    private ImageView dateImageView, timeImageView;
    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    List<LatLng> polygonGreen = new ArrayList<>();
    List<LatLng> polygonYellow = new ArrayList<>();

    @Override
    public FinalMvp.Presenter getPresenter() {
        return new FinalPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.activity_final;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RadioGroup paymentRadioGroup = findViewById(R.id.final_payment_radio_group);
        RadioGroup deliveryRadioGroup = findViewById(R.id.final_delivery_radio_group);
        nameEditText = findViewById(R.id.final_name_edit_text);
        dateTextView = findViewById(R.id.final_date_text_view);
        timeTextView = findViewById(R.id.final_time_text_view);
        phoneEditText = findViewById(R.id.final_phone_edit_text);
        cityAutoView = findViewById(R.id.final_city_spinner);
        streetView = findViewById(R.id.final_street_view);
        numberEditText = findViewById(R.id.final_number_edit_text);
        cafeAddressTextView = findViewById(R.id.final_cafe_address_edit_text);
        sumTextView = findViewById(R.id.final_sum_text_view);
        isCallMeBox = findViewById(R.id.is_call_me_check_box);
        includeFinal = findViewById(R.id.final_include);
        attentionTextView = findViewById(R.id.attention_text_view);
        findButton = findViewById(R.id.final_find_button);
        Button applyButton = findViewById(R.id.final_apply_button);
        mapView = findViewById(R.id.final_map_view);
        recyclerView = findViewById(R.id.final_recycler_view);
        ImageButton backButton = findViewById(R.id.final_back_image_button);
        errorTextView = findViewById(R.id.final_error_text_view);
        dateImageView = findViewById(R.id.final_date_image_view);
        timeImageView = findViewById(R.id.final_time_image_view);

        initGoogleMap(savedInstanceState);

        applyButton.setOnClickListener(v -> {
            presenter.applyButtonClick();
            hideKeyboardFrom(applyButton.getContext(), applyButton);
        });
        backButton.setOnClickListener(v -> {
            presenter.backButtonClick();
            hideKeyboardFrom(backButton.getContext(), backButton);
        });

        isCallMeBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            presenter.isCallMeBoxClick();
            isChecked = !isChecked;
            hideKeyboardFrom(isCallMeBox.getContext(), isCallMeBox);
        });

        findButton.setOnClickListener(v -> {
            presenter.findAddress(
                    streetView.getText().toString(), numberEditText.getText().toString());
            hideKeyboardFrom(findButton.getContext(), findButton);
        });

        timeTextView.setOnClickListener(v -> presenter.onTimeTextClick());
        dateTextView.setOnClickListener(v -> presenter.onDateTextClick());

        cityAutoView.setOnItemSelectedListener(this);

        paymentRadioGroup.setOnCheckedChangeListener((radioGroup, idView) -> {
            switch (idView){
                case R.id.final_cash_radio_button:
                    presenter.paymentChoose(CASH);
                    break;
                case R.id.final_card_radio_button:
                    presenter.paymentChoose(CARD);
                    break;
            }
        });

        deliveryRadioGroup.setOnCheckedChangeListener((radioGroup, idView) -> {
            switch (idView){
                case R.id.final_delivery_radio_button:
                    presenter.deliveryChoose(DELIVERY);
                    break;
                case R.id.final_pickup_radio_button:
                    presenter.deliveryChoose(PICKUP);
                    break;
                case R.id.final_advance_radio_button:
                    presenter.deliveryChoose(ADVANCE);
                    break;
            }
        });
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void initGoogleMap(Bundle savedInstanceState) {
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(FINAL_MAP_VIEW_BUNDLE_KEY);
        }
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
    }

    @Override
    public void setVisibility(DeliveryCounter delivery) {
        switch (delivery){
            case DELIVERY:
                setVisibilityOfView(VISIBLE,GONE,GONE,GONE,VISIBLE);
                break;
            case PICKUP:
                setVisibilityOfView(GONE,GONE,VISIBLE,GONE,GONE);
                break;
            case ADVANCE:
                setVisibilityOfView(VISIBLE,VISIBLE,GONE,VISIBLE,GONE);
                break;
        }
    }

    private void setVisibilityOfView(int group, int timeDate, int cafeAddress,
                                     int attention, int callMe) {
        setVisibilityOfGroup(group);
        setVisibilityOfTimeDate(timeDate);
        cafeAddressTextView.setVisibility(cafeAddress);
        attentionTextView.setVisibility(attention);
        isCallMeBox.setVisibility(callMe);
        setVisibilityInclude();
    }

    private void setVisibilityOfGroup(int visibility) {
        cityAutoView.setVisibility(visibility);
        streetView.setVisibility(visibility);
        numberEditText.setVisibility(visibility);
        mapView.setVisibility(visibility);
        findButton.setVisibility(visibility);
    }

    private void setVisibilityOfTimeDate(int visibility) {
        dateTextView.setVisibility(visibility);
        timeTextView.setVisibility(visibility);
        dateImageView.setVisibility(visibility);
        timeImageView.setVisibility(visibility);
    }

    @Override
    public void setVisibilityInclude() {
        if (isCallMeBox.isChecked()) includeFinal.setVisibility(VISIBLE);
        else includeFinal.setVisibility(GONE);
    }

    @Override
    public void showFinalToast() {
        Toast.makeText(this, R.string.final_toast, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setLatLng(double lat, double lng) {
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(lat, lng), 10));
    }

    @Override
    public void showCoordinate(ZonePath zonePath, boolean zone_color) {
        LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();
        int color = Color.parseColor(zonePath
                .getColor()
                .replace("#", "#6E"));
        PolygonOptions polygonOptions = new PolygonOptions()
                .fillColor(color)
                .strokeColor(setColor(zonePath.getColor()))
                .strokeWidth(2);
        for (Coordinate it : zonePath.getCoordinates()) {
            polygonOptions.add(new LatLng(it.getLat(), it.getLng()));
            latLngBounds.include(new LatLng(it.getLat(), it.getLng()));
            if (zone_color) polygonGreen.add(new LatLng(it.getLat(), it.getLng()));
            else polygonYellow.add(new LatLng(it.getLat(), it.getLng()));
        }
        googleMap.addPolygon(polygonOptions);
    }

    private int setColor(String color) {
        return Color.parseColor(color);
    }

    @Override
    public void showMenuList(List<Menu> menu) {
        FinalAdapter adapter = new FinalAdapter(menu);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showTotalSum(int sum) {
        sumTextView
                .setText(sumTextView.getContext().getString(R.string.final_sum, sum));
    }

    @Override
    public void showCity(List<String> cities, int pos) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.final_city_row, cities);
//        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.final_city_row, cities);
        cityAutoView.setAdapter(adapter);
        cityAutoView.setSelection(pos);
    }

    @Override
    public void setStreets(List<Street> streets) {
        List<String> street = new ArrayList<>();

        for(Street it : streets){
            street.add(it.getName());
        }

        ArrayAdapter adapter = new ArrayAdapter(
                this, R.layout.final_streets_row, street);
        streetView.setAdapter(adapter);
    }

    @Override
    public void showLatLngResult(double lat, double lng) {
        LatLng point = new LatLng(lat,lng);
        boolean in_green_zone = PolyUtil.containsLocation(point, polygonGreen, true);
        boolean in_yellow_zone = PolyUtil.containsLocation(point, polygonYellow, true);
        System.out.println("!!!!!!!" + in_green_zone + "!!!!!!!!" + in_yellow_zone);
        if (in_yellow_zone && !in_green_zone) {
            zoneChosen(false);
        } else if (in_green_zone) {
            zoneChosen(true);
        } else {
            presenter.noZoneChosen();
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 17));
    }

    private void zoneChosen(boolean zone_color) {
        if (!zone_color) presenter.yellowZoneSumCheck();
        else presenter.greenZoneSumCheck();
    }

    @Override
    public void backToCart() {
        onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        presenter.onCreated((List<Menu>) getIntent().getSerializableExtra(FINAL_ACTIVITY));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(FINAL_MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(FINAL_MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }
        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }
    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        presenter.onCityChosen(parent.getItemAtPosition(position).toString());
        streetView.setText("");
        numberEditText.setText("");
        errorTextView.setVisibility(GONE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, R.string.city_not_chosen, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorFindToast() {
        Toast.makeText(this, R.string.final_address_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorZoneText() {
        errorTextView.setVisibility(VISIBLE);
        errorTextView.setText(R.string.final_error_zone);
    }

    @Override
    public void setVisibilityError() {
        errorTextView.setVisibility(GONE);
    }

    @Override
    public void showErrorNoZoneText() {
        errorTextView.setVisibility(VISIBLE);
        errorTextView.setText(R.string.final_error_no_zone);
    }

    @Override
    public void onChooseTime() {
        Calendar now = Calendar.getInstance();
        Timepoint[] disabledTime = {
                new Timepoint(0), new Timepoint(0,15),
                new Timepoint(0, 30), new Timepoint(0, 45),
                new Timepoint(1), new Timepoint(1,15),
                new Timepoint(1,30), new Timepoint(1,45),
                new Timepoint(2), new Timepoint(2,15),
                new Timepoint(2,30), new Timepoint(2,45),
                new Timepoint(3), new Timepoint(3,15),
                new Timepoint(3,30), new Timepoint(3,45),
                new Timepoint(4), new Timepoint(4,15),
                new Timepoint(4,30), new Timepoint(4,45),
                new Timepoint(5), new Timepoint(5,15),
                new Timepoint(5,30), new Timepoint(5,45),
                new Timepoint(6), new Timepoint(6,15),
                new Timepoint(6,30), new Timepoint(6,45),
                new Timepoint(7), new Timepoint(7,15),
                new Timepoint(7,30), new Timepoint(7,45),
                new Timepoint(8), new Timepoint(8,15),
                new Timepoint(8,30), new Timepoint(8,45),
                new Timepoint(9), new Timepoint(9,15),
                new Timepoint(9,30), new Timepoint(9,45),
        };
        if (timePickerDialog == null){
            timePickerDialog = TimePickerDialog.newInstance(
                    FinalActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    true);
        } else {
            timePickerDialog.initialize(
                    FinalActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    true);
        }
        timePickerDialog.enableSeconds(false);
        timePickerDialog.setVersion(TimePickerDialog.Version.VERSION_2);
        timePickerDialog.setAccentColor(Color.parseColor("#FFBB33"));
        timePickerDialog.setTimeInterval(1, 15);
        timePickerDialog.setDisabledTimes(disabledTime);
        timePickerDialog.setLocale(new Locale("uk","UA"));
        timePickerDialog.setCancelText(R.string.date_time_cancel);
        timePickerDialog.setOkText(R.string.date_time_ok);
        timePickerDialog.setOnCancelListener(dialog -> timePickerDialog = null);
        timePickerDialog.show(getSupportFragmentManager(), TIME_PICKER_DIALOG);
    }

    @Override
    public void onChooseDate() {
        Calendar now = Calendar.getInstance();
        if (datePickerDialog == null){
            datePickerDialog = DatePickerDialog.newInstance(
                    FinalActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.DAY_OF_MONTH));
        } else {
            datePickerDialog.initialize(
                    FinalActivity.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
        }
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor("#FFBB33");
        Calendar[] days = new Calendar[29];
        for (int i = 0; i < 29; i++) {
            Calendar day = Calendar.getInstance();
            day.add(Calendar.DAY_OF_MONTH, i);
            days[i] = day;
        }
        datePickerDialog.setSelectableDays(days);
        datePickerDialog.setLocale(new Locale("uk","UA"));
        datePickerDialog.setCancelText(R.string.date_time_cancel);
        datePickerDialog.setOkText(R.string.date_time_ok);
        datePickerDialog.setOnCancelListener(dialog -> datePickerDialog = null);
        datePickerDialog.show(getSupportFragmentManager(), DATE_PICKER_DIALOG);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String startMinute = String.valueOf(minute);
        if (minute == 0) startMinute = "00";
        String startTime = hourOfDay + ":" + startMinute;
        String endMinute;
        int endHour = hourOfDay;
        if (minute == 45) {
            endMinute = "00";
            endHour++;
        } else endMinute = String.valueOf(minute + 15);
        String finalTime = endHour + ":" + endMinute;
        timeTextView.setText(timeTextView.getResources()
                .getString(R.string.final_time_set, startTime, finalTime));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        SimpleDateFormat formatDay = new SimpleDateFormat(
                "dd MMMM yyyy", new Locale("uk","UA"));
        String date = formatDay.format(calendar.getTime());
        dateTextView.setText(date);
    }
}*/
