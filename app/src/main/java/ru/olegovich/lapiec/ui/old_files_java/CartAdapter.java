/**package ru.olegovich.lapiec;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.lapiec.adapters.CartAdapter;
import ru.olegovich.lapiec.ui.R;

public class CartAdapter1 extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    List<Menu> items;
    OnItemClickListener listener;
    boolean isStart = false;

    public CartAdapter(List<Menu> menu, OnItemClickListener listener) {
        this.items = menu;
        this.listener = listener;
    }

    public void removeItem(Menu item, int position) {
        items.remove(item);
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_cart, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Menu item = items.get(position);
        holder.cartNameTextView.setText(item.getTitle().getRendered());
        Glide
                .with(holder.cartImageView)
                .load(item.getMetaData().getGallery().get(0))
                .into(holder.cartImageView);
        holder.deleteImageButton.setOnClickListener(v -> {
            listener.onDeleteClick(item, position);
        });

        String s = "";
        int sum = 0;
        for (int i = 0; i < item.getModifiers().size(); i++){
            String title = item.getModifiers().get(i).getTitle().getRendered().toLowerCase();
            s = i == 0 ? title : s + ", " + title;
            sum = sum + item.getModifiers().get(i).getMetaData().getPrice();
        }

        holder.cartPriceTextView
                .setText(String.valueOf(item.getCounter() * (item.getMetaData().getPrice() + sum)));
        holder.modifiersToPizzaTextView.setText(s);
        holder.addToCartImageButton.setOnClickListener(v -> {
            listener.onChangeItemClick(item, true, position);
        });
        holder.removeFromCartImageButton.setOnClickListener(v -> {
            listener.onChangeItemClick(item, false, position);
        });
        holder.cartQuantity.setText(String.valueOf(item.getCounter()));

        if (item.getModifiers().isEmpty()){
            holder.additionsTextView.setVisibility(View.GONE);
            holder.rotateImage.setVisibility(View.GONE);
        }

        holder.rotateImage.setOnClickListener(v -> {
            if (!isStart) {
                rotateArrow(0,90,holder);
            }
            else rotateArrow(90,0,holder);
        });

    }

    private void rotateArrow(int start, int end, ViewHolder holder) {
        int pivotX = holder.rotateImage.getWidth() / 2;
        int pivotY = holder.rotateImage.getHeight() / 2;
        Animation animation = new RotateAnimation(start, end, pivotX, pivotY);
        animation.setDuration(250);
        animation.setFillAfter(true);
        holder.rotateImage.startAnimation(animation);
        if (!isStart) {
            holder.modifiersToPizzaTextView.setVisibility(View.VISIBLE);
        } else {
            holder.modifiersToPizzaTextView.setVisibility(View.GONE);
        }
        isStart = !isStart;
    }

    @Override
    public int getItemCount() { return items.size(); }

    public boolean isEmpty() { return items.isEmpty(); }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView cartImageView;
        TextView cartNameTextView;
        TextView cartPriceTextView;
        TextView cartQuantity;
        ImageButton addToCartImageButton;
        ImageButton removeFromCartImageButton;
        ImageButton deleteImageButton;
        TextView modifiersToPizzaTextView;
        TextView additionsTextView;
        ImageView rotateImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cartImageView = itemView.findViewById(R.id.cart_image_view);
            cartNameTextView = itemView.findViewById(R.id.cart_name_text_view);
            cartPriceTextView = itemView.findViewById(R.id.cart_price_text_view);
            cartQuantity = itemView.findViewById(R.id.cart_quantity_text_view);
            addToCartImageButton = itemView.findViewById(R.id.add_in_cart_image_button);
            removeFromCartImageButton = itemView.findViewById(R.id.remove_in_cart_image_button);
            deleteImageButton = itemView.findViewById(R.id.delete_image_button);
            modifiersToPizzaTextView = itemView.findViewById(R.id.modifiers_to_pizza_text_view);
            rotateImage = itemView.findViewById(R.id.cart_rotate_arrow);
            additionsTextView = itemView.findViewById(R.id.additions_text_view);
        }
    }

    public interface OnItemClickListener {
        void onDeleteClick(Menu item, int position);
        void onChangeItemClick(Menu item, boolean isAdd, int position);
    }

}*/
