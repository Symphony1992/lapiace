/**package ru.olegovich.lapiec;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.lapiec.ui.R;

public class FinalAdapter extends RecyclerView.Adapter<FinalAdapter.ViewHolder> {

    List<Menu> items;

    public FinalAdapter(List<Menu> menu) {
        this.items = menu;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_final, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Menu item = items.get(position);
        Glide.with(holder.pizzaImageView)
                .load(item.getMetaData().getGallery().get(0))
                .into(holder.pizzaImageView);
        holder.titleTextView.setText(item.getTitle().getRendered());
        holder.quantityTextView.setText(String.valueOf(item.getCounter()));
        int sum = 0;
        for (int i = 0; i < item.getModifiers().size(); i++){
            sum = sum + item.getModifiers().get(i).getMetaData().getPrice();
        }
        holder.sumTextView
                .setText(String.valueOf(item.getCounter() * (item.getMetaData().getPrice() + sum)));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView pizzaImageView;
        TextView titleTextView, quantityTextView, sumTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pizzaImageView = itemView.findViewById(R.id.item_final_pizza_image_view);
            titleTextView = itemView.findViewById(R.id.item_final_title_text_view);
            quantityTextView = itemView.findViewById(R.id.item_final_quantity_text_view);
            sumTextView = itemView.findViewById(R.id.item_final_sum_text_view);
        }
    }

}*/
