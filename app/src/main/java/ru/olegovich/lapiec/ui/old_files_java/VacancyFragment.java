/**package ru.olegovich.lapiec.ui.old_files_java;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.olegovich.domain.data.vacancies.Vacancy;
import ru.olegovich.domain.mvp.vacancy.VacancyMvp;
import ru.olegovich.domain.mvp.vacancy.VacancyPresenter;
import ru.olegovich.lapiec.VacancyAdapter;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

import static ru.olegovich.domain.data.enums.Status.VACANCY;
import static ru.olegovich.domain.data.Constants.FRAGMENT_TO_PAGER;
import static ru.olegovich.domain.data.Constants.STATUS_OF_VIEW;

public class VacancyFragment11 extends BaseFragmentMvp<VacancyMvp.Presenter, VacancyMvp.View> implements VacancyMvp.View {

    private RecyclerView recyclerView;
    private NestedScrollView scrollView;
    private TextView toScroll;
    private View include;

    @Override
    public void showVacancy(List<Vacancy> vacancyList) {
        VacancyAdapter adapter = new VacancyAdapter(vacancyList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public VacancyMvp.Presenter getPresenter() {
        return new VacancyPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.vacancies_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.vacancy_recycleview);
        Button moveToVacancyButton = view.findViewById(R.id.move_vacancy_button);
        scrollView = view.findViewById(R.id.vacancy_scroll_view);
        toScroll = view.findViewById(R.id.to_scroll_text_view);
        include = view.findViewById(R.id.no_internet_vacancy_include);
        Button tryAgainButton = view.findViewById(R.id.no_internet_try_again_button);

//        UtilKt.text(toScroll, R.string.final_sum);

        presenter.onCreated();
        moveToVacancyButton.setOnClickListener(v -> presenter.onMoveToVacancyClick());
        tryAgainButton.setOnClickListener(v -> presenter.onTryAgainClick());

    }

    @Override
    public void scrollToVacancy() {
        scrollView.smoothScrollTo(0, toScroll.getTop());
    }

    @Override
    public void showError() {
        scrollView.setVisibility(View.GONE);
        include.setVisibility(View.VISIBLE);
    }

    @Override
    public void reOpen() {
        presenter.onCreated();
        scrollView.setVisibility(View.VISIBLE);
        include.setVisibility(View.GONE);
    }

    @Override
    public void sendingStatusOfView() {
        setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, VACANCY);
    }

}*/
