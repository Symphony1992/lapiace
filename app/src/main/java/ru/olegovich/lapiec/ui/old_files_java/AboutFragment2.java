/**package ru.olegovich.lapiec.ui;


import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.olegovich.lapiec.ui.base.BaseFragment;

import static ru.olegovich.domain.data.enums.Status.*;
import static ru.olegovich.domain.data.ConstantsKt.*;

public class AboutFragment2 extends BaseFragment {

    @Override
    public int layoutRes() {
        return R.layout.about_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFragmentResultToActivity(STATUS_OF_VIEW,FRAGMENT_TO_PAGER,ABOUT);
    }

}
*/