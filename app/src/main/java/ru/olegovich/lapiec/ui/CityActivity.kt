package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.data.entity.enums.City.*
import ru.olegovich.domain.mvp.city.CityMvp
import ru.olegovich.domain.mvp.city.CityPresenter
import ru.olegovich.lapiec.ui.base.BaseViewBindActivityMvp
import ru.olegovich.lapiec.ui.databinding.ActivityCityBinding
import ru.olegovich.lapiec.util.*

class CityActivity : BaseViewBindActivityMvp<
            CityMvp.Presenter,
            CityMvp.View,
            ActivityCityBinding>(),
        CityMvp.View {

    override fun presenter(): CityMvp.Presenter = CityPresenter()

    override val bindingInflater: (LayoutInflater) -> ActivityCityBinding =
            ActivityCityBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter.onCreated()

        with(binding){
            applyButton.setOnClickListener { presenter.onCityApplyClick() }
            cityChooseRadioGroup.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    R.id.city_lviv_radio_button -> presenter.onCitySelect(LVIV)
                    R.id.city_vinnitsa_radio_button -> presenter.onCitySelect(VINNYTSIA)
                    R.id.city_ivanof_radio_button -> presenter.onCitySelect(IVANO_FRANKIVSK)
                    R.id.city_lutsk_radio_button -> presenter.onCitySelect(LUTSK)
                }
            }
        }
    }

    override fun showSelectedToast() = toast(R.string.error_city)

    override fun goToMainActivity() {
        startActivity(intent(MainActivity::class.java))
        finish()
        overridePendingTransition(R.anim.top_in, R.anim.bottom_out)
    }

    override fun initRadioGroup(city: City) {
        with(binding){
            when (city) {
                LVIV -> cityChooseRadioGroup.check(R.id.city_lviv_radio_button)
                VINNYTSIA -> cityChooseRadioGroup.check(R.id.city_vinnitsa_radio_button)
                IVANO_FRANKIVSK -> cityChooseRadioGroup.check(R.id.city_ivanof_radio_button)
                LUTSK -> cityChooseRadioGroup.check(R.id.city_lutsk_radio_button)
                else -> Unit
            }
        }
    }
}