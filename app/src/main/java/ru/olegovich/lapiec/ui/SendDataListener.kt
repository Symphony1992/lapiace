package ru.olegovich.lapiec.ui

import ru.olegovich.domain.data.entity.pizza.Menu

interface SendDataListener {
    fun sendData(menuItem: Menu)
}