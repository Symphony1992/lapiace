/**package ru.olegovich.lapiec.ui.old_files_java;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.base.BaseFragment;
import ru.olegovich.lapiec.ui.maskedEditText.MaskedEditText;

import static ru.olegovich.domain.data.enums.Status.PARTNERS;
import static ru.olegovich.domain.data.Constants.FRAGMENT_TO_PAGER;
import static ru.olegovich.domain.data.Constants.STATUS_OF_VIEW;

public class PartnersFragment2 extends BaseFragment {

    @Override
    public int layoutRes() {
        return R.layout.partners_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //    private String g;
        MaskedEditText phoneEditText = view.findViewById(R.id.phone_edit_text);
        setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, PARTNERS);

        phoneEditText.setMask("+38(0##)###-##-##");

//        phoneEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean hasFocus) {
//
//                if (hasFocus) {
//                    phoneEditText.setMask("+38(0##)###-##-##");
//                    if (g != null)   phoneEditText.setText(g);
//                }
//                if (!hasFocus && phoneEditText.getRawText() != null) {
//                    g = phoneEditText.getRawText();
//                    phoneEditText.setMask("+38(0##)###-##-##");
//                    phoneEditText.setText(g);
//                }
//
//                if (!hasFocus && g == null) {
//                    phoneEditText.setMask("#######");
//                }
//            }
//        });
    }

}*/
