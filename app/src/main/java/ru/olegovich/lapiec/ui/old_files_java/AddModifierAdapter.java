/**package ru.olegovich.lapiec.ui.old_files_java;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import ru.olegovich.domain.data.modifiers.Modifier;
import ru.olegovich.lapiec.adapters.AddModifierAdapter;
import ru.olegovich.lapiec.ui.R;

public class AddModifierAdapter1 extends RecyclerView.Adapter<AddModifierAdapter.ViewHolder> {

    private List<Modifier> items;
    private OnItemClickListener listener;

    public AddModifierAdapter(List<Modifier> items,  OnItemClickListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AddModifierAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_add_modifier, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AddModifierAdapter.ViewHolder holder, int position) {
        Modifier item = items.get(position);
        AtomicBoolean isChecked = new AtomicBoolean(item.isSelected());
        holder.modifierTitleTextView.setText(item.getTitle().getRendered());
        holder.modifierWeightTextView
                .setText(holder.modifierWeightTextView
                        .getContext().getString(R.string.gram, item.getMetaData().getWeight()));
        holder.modifierPriceTextView.setText(String.valueOf(item.getMetaData().getPrice()));
        initCard(holder, isChecked.get());
        System.out.println("!!!!!" + isChecked.get());
        Glide
                .with(holder.modifierImgView)
                .load(item.getMetaData().getMobImg())
                .into(holder.modifierImgView);
        holder.modifierCardView.setOnClickListener(v -> {
            listener.onItemClick(item);
            isChecked.set(!isChecked.get());
            initCard(holder, isChecked.get());
        });

    }

    private void initCard(AddModifierAdapter.ViewHolder holder, boolean isChecked){
        holder.modifierCardView.setCardElevation(isChecked ? 10 : 2);
        holder.modifierButton.setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {        return items.size();    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView modifierImageView;
        TextView modifierTitleTextView;
        TextView modifierWeightTextView;
        TextView modifierPriceTextView;
        CardView modifierCardView;
        Button modifierButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            modifierImageView = itemView.findViewById(R.id.modifier_image_view);
            modifierTitleTextView = itemView.findViewById(R.id.modifier_title_text_view);
            modifierWeightTextView = itemView.findViewById(R.id.modifier_weight_text_view);
            modifierPriceTextView = itemView.findViewById(R.id.modifier_price_text_view);
            modifierCardView = itemView.findViewById(R.id.modifier_cardview);
            modifierButton = itemView.findViewById(R.id.modifier_cancel_button);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Modifier item);
    }
}*/
