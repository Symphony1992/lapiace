package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.partners_fragment.*
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.Status.*
import ru.olegovich.lapiec.ui.base.BaseViewBindFragment
import ru.olegovich.lapiec.ui.databinding.PartnersFragmentBinding

class PartnersFragment : BaseViewBindFragment<PartnersFragmentBinding>() {

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> PartnersFragmentBinding = PartnersFragmentBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFragmentResultToActivity(STATUS_OF_VIEW, FRAGMENT_TO_PAGER, PARTNERS)
        phone_edit_text.mask = "+38(0##)###-##-##"
    }

}