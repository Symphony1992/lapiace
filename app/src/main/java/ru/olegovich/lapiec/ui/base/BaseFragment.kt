package ru.olegovich.lapiec.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hannesdorfmann.fragmentargs.FragmentArgs
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import java.io.Serializable


@FragmentWithArgs
abstract class BaseFragment : Fragment() {

    abstract val layoutRes: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FragmentArgs.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    open fun setFragmentResultToActivity(key: String, requestKey: String, value: Serializable) {
        val result = Bundle()
        result.putSerializable(key, value)
        parentFragmentManager.setFragmentResult(requestKey, result)
    }

    open fun setFragmentResultToActivity(key: String,
                                         key2: String,
                                         requestKey: String,
                                         value: Serializable,
                                         value2: Serializable) {
        val result = Bundle()
        result.putSerializable(key, value)
        result.putSerializable(key2, value2)
        parentFragmentManager.setFragmentResult(requestKey, result)
    }

}