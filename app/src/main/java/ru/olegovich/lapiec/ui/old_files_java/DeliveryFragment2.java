/**package ru.olegovich.lapiec.ui.old_files_java;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.jetbrains.annotations.NotNull;

import ru.olegovich.domain.data.coordinates.Coordinate;
import ru.olegovich.domain.data.coordinates.ZonePath;
import ru.olegovich.domain.mvp.delivery_screen.DeliveryMvp;
import ru.olegovich.domain.mvp.delivery_screen.DeliveryPresenter;
import ru.olegovich.lapiec.adapters.AutoCompleteAdapter;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

import static ru.olegovich.domain.data.Constants.MAP_VIEW_BUNDLE_KEY;

public class DeliveryFragment2 extends BaseFragmentMvp<DeliveryMvp.Presenter, DeliveryMvp.View>
        implements DeliveryMvp.View, OnMapReadyCallback {

    private MapView mapView;
    private AutoCompleteTextView addressEditText;
    private GoogleMap googleMap;

    @Override
    public int layoutRes() {
        return R.layout.delivery_fragment;
    }

    @Override
    public DeliveryMvp.Presenter getPresenter() {
        return new DeliveryPresenter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = view.findViewById(R.id.mapView);
        addressEditText = view.findViewById(R.id.address_edit_text);

        initGoogleMap(savedInstanceState);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        presenter.onCreated();
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void showCoordinate(ZonePath zonePath) {
        LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();
        int color = Color.parseColor(zonePath
                .getColor()
                .replace("#", "#6E"));
        PolygonOptions polygonOptions = new PolygonOptions()
                .fillColor(color)
                .strokeColor(setColor(zonePath.getColor()))
                .strokeWidth(2);
        for (Coordinate it : zonePath.getCoordinates()) {
            polygonOptions.add(new LatLng(it.getLat(), it.getLng()));
            latLngBounds.include(new LatLng(it.getLat(), it.getLng()));
        }
        googleMap.addPolygon(polygonOptions);


        if (getContext() != null)
        {
            Places.initialize(getContext(), getResources().getString(R.string.google_maps_api_key));
            PlacesClient placesClient = Places.createClient(getContext());
            AutoCompleteAdapter adapter = new AutoCompleteAdapter(
                    getContext(), placesClient, latLngBounds.build()
            );
            addressEditText.setAdapter(adapter);
            addressEditText.setOnItemClickListener((parent, view1, position, id) -> {
                presenter.sendLatLngRequest(adapter.resultList.get(position).getPlaceId());
            });
        }

    }

    @Override
    public void setLatLng(double lat, double lng) {
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(lat, lng), 10));
    }

    @Override
    public void showLatLngResult(double lat, double lng) {
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(lat, lng), 17));
    }

    public int setColor (String color){
        return Color.parseColor(color);
    }

    private void initGoogleMap(Bundle savedInstanceState) {
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }
    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}*/
