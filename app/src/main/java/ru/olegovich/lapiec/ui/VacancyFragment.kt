package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.include_no_internet.*
import kotlinx.android.synthetic.main.vacancies_fragment.*
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.Status
import ru.olegovich.domain.data.entity.vacancies.Vacancy
import ru.olegovich.domain.mvp.vacancy.VacancyMvp
import ru.olegovich.domain.mvp.vacancy.VacancyPresenter
import ru.olegovich.lapiec.adapters.VacancyAdapter
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.VacanciesFragmentBinding

class VacancyFragment : BaseViewBindFragmentMvp<
            VacancyMvp.Presenter,
            VacancyMvp.View,
            VacanciesFragmentBinding>(),
        VacancyMvp.View {

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> VacanciesFragmentBinding = VacanciesFragmentBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreated()
        with(binding){
            moveVacancyButton.setOnClickListener { presenter.onMoveToVacancyClick() }
            includeNoInternet.noInternetIncludeLayout
                    .setOnClickListener { presenter.onTryAgainClick() }
        }
    }

    override fun presenter(): VacancyMvp.Presenter = VacancyPresenter()

    override fun showVacancy(vacancyList: List<Vacancy>) {
        binding.vacancyRecycleview.adapter = VacancyAdapter(vacancyList)
    }

    override fun scrollToVacancy() = binding.vacancyScrollView
            .smoothScrollTo(0, to_scroll_text_view.top)

    override fun showError() = setVisibilityOfViews(vacancyScrollView = false, include = true)

    override fun reOpen() = setVisibilityOfViews(vacancyScrollView = true, include = false)

    private fun setVisibilityOfViews(vacancyScrollView: Boolean, include: Boolean) {
        with(binding){
            this.vacancyScrollView.isVisible = vacancyScrollView
            includeNoInternet.noInternetIncludeLayout.isVisible = include
        }
    }

    override fun sendingStatusOfView() = setFragmentResultToActivity(
            STATUS_OF_VIEW, FRAGMENT_TO_PAGER, Status.VACANCY)

}