package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import ru.olegovich.domain.mvp.splash_screen.SplashMvp
import ru.olegovich.domain.mvp.splash_screen.SplashPresenter
import ru.olegovich.lapiec.ui.base.BaseViewBindActivityMvp
import ru.olegovich.lapiec.ui.databinding.ActivitySplashBinding
import ru.olegovich.lapiec.util.*

class SplashActivity : BaseViewBindActivityMvp<
            SplashMvp.Presenter,
            SplashMvp.View,
            ActivitySplashBinding>(),
        SplashMvp.View {

    override val bindingInflater: (LayoutInflater) -> ActivitySplashBinding =
            ActivitySplashBinding::inflate

    override fun presenter(): SplashMvp.Presenter = SplashPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCityChecked()
        finish()
    }

    override fun goToCityActivity() = startActivity(intent(CityActivity::class.java))

    override fun goToMainActivity() = startActivity(intent(MainActivity::class.java))

}