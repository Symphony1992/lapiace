/**package ru.olegovich.lapiec.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import ru.olegovich.domain.data.enums.City;
import ru.olegovich.domain.data.enums.Status;
import ru.olegovich.domain.data.pizza.Menu;
import ru.olegovich.domain.data.sales.LocationDatum;
import ru.olegovich.domain.mvp.main.MainMvp;
import ru.olegovich.domain.mvp.main.MainPresenter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static ru.olegovich.domain.data.Constants.ITEM_TITLE_TRANSFER;
import static ru.olegovich.domain.data.Constants.PAGER_TO_MAIN;
import static ru.olegovich.domain.data.Constants.REQUEST_MENU;
import static ru.olegovich.domain.data.Constants.STATUS_OF_VIEW;
import static ru.olegovich.lapiec.ui.PagerFragment.Counter.PAGER_INFO;
import static ru.olegovich.lapiec.ui.PagerFragment.Counter.PAGER_MENU;
import static ru.olegovich.lapiec.ui.PagerFragment.Counter.PAGER_SALES;


public class MainActivity extends BaseActivityMvp2<MainMvp.Presenter, MainMvp.View>
        implements MainMvp.View, SendDataListener {

    private ViewPager2 viewPager;
    private Button cityButton;
    private Button cartButton;
    private ImageButton backButton;
    private ImageView quantityImageView;
    private TextView titleTextView, quantityTextView;
    private BottomNavigationView bottomNavigationView;
    private ScaleAnimation shrink, grow;
    private int quantity;

    @Override
    public MainMvp.Presenter getPresenter() {
        return new MainPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        cityButton = findViewById(R.id.city_button);
        cartButton = findViewById(R.id.cart_button);
        backButton = findViewById(R.id.back_image_button);
        viewPager = findViewById(R.id.main_viewpager);
        titleTextView = findViewById(R.id.title_text_view);
        quantityTextView = findViewById(R.id.quantity_cart_text_view);
        quantityImageView = findViewById(R.id.quantity_cart_image_view);
        bottomNavigationView = findViewById(R.id.bottom_navigation_view);

        ViewPagerFragmentAdapter pagerAdapter = new ViewPagerFragmentAdapter(this);
        pagerAdapter.addFragment(new PagerFragmentBuilder(PAGER_SALES).build());
        pagerAdapter.addFragment(new PagerFragmentBuilder(PAGER_MENU).build());
        pagerAdapter.addFragment(new PagerFragmentBuilder(PAGER_INFO).build());

        viewPager.setAdapter(pagerAdapter);
        viewPager.setUserInputEnabled(false);
        viewPager.setCurrentItem(1);
        titleTextView.setText(R.string.menu_button_item);
        bottomNavigationView.setSelectedItemId(R.id.action_menu);

        presenter.onCreated();

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.action_sales:
                    viewPager.setCurrentItem(0);
                    item.setChecked(true);
                    titleTextView.setText(R.string.sales_button_item);
                    break;
                case R.id.action_menu:
                    viewPager.setCurrentItem(1);
                    item.setChecked(true);
                    titleTextView.setText(R.string.menu_button_item);
                    break;
                case R.id.action_info:
                    viewPager.setCurrentItem(2);
                    item.setChecked(true);
                    titleTextView.setText(R.string.info_button_item);
                    break;
            }
            return false;
        });

        cityButton.setOnClickListener(v -> {
            presenter.onCityButtonClick();
            hideKeyboardFrom(backButton.getContext(), backButton);
        });
        cartButton.setOnClickListener(v -> presenter.onCartButtonClick());
        backButton.setOnClickListener(v -> {
            presenter.onBackButtonClick();
            hideKeyboardFrom(backButton.getContext(), backButton);
        });

        getSupportFragmentManager()
                .setFragmentResultListener(PAGER_TO_MAIN,this,(requestKey, result)
                -> {
                    presenter.onStatusResult(
                            (Status) result.getSerializable(STATUS_OF_VIEW),
                            (Menu) result.getSerializable(ITEM_TITLE_TRANSFER));
                });
//        onItemClickListener(it -> {
//            return Unit.INSTANCE;
//        });

    }

//    private Function1<LocationDatum, Unit> listener;
//
//    private void onItemClickListener(Function1<LocationDatum, Unit> listener) {
//        this.listener = listener;
//
//    }

    @Override
    public void animationQuantityCart() {
        quantityImageView.setImageResource(R.drawable.krug_cart);
        shrink = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(200);
        shrink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                if (quantity == 0) {
                    showVisibilityOfQuantity(GONE);
                } else {
                    quantityImageView.startAnimation(grow);
                    quantityTextView.setText(String.valueOf(quantity));
                    showVisibilityOfQuantity(VISIBLE);
                }
            }

        });
        grow = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        grow.setDuration(200);
    }

    @Override
    public void showTotalSumCart(int sum) {
            cartButton.setText(cartButton.getResources().getString(R.string.cart_total_sum, sum));
    }

    private void showVisibilityOfQuantity(int vis) {
        quantityTextView.setVisibility(vis);
        quantityImageView.setVisibility(vis);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void initButton(City city) {
        switch (city) {
            case NOT_SELECTED:
                break;
            case LVIV:
                cityButton.setText(R.string.city_choose_lviv_radio_button);
                break;
            case VINNYTSIA:
                cityButton.setText(R.string.city_choose_vinnitsa_radio_button);
                break;
            case IVANO_FRANKIVSK:
                cityButton.setText(R.string.city_choose_ivanof_radio_button);
                break;
            case LUTSK:
                cityButton.setText(R.string.city_choose_lutsk_radio_button);
                break;
        }
    }

    @Override
    public void setVisibility(Status status, Menu titleCart) {
        switch (status) {
            case ABOUT:
                setViewVisibility(GONE, GONE, VISIBLE, GONE);
                titleTextView.setText(getString(R.string.title_info_about_us));
                break;
            case ADD_MODIFIER:
                setViewVisibility(GONE, GONE, VISIBLE, VISIBLE);
                titleTextView.setText(getString(R.string.title_modifier));
                break;
            case CONTACTS:
                setViewVisibility(GONE, GONE, VISIBLE, GONE);
                titleTextView.setText(getString(R.string.title_info_contact));
                break;
            case DELIVERY:
                setViewVisibility(GONE, GONE, VISIBLE, GONE);
                titleTextView.setText(getString(R.string.title_info_delivery));
                break;
            case INFO:
                setViewVisibility(VISIBLE, VISIBLE, GONE, VISIBLE);
                titleTextView.setText(getString(R.string.info_button_item));
                break;
            case MENU:
                setViewVisibility(VISIBLE, VISIBLE, GONE, VISIBLE);
                titleTextView.setText(getString(R.string.menu_button_item));
                break;
            case PARTNERS:
                setViewVisibility(GONE, GONE, VISIBLE, GONE);
                titleTextView.setText(getString(R.string.title_info_partner));
                break;
            case PIZZA_DETAILS:
                setViewVisibility(GONE, GONE, VISIBLE, VISIBLE);
                titleTextView.setText(titleCart.getTitle().getRendered());
                break;
            case PIZZA:
                setViewVisibility(VISIBLE, GONE, VISIBLE, VISIBLE);
                titleTextView.setText(getString(R.string.pizza));
                break;
            case SALES_DETAILS:
                setViewVisibility(VISIBLE, GONE, VISIBLE, VISIBLE);
                titleTextView.setText(getString(R.string.sales_button_item));
                break;
            case SALES:
                setViewVisibility(VISIBLE, VISIBLE, GONE, VISIBLE);
                titleTextView.setText(getString(R.string.sales_button_item));
                break;
            case VACANCY:
                setViewVisibility(VISIBLE, VISIBLE, GONE, GONE);
                titleTextView.setText(getString(R.string.title_info_vacancy));
                break;
        }
    }

    @Override
    public void showUpdatedList(List<Menu> menu) {
        Intent intent = new Intent(this, CartActivity.class);
        intent.putExtra("Menu", (Serializable) menu);
        startActivityForResult(intent, REQUEST_MENU);
    }

    @Override
    public void onGoToCityActivity() {
        goToCityActivity();
    }

    @Override
    public void back() {
        onBackPressed();
    }

    @Override
    public void onActivityResultToPizzaFragment(List<Menu> menu) {
        updatePizzaFragment(menu);
    }

    @Override
    public void showAnimationCart(int quantity) {
        quantityImageView.startAnimation(shrink);
        this.quantity = quantity;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        for (Fragment fragManager : fragmentManager.getFragments()) {
            if (fragManager.isVisible()) {
                FragmentManager childFragmentManager = fragManager.getChildFragmentManager();
                if (childFragmentManager.getBackStackEntryCount() > 0) {
                    childFragmentManager.popBackStack();
                    return;
                }
            }
        }
        super.onBackPressed();
    }

    @Override
    public void sendData(@NotNull Menu menuItem) {
        presenter.onResult(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_MENU) {
            List<Menu> menu = (List<Menu>) data.getSerializableExtra("Menu");
            if (menu != null) presenter.menuList(menu);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setViewVisibility(int buttonNav, int city, int back, int cart) {
        bottomNavigationView.setVisibility(buttonNav);
        cityButton.setVisibility(city);
        backButton.setVisibility(back);
        cartButton.setVisibility(cart);
    }

    public void goToCityActivity(){
        Intent intent = new Intent(this, CityActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.bottom_in, R.anim.top_out);
    }

    private void updatePizzaFragment(List<Menu> menu) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment.isVisible() && fragment instanceof PagerFragment) {
               ((PagerFragment) fragment).updatePizzaFragment(menu);
            }
        }
    }

}*/
