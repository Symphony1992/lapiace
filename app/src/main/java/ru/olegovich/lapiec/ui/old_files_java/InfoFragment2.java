/**package ru.olegovich.lapiec.ui.old_files_java;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import ru.olegovich.domain.mvp.info_screen.InfoMvp;
import ru.olegovich.domain.mvp.info_screen.InfoPresenter;
import ru.olegovich.lapiec.ui.AboutFragment;
import ru.olegovich.lapiec.ui.ContactsFragment;
import ru.olegovich.lapiec.ui.DeliveryFragment;
import ru.olegovich.lapiec.ui.PartnersFragment;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.VacancyFragment;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

public class InfoFragment2 extends BaseFragmentMvp<InfoMvp.Presenter, InfoMvp.View>
        implements InfoMvp.View {

    @Override
    public InfoMvp.Presenter getPresenter() {
        return new InfoPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.info_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CardView aboutUsCardView = view.findViewById(R.id.info_about_card_view);
        CardView contactsCardView = view.findViewById(R.id.info_contact_card_view);
        CardView vacancyCardView = view.findViewById(R.id.info_vacancy_card_view);
        CardView partnersCardView = view.findViewById(R.id.info_partner_card_view);
        CardView deliveryCardView = view.findViewById(R.id.info_delivery_card_view);

        presenter.onCreated();

        aboutUsCardView.setOnClickListener(v -> presenter.onAboutUsCardViewClick());
        contactsCardView.setOnClickListener(v -> presenter.onContactsCardViewClick());
        vacancyCardView.setOnClickListener(v -> presenter.onVacancyCardViewClick());
        partnersCardView.setOnClickListener(v -> presenter.onPartnersCardViewClick());
        deliveryCardView.setOnClickListener(v -> presenter.onDeliveryCardViewClick());

    }

    @Override
    public void startAboutUsFragment() {
        start(new AboutFragment());
    }

    @Override
    public void startContactsFragment() {
        start(new ContactsFragment());
    }

    @Override
    public void startVacancyFragment() {
        start(new VacancyFragment());
    }

    @Override
    public void startPartnersFragment() {
        start(new PartnersFragment());
    }

    @Override
    public void startDeliveryFragment() {
        start(new DeliveryFragment());
    }

    public void start(Fragment fragment){
        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.pager_container, fragment)
                .addToBackStack(null)
                .commit();
    }

}*/
