/**package ru.olegovich.lapiec.ui.old_files_java;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.olegovich.domain.mvp.contacts_screen.ContactsMvp;
import ru.olegovich.domain.mvp.contacts_screen.ContactsPresenter;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;
import ru.olegovich.lapiec.util.MyUtil;

import static android.content.Intent.EXTRA_EMAIL;
import static ru.olegovich.domain.data.Constants.EMAIL_ADDRESS;
import static ru.olegovich.domain.data.Constants.GEO_POSITION_OFFICE;
import static ru.olegovich.domain.data.Constants.MAIL_TITLE;
import static ru.olegovich.domain.data.Constants.MAPS_PACKAGE;
import static ru.olegovich.domain.data.Constants.PHONE_NUMBER;
import static ru.olegovich.domain.data.Constants.PLAIN_TEXT;

public class ContactsFragment extends BaseFragmentMvp<ContactsMvp.Presenter, ContactsMvp.View>
        implements ContactsMvp.View {

    private ImageButton phoneImageButton, locationImageButton;
    private ImageButton fbImageButton, instImageButton, emailImageButton;

    @Override
    public int layoutRes() {
        return R.layout.contacts_fragment;
    }

    @Override
    public ContactsPresenter getPresenter() {
        return new ContactsPresenter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        phoneImageButton = view.findViewById(R.id.phone_image_button);
        locationImageButton = view.findViewById(R.id.location_image_button);
        fbImageButton = view.findViewById(R.id.fb_image_button);
        instImageButton = view.findViewById(R.id.inst_image_button);
        emailImageButton = view.findViewById(R.id.email_image_button);

        presenter.onCreated();
    }

    @Override
    public void startCallActivity() {
        Intent intentCall = new Intent(Intent.ACTION_DIAL);
        intentCall.setData(Uri.parse(PHONE_NUMBER));
        phoneImageButton.setOnClickListener(v -> startActivity(intentCall));
    }

    @Override
    public void startMapActivity() {
        Intent intentMap = new Intent(Intent.ACTION_VIEW, Uri.parse(GEO_POSITION_OFFICE));
        intentMap.setPackage(MAPS_PACKAGE);
//        if (intentMap.resolveActivity(getPackageManager()) != null) {  startActivity(intentMap);  }
        locationImageButton.setOnClickListener(v -> startActivity(intentMap));
    }

    @Override
    public void startEmailActivity() {
        Intent intentEmail = MyUtil.openEmailIntent();
        intentEmail.setType(PLAIN_TEXT);
        intentEmail.putExtra(EXTRA_EMAIL, new String[]{EMAIL_ADDRESS});
//        intentEmail.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
        emailImageButton.setOnClickListener(v ->
                startActivity(Intent.createChooser(intentEmail, MAIL_TITLE)));
    }

    @Override
    public void startFacebookActivity() {
        Intent intentFacebook = MyUtil.openFacebookIntent(getContext());
        fbImageButton.setOnClickListener(v -> startActivity(intentFacebook));
    }

    @Override
    public void startInstagramActivity() {
        Intent intentInstagram = MyUtil.openInstagramIntent();
        instImageButton.setOnClickListener(v -> startActivity(intentInstagram));
    }
}*/
