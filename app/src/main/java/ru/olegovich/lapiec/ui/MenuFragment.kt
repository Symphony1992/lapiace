package ru.olegovich.lapiec.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.menu_fragment.*
import ru.olegovich.domain.data.entity.enums.Counter.*
import ru.olegovich.domain.mvp.menu_screen.MenuMvp
import ru.olegovich.domain.mvp.menu_screen.MenuPresenter
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp
import ru.olegovich.lapiec.ui.base.BaseViewBindFragmentMvp
import ru.olegovich.lapiec.ui.databinding.MenuFragmentBinding

class MenuFragment : BaseViewBindFragmentMvp<
            MenuMvp.Presenter,
            MenuMvp.View,
            MenuFragmentBinding>(),
        MenuMvp.View {

    override val bindingInflater: (
            LayoutInflater,
            ViewGroup?,
            Boolean) -> MenuFragmentBinding = MenuFragmentBinding::inflate

    override fun presenter(): MenuMvp.Presenter = MenuPresenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreated()
        with(binding){
            menuPizzaImageView.setOnClickListener { presenter.onMenuPizzaImageViewClick() }
            menuSaladImageView.setOnClickListener { presenter.onSaladPizzaImageViewClick() }
            menuDrinkImageView.setOnClickListener { presenter.onDrinkPizzaImageViewClick() }
        }
    }

    override fun startPizzaFragment() = start(PizzaFragmentBuilder(PIZZA).build())

    override fun startSaladFragment() = start(PizzaFragmentBuilder(SALAD).build())

    override fun startDrinkFragment() = start(PizzaFragmentBuilder(DRINK).build())

    private fun start(fragment: Fragment) {
        parentFragmentManager
                .beginTransaction()
                .replace(R.id.pager_container, fragment)
                .addToBackStack(null)
                .commit()
    }

}