/**package ru.olegovich.lapiec;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ru.olegovich.lapiec.ui.R;

public class PizzaDetailsAdapter extends RecyclerView.Adapter<PizzaDetailsAdapter.ViewHolder>{

    private String[] items;

    public PizzaDetailsAdapter(String[] items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_pizza_details_recycleview, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.itemTextView.setText(firstUpperCase(items[position]));
    }

    @Override
    public int getItemCount() {   return items.length;    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemTextView = itemView.findViewById(R.id.item_text_view);
        }
    }

    public String firstUpperCase (String s){
        return s.substring( 0, 1 ).toUpperCase() + s.substring(1);
    }

}*/
