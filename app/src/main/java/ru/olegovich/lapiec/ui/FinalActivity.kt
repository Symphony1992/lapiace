package ru.olegovich.lapiec.ui

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolygonOptions
import com.google.maps.android.PolyUtil
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import com.wdullaer.materialdatetimepicker.time.Timepoint
import kotlinx.android.synthetic.main.activity_final.*
import ru.olegovich.domain.data.entity.coordinates.ZonePath
import ru.olegovich.domain.data.entity.enums.DeliveryCounter
import ru.olegovich.domain.data.entity.enums.DeliveryCounter.*
import ru.olegovich.domain.data.entity.enums.*
import ru.olegovich.domain.data.entity.enums.PaymentCounter.*
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.streets.Street
import ru.olegovich.domain.mvp.final_screen.FinalMvp
import ru.olegovich.domain.mvp.final_screen.FinalPresenter
import ru.olegovich.lapiec.adapters.FinalAdapter
import ru.olegovich.lapiec.ui.base.BaseViewBindActivityMvp
import ru.olegovich.lapiec.ui.databinding.ActivityFinalBinding
import ru.olegovich.lapiec.util.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*

class FinalActivity : BaseViewBindActivityMvp<
            FinalMvp.Presenter,
            FinalMvp.View,
            ActivityFinalBinding>(),
        FinalMvp.View,
        OnMapReadyCallback,
        AdapterView.OnItemSelectedListener,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener
{

    private var polygonGreen = mutableListOf<LatLng>()
    private var polygonYellow = mutableListOf<LatLng>()
    private lateinit var googleMap: GoogleMap

    override val bindingInflater: (LayoutInflater) -> ActivityFinalBinding =
            ActivityFinalBinding::inflate

    override fun presenter(): FinalMvp.Presenter = FinalPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initGoogleMap(savedInstanceState)

        with(binding) {
            finalApplyButton.setOnClickListener {
                presenter.applyButtonClick()
                it.hideKeyboard()
            }
            finalBackImageButton.setOnClickListener {
                presenter.backButtonClick()
                it.hideKeyboard()
            }
            isCallMeCheckBox.setOnCheckedChangeListener { view, isChecked ->
                presenter.onCallMeCheckedChange(isChecked)
                view.hideKeyboard()
            }
            finalFindButton.setOnClickListener {
                presenter.findAddress(
                        finalStreetView.text.toString(), finalNumberEditText.text.toString())
                it.hideKeyboard()
            }
            finalDateTextView.setOnClickListener { presenter.onDateTextClick() }
            finalTimeTextView.setOnClickListener { presenter.onTimeTextClick() }
            finalPaymentRadioGroup.setOnCheckedChangeListener { _, idView ->
                when (idView){
                    R.id.final_cash_radio_button -> presenter.paymentChoose(CASH)
                    R.id.final_card_radio_button -> presenter.paymentChoose(CARD)
                }
            }
            finalDeliveryRadioGroup.setOnCheckedChangeListener { _, idView ->
                when (idView){
                    R.id.final_delivery_radio_button -> presenter.deliveryChoose(DELIVERY)
                    R.id.final_pickup_radio_button -> presenter.deliveryChoose(PICKUP)
                    R.id.final_advance_radio_button -> presenter.deliveryChoose(ADVANCE)
                }
            }
            finalCitySpinner.onItemSelectedListener = this@FinalActivity
        }
    }

    private fun initGoogleMap(savedInstanceState: Bundle?) {
        var mapViewBundle: Bundle? = null
        savedInstanceState?.let {
            mapViewBundle = savedInstanceState.getBundle(FINAL_MAP_VIEW_BUNDLE_KEY) }
        binding.finalMapView.apply {
            onCreate(mapViewBundle)
            getMapAsync(this@FinalActivity)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var mapViewBundle = outState.getBundle(FINAL_MAP_VIEW_BUNDLE_KEY)
        mapViewBundle?.let {
            mapViewBundle = Bundle()
            outState.putBundle(FINAL_MAP_VIEW_BUNDLE_KEY, mapViewBundle)
        }
        binding.finalMapView.onSaveInstanceState(mapViewBundle)
    }

    override fun setVisibility(payment: DeliveryCounter) {
        when (payment){
            DELIVERY -> setVisibilityOfView(VISIBLE, GONE, GONE, GONE, VISIBLE)
            PICKUP -> setVisibilityOfView(GONE, GONE, VISIBLE, GONE, GONE)
            ADVANCE -> setVisibilityOfView(VISIBLE, VISIBLE, GONE, VISIBLE, GONE)
        }
    }

    private fun setVisibilityOfView(group: Int, timeDate: Int, cafeAddress: Int,
                                    attention: Int, callMe: Int) {
        setVisibilityOfGroup(group)
        setVisibilityOfTimeDate(timeDate)
        binding.apply {
            finalCafeAddressEditText.visibility = cafeAddress
            attentionTextView.visibility = attention
            isCallMeCheckBox.visibility = callMe
        }
        setVisibilityInclude(false) // TODO: 15.04.2021 check
    }

    private fun setVisibilityOfGroup(visibility: Int) {
        with(binding) {
            finalCitySpinner.visibility = visibility
            finalStreetView.visibility = visibility
            finalNumberEditText.visibility = visibility
            finalMapView.visibility = visibility
            finalFindButton.visibility = visibility
        }
    }

    private fun setVisibilityOfTimeDate(visibility: Int) {
        with(binding) {
            finalDateTextView.visibility = visibility
            finalDateImageView.visibility = visibility
            finalTimeTextView.visibility = visibility
            finalTimeImageView.visibility = visibility
        }
    }

    override fun setVisibilityInclude(isVisible: Boolean) {
        binding.includeFinal.finalIncludeConstrLayout.isVisible = isVisible
    }

    override fun showFinalToast() = toast(R.string.final_toast)


    override fun onLatLngZoom(lat: Double, lng: Double, zoom: Float) = googleMap.
        moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), zoom))

    override fun showCoordinate(zonePath: ZonePath, zone_color: Boolean) {
        val latLngBounds = LatLngBounds.Builder()
        val polygonOptions = PolygonOptions()
                .fillColor(Color.parseColor(zonePath.color.replace("#", "#6E")))
                .strokeColor(setColor(zonePath.color))
                .strokeWidth(2F)
        zonePath.coordinates.forEach {
            polygonOptions.add(LatLng(it.lat,it.lng))
            latLngBounds.include(LatLng(it.lat,it.lng))
            if (zone_color) polygonGreen.add(LatLng(it.lat,it.lng))
            else polygonYellow.add(LatLng(it.lat,it.lng))
        }
        googleMap.addPolygon(polygonOptions)
    }

    private fun setColor(color: String): Int = Color.parseColor(color)

    override fun showMenuList(menu: List<Menu>) {
        binding.finalRecyclerView.adapter = FinalAdapter(menu)
    }

    override fun showTotalSum(sum: Int) {
        binding.finalSumTextView.text(R.string.final_sum, sum)
    }

    override fun showCity(cities: List<String>, pos: Int) {
        binding.finalCitySpinner.apply {
            adapter = ArrayAdapter(this@FinalActivity, R.layout.final_city_row, cities)
            setSelection(pos)
        }
    }

    override fun setStreets(streets: List<Street>) {
        val street = mutableListOf<String>()
        streets.map { street.add(it.name) }
        binding.finalStreetView
                .setAdapter(ArrayAdapter(this, R.layout.final_streets_row, street))
    }

    override fun showLatLngResult(lat: Double, lng: Double) {
        val point = LatLng(lat, lng)
        val inGreenZone = PolyUtil.containsLocation(point, polygonGreen, true)
        val inYellowZone = PolyUtil.containsLocation(point, polygonYellow, true)
        if (inYellowZone && !inGreenZone) zoneChosen(false)
        else if (inGreenZone) zoneChosen(true)
        else presenter.noZoneChosen()
        onLatLngZoom(point.latitude, point.longitude,17F)
    }

    private fun zoneChosen(zone_color: Boolean) {
        if (!zone_color) presenter.yellowZoneSumCheck()
        else presenter.greenZoneSumCheck()
    }

    override fun backToCart() = onBackPressed()

    override fun onResume() {
        binding.finalMapView.onResume()
        super.onResume()
    }

    override fun onPause() {
        binding.finalMapView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        binding.finalMapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        onLowMemory()
    }

    override fun showErrorFindToast() = toast(R.string.final_address_error)

    override fun showErrorZoneText() {
        binding.finalErrorTextView.apply {
            isVisible = true
            text(R.string.final_error_zone)
        }
    }

    override fun setVisibilityError() {
        binding.finalErrorTextView.isVisible = true
    }

    override fun showErrorNoZoneText() {
        binding.finalErrorTextView.apply {
            isVisible = true
            text(R.string.final_error_no_zone)
        }
    }

    override fun onChooseTime() {
        val calendar = getInstance()
        val timePoints = mutableListOf<Timepoint>()
        for (i in 0..585 step 15) timePoints.add(Timepoint(i % 60, i))
        TimePickerDialog.newInstance(
                    this,
                    calendar[HOUR_OF_DAY],
                    calendar[MINUTE],
                    true).let {
            it.enableSeconds(false)
            it.version = TimePickerDialog.Version.VERSION_2
            it.accentColor = Color.parseColor("#FFBB33")
            it.setTimeInterval(1, 15)
            it.setDisabledTimes(timePoints.toTypedArray())
            it.setLocale(Locale("uk", "UA"))
            it.setCancelText(R.string.date_time_cancel)
            it.setOkText(R.string.date_time_ok)
            it.setOnCancelListener { _ -> it.dismiss() }
            it.show(supportFragmentManager, TIME_PICKER_DIALOG)
        }
    }

    override fun onChooseDate() {
        val calendar = getInstance()
        DatePickerDialog.newInstance(
                    this,
                    calendar[HOUR_OF_DAY],
                    calendar[MINUTE],
                    calendar[DAY_OF_MONTH]
            ).let {
            it.version = DatePickerDialog.Version.VERSION_2
            it.setAccentColor("#FFBB33")
            val days = arrayOfNulls<Calendar>(29)
            for (i in 0..28) {
                val day = getInstance()
                day.add(DAY_OF_MONTH, i)
                days[i] = day
            }
            it.selectableDays = days
            it.locale = Locale("uk", "UA")
            it.setCancelText(R.string.date_time_cancel)
            it.setOkText(R.string.date_time_ok)
            it.setOnCancelListener { _ -> it.dismiss() }
            it.show(supportFragmentManager, DATE_PICKER_DIALOG)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        presenter.onCreated(intent.getSerializableExtra(FINAL_ACTIVITY) as List<Menu>)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        presenter.onCityChosen(parent?.getItemAtPosition(position).toString())
        with(binding){
            finalStreetView.empty()
            finalNumberEditText.empty()
            finalErrorTextView.isVisible = false
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) = toast(R.string.city_not_chosen)

    override fun onTimeSet(view: TimePickerDialog, hourOfDay: Int, minute: Int, second: Int) {
        var startMinute = minute.toString()
        if (minute == 0) startMinute = "00"
        val startTime = "$hourOfDay:$startMinute"
        val endMinute: String
        var endHour = hourOfDay
        if (minute == 45) {
            endMinute = "00"
            endHour++
        } else endMinute = (minute + 15).toString()
        val finalTime = "$endHour:$endMinute"
        binding.finalTimeTextView.text(R.string.final_time_set, startTime, finalTime)
    }

    override fun onDateSet(view: DatePickerDialog, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val calendar = getInstance()
        calendar.set(year, monthOfYear, dayOfMonth)
        val formatDay = SimpleDateFormat("dd MMMM yyyy", Locale("uk","UA"))
        val date = formatDay.format(calendar.time)
        binding.finalDateTextView.text = date
    }

}