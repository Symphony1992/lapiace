package ru.olegovich.lapiec.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import ru.olegovich.domain.mvp.base.BaseMvp
import java.io.Serializable

abstract class BaseViewBindActivityMvp <
        P : BaseMvp.BasePresenter<V>,
        V : BaseMvp.BaseView,
        B : ViewBinding>
: AppCompatActivity(), BaseMvp.BaseView {

    abstract fun presenter(): P
    protected lateinit var presenter: P
    private var _binding: ViewBinding? = null
    abstract val bindingInflater: (LayoutInflater) -> B

    protected val binding: B
        get() = _binding as B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = bindingInflater.invoke(layoutInflater)
        setContentView(requireNotNull(_binding).root)
        presenter = presenter()
        presenter.attachView(this as V)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
        _binding = null
    }

    override fun setFragmentResultToActivity(key: String, requestKey: String, value: Serializable) {
    }

    override fun setFragmentResultToActivity(key: String,
                                             key2: String,
                                             requestKey: String,
                                             value: Serializable,
                                             value2: Serializable) {
    }

}