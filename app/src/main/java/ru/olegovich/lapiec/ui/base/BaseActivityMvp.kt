package ru.olegovich.lapiec.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import ru.olegovich.domain.mvp.base.BaseMvp.BasePresenter
import ru.olegovich.domain.mvp.base.BaseMvp.BaseView
import java.io.Serializable

abstract class BaseActivityMvp<P : BasePresenter<V>, V : BaseView> : AppCompatActivity(), BaseView {

    abstract fun presenter(): P
    protected lateinit var presenter: P
    abstract val binding: ViewBinding

    abstract val layoutRes: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter = presenter()
        presenter.attachView(this as V)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun setFragmentResultToActivity(key: String, requestKey: String, value: Serializable) {
    }

    override fun setFragmentResultToActivity(key: String,
                                         key2: String,
                                         requestKey: String,
                                         value: Serializable,
                                         value2: Serializable) {
    }

}