/**package ru.olegovich.lapiec.ui.old_files_java;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.olegovich.domain.data.sales.LocationDatum;
import ru.olegovich.domain.mvp.sales_screen.SalesMvp;
import ru.olegovich.domain.mvp.sales_screen.SalesPresenter;
import ru.olegovich.lapiec.SalesAdapter;
import ru.olegovich.lapiec.ui.R;
import ru.olegovich.lapiec.ui.base.BaseFragmentMvp;

public class SalesFragment2 extends BaseFragmentMvp<SalesMvp.Presenter, SalesMvp.View>
        implements SalesMvp.View {

    private RecyclerView recyclerView;
    private View include;

    @Override
    public SalesMvp.Presenter getPresenter() {
        return new SalesPresenter();
    }

    @Override
    public int layoutRes() {
        return R.layout.sales_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.sales_recycler_view);
        include = view.findViewById(R.id.no_internet_sales_include);
        Button tryAgain = view.findViewById(R.id.no_internet_try_again_button);

        presenter.onCreated();

        tryAgain.setOnClickListener(v -> presenter.onTryAgain());

    }

    @Override
    public void showSales(List<LocationDatum> locationDatum) {
        SalesAdapter adapter = new SalesAdapter(locationDatum);
        adapter.setOnItemClickListener(item -> {
            getParentFragmentManager()
                    .beginTransaction()
                    .replace(R.id.pager_container, new SalesDetailsFragmentBuilder(item).build())
                    .addToBackStack(null)
                    .commit();
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError() {
        recyclerView.setVisibility(View.GONE);
        include.setVisibility(View.VISIBLE);
    }

    @Override
    public void reOpen() {
        recyclerView.setVisibility(View.VISIBLE);
        include.setVisibility(View.GONE);
    }

}*/
