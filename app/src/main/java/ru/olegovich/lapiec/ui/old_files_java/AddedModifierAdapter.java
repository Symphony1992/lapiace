/**package ru.olegovich.lapiec;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ru.olegovich.domain.data.modifiers.Modifier;
import ru.olegovich.lapiec.adapters.AddedModifierAdapter;
import ru.olegovich.lapiec.ui.R;

public class AddedModifierAdapter extends RecyclerView.Adapter<AddedModifierAdapter.ViewHolder> {

    private ArrayList<Modifier> items = new ArrayList<>();
    private OnItemClickListener listener;

    public AddedModifierAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void update(ArrayList<Modifier> modifiers) {
        items.clear();
        items.addAll(modifiers);
        notifyDataSetChanged();
    }

    public void removeItem(Modifier item, int position) {
        items.remove(item);
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_pizza_modifier_recyclerview, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Modifier item = items.get(position);
        if(item.isSelected()) {
            holder.pModTitleTextView.setText(item.getTitle().getRendered());
            holder.pModWeightTextView
                    .setText(holder.pModWeightTextView
                            .getContext().getString(R.string.gram, item.getMetaData().getWeight()));
            holder.pModPriceTextView.setText(String.valueOf(item.getMetaData().getPrice()));
            Glide
                    .with(holder.pModImageView)
                    .load(item.getMetaData().getMobImg())
                    .into(holder.pModImageView);
            holder.pModCancelImageButton.setOnClickListener(view -> {
                    listener.onItemClick(item, position);
            });
        }
    }

    public boolean isEmpty() { return items.isEmpty(); }

    @Override
    public int getItemCount() {        return items.size();    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView pModTitleTextView;
        TextView pModWeightTextView;
        TextView pModPriceTextView;
        ImageButton pModCancelImageButton;
        ImageView pModImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pModTitleTextView = itemView.findViewById(R.id.pizza_modifier_title_text_view);
            pModWeightTextView = itemView.findViewById(R.id.pizza_modifier_weight_text_view);
            pModPriceTextView = itemView.findViewById(R.id.pizza_modifier_price_text_view);
            pModCancelImageButton = itemView.findViewById(R.id.pizza_modifier_cancel_image_button);
            pModImageView = itemView.findViewById(R.id.pizza_modifier_image_view);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Modifier item, int position);
    }
}*/
