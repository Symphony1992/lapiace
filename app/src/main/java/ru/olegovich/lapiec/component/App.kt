package ru.olegovich.lapiec.component

import android.app.Application
import ru.olegovich.domain.component.ComponentProvider

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        ComponentProvider.inject(ComponentProviderImpl(this))
    }
}