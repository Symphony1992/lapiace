package ru.olegovich.lapiec.component

import ru.olegovich.domain.component.DataRepository
import ru.olegovich.domain.data.entity.coordinates.Delivery
import ru.olegovich.domain.data.entity.enums.UpdatableData.*
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.data.entity.vacancies.Vacancy
import ru.olegovich.domain.data.local.Database
import ru.olegovich.domain.data.local.cache.DiskCache
import ru.olegovich.domain.data.repository.CacheRepository
import ru.olegovich.domain.data.repository.RemoteRepository
import ru.olegovich.domain.data.repository.Repository

class DataRepositoryImpl(
        private val preference: PreferenceImpl,
        private val schedulers: AppSchedulersImpl,
        private val db: Database
) : DataRepository {

    override val vacancy: Repository<Vacancy>
        get() = CacheRepository(preference, VACANCIES,DiskCache(db.vacancyDao, schedulers), remoteVacancy)

    private val remoteVacancy: Repository<Vacancy>
        get() = RemoteRepository(preference, schedulers) { vacancyResult() }

    override val modifier: Repository<Modifier>
        get() = CacheRepository(preference, MODIFIERS,DiskCache(db.modifierDao, schedulers), remoteModifier)

    private val remoteModifier: Repository<Modifier>
        get() = RemoteRepository(preference, schedulers) { modifierResult() }

    override val menu: Repository<Menu>
        get() = CacheRepository(preference, MENU, DiskCache(db.menuDao, schedulers), remoteMenu)

    private val remoteMenu: Repository<Menu>
        get() = RemoteRepository(preference, schedulers) { pizzaResult() }

    override val sale: Repository<LocationDatum>
        get() = CacheRepository(preference, SALES, DiskCache(db.saleDao, schedulers), remoteSale)

    private val remoteSale: Repository<LocationDatum>
        get() = RemoteRepository(preference, schedulers) { salesResult() }

    override val delivery: Repository<Delivery>
        get() = CacheRepository(preference, COORDINATES, DiskCache(db.coordinateDao, schedulers), remoteDelivery)

    private val remoteDelivery: Repository<Delivery>
        get() = RemoteRepository(preference, schedulers) {
            coordinateResult(preference.city.url).map {
            listOf(it)}
        }

}