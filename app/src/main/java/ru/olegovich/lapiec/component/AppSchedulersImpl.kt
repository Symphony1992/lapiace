package ru.olegovich.lapiec.component

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.olegovich.domain.component.AppSchedulers
import java.util.concurrent.Executors

class AppSchedulersImpl : AppSchedulers {

    override val io: Scheduler = Schedulers.from(Executors.newFixedThreadPool(2))
    override val ui: Scheduler = AndroidSchedulers.mainThread()
    override val db: Scheduler = Schedulers.from(Executors.newSingleThreadExecutor())

}