package ru.olegovich.lapiec.component

import android.content.Context
import androidx.room.Room
import ru.olegovich.domain.component.ComponentProvider
import ru.olegovich.domain.data.local.Database
import ru.olegovich.domain.data.entity.enums.DATABASE_NAME

class ComponentProviderImpl(context: Context) :  ComponentProvider {

    override val preference by lazy { PreferenceImpl(context) }
    override val schedulers by lazy { AppSchedulersImpl() }
    override val dataRepository by lazy { DataRepositoryImpl(preference, schedulers, db) }
    private val db: Database by lazy {
        Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).build()
    }

}