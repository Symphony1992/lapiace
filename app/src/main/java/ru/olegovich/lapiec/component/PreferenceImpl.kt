package ru.olegovich.lapiec.component

import android.content.Context
import androidx.preference.PreferenceManager
import ru.olegovich.domain.component.Preference
import ru.olegovich.domain.data.entity.enums.CITY
import ru.olegovich.domain.data.entity.enums.City
import ru.olegovich.domain.data.entity.enums.LAST_UPDATE_TIME
import ru.olegovich.domain.data.entity.enums.UpdatableData
import ru.olegovich.lapiec.util.toCalendar
import java.util.*

class PreferenceImpl(val context: Context) : Preference {

    private var preference = PreferenceManager.getDefaultSharedPreferences(context)

    override var city: City
        set(value) = preference.edit().putInt(CITY, value.number).apply()
        get() = when (preference.getInt(CITY, -1)) {
            0 -> City.LVIV
            1 -> City.VINNYTSIA
            2 -> City.IVANO_FRANKIVSK
            3 -> City.LUTSK
            else -> City.NOT_SELECTED
        }
//    override var lastUpdateTime: Calendar
//        get() = preference.getLong(LAST_UPDATE_TIME, defaultTime).toCalendar()
//        set(value) = preference.edit().putLong(LAST_UPDATE_TIME, value.timeInMillis).apply()

    private val defaultTime
        get() = Calendar
                .getInstance()
                .apply { set(2019, 1, 1, 1, 1) }
                .timeInMillis

    override fun lastUpdateTime(type: UpdatableData) = preference
            .getLong("${LAST_UPDATE_TIME}_${type.name}", defaultTime)

    override fun lastUpdateTime(type: UpdatableData, value: Long) = preference
            .edit()
            .putLong("${LAST_UPDATE_TIME}_${type.name}", value)
            .apply()
}