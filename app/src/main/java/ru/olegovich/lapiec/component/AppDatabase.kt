package ru.olegovich.lapiec.component

import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.olegovich.domain.data.entity.coordinates.Delivery
import ru.olegovich.domain.data.entity.delivery_address.DeliveryAddress
import ru.olegovich.domain.data.entity.modifiers.Modifier
import ru.olegovich.domain.data.entity.pizza.Menu
import ru.olegovich.domain.data.entity.places.Places
import ru.olegovich.domain.data.entity.sales.LocationDatum
import ru.olegovich.domain.data.entity.streets.Street
import ru.olegovich.domain.data.entity.vacancies.Vacancy
import ru.olegovich.domain.data.local.Database
import ru.olegovich.domain.data.local.converters.*

@androidx.room.Database(
        entities = [
            Vacancy::class,
            Modifier::class,
            Menu::class,
            LocationDatum::class,
            Delivery::class,
//            DeliveryAddress::class,
//            Places::class,
//            Street::class
        ],
        version = 1)
@TypeConverters(
        ResultConverter::class,
        ModifierConverter::class,
        CoordinateConverter::class,
        CandidateConverter::class,
        StringConverter::class
)

abstract class AppDatabase : RoomDatabase(), Database {

}