package ru.olegovich.lapiec.util;

import android.annotation.SuppressLint;
import android.util.Pair;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ThreadsTest {

    /**
     *  выполняется в ui потоке
     */
    @SuppressLint("CheckResult")
    private void exampleRxJava() {
        Single.fromCallable((Callable<Integer>) () -> {
            Thread.sleep(5000);
            return 1;
        }).subscribe(); // неважен резульат, просто выполнение

        Single.fromCallable((Callable<Integer>) () -> {
            Thread.sleep(5000);
            return 1;
        }).subscribe(
                it -> {
                    System.out.println("!!!!" + it);
                },
                throwable -> {
                    throwable.getMessage();
                });

    }
    @SuppressLint("CheckResult")
    private void exampleRxJava2() {

        Single.fromCallable((Callable<Integer>) () -> {
            Thread.sleep(5000);
            return 1;
        })
                .subscribeOn(Schedulers.io())  /// выполнение в "другом" потоке + результат в "другом" потоке
                .subscribe(
                it -> {
                    System.out.println("!!!!" + it);
                },
                throwable -> {
                    throwable.getMessage();
                });

        Single.fromCallable((Callable<Integer>) () -> {
            Thread.sleep(5000);
            return 1;
        })
                .subscribeOn(Schedulers.io())/// выполнение в "другом" потоке
                .observeOn(AndroidSchedulers.mainThread()) /// возврат(результат) в ui поток
                .subscribe(
                it -> {

                    System.out.println("!!!!" + it);
                },
                throwable -> {
                    throwable.getMessage();
                });

    }

    @SuppressLint("CheckResult")
    private void exampleRxJava3() {

        Single.fromCallable((Callable<Integer>) () -> {
            return 2 + 2;
        })
                .subscribeOn(Schedulers.io())
                .flatMap(integer -> {
                    return Single.fromCallable(() -> integer - 1);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                it -> {
                    System.out.println("!!!!" + it); //// it = 3
                },
                throwable -> {
                    throwable.getMessage();
                });

        Single.fromCallable((Callable<Integer>) () -> {
            return 2 + 2;
        })
                .subscribeOn(Schedulers.io())
                .flatMap(integer -> {
                    return Single.fromCallable(() -> "!!!" + (integer - 1));
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                it -> {
                    System.out.println(it); //// String it = "3"
                },
                throwable -> {
                    throwable.getMessage();
                });

    }
    @SuppressLint("CheckResult")
    private void exampleRxJava4() {

        Single.fromCallable((Callable<Integer>) () -> {
            return 2 + 2;
        })
                .subscribeOn(Schedulers.io())
                .map(integer -> {
                    return integer - 1;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                it -> {
                    System.out.println("!!!!" + it); //// it = 3
                },
                throwable -> {
                    throwable.getMessage();
                });

        Single.fromCallable((Callable<Integer>) () -> {
            return 2 + 2;
        })
                .subscribeOn(Schedulers.io())
                .map(integer -> {
                    return  "!!!" + (integer - 1);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                it -> {
                    System.out.println(it); //// String it = "!!!!3"
                },
                throwable -> {
                    throwable.getMessage();
                });

    }

    private Single<Integer> exampleRxJava6(int a) {
        return Single.fromCallable((Callable<Integer>) () -> (a - 1));
    }

    @SuppressLint("CheckResult")
    private void exampleRxJava7() {

        Single.fromCallable((Callable<Integer>) () -> {
            return 2 + 2;
        })
                .subscribeOn(Schedulers.io()) //// поток 1
                .flatMap(integer -> {
                    return Single.fromCallable(() -> "!!!" + (integer - 1)).subscribeOn(Schedulers.io()); /// поток 2
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {
                            System.out.println("!!!!" + it); //// it = 3
                        },
                        throwable -> {
                            throwable.getMessage();
                        });

        Single.fromCallable((Callable<Integer>) () -> {
            return 2 + 2;
        })
                .flatMap(integer -> {
                    return Single.fromCallable(() -> "!!!" + (integer - 1)).subscribeOn(Schedulers.io()); /// поток 2
                })
                .subscribeOn(Schedulers.io()) //// поток 1
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {
                            System.out.println("!!!!" + it); //// it = 3
                        },
                        throwable -> {
                            throwable.getMessage();
                        });
        Single.just(2 + 2).subscribeOn(Schedulers.io()); /// выполнится в ui потоке вернет it = 4 (выполнит в том потоке где он находится)


        Single.fromCallable(() -> {
            return 2 + 2;
        })
                .flatMap(integer -> {
                    return Single.just( "!!!" + (integer - 1));
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {
                            System.out.println("!!!!" + it); //// it = 3
                        },
                        throwable -> {
                            throwable.getMessage();
                        });

    }

    @SuppressLint("CheckResult")
    private void exampleRxJava8() {
        Observable.fromPublisher((Publisher<Integer>) publisher -> {
            for (int i = 0; i < 2; i++) {
                publisher.onNext(i);
            }
            publisher.onComplete();
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {

                            System.out.println("!!!!" + it);
                        },
                        throwable -> {
                            throwable.getMessage();
                        },
                        () -> {

                        });

        List<Integer> list = new ArrayList();
        List<Integer> list1 = new ArrayList<>(3);
        list.add(0);
        list.add(1);
        list.add(2);

        Observable
                .fromIterable(list)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {

                            System.out.println("!!!!" + it);
                        },
                        throwable -> {
                            throwable.getMessage();
                        },
                        () -> {

                        });
    }

    @SuppressLint("CheckResult")
    private void exampleRxJava9() {
        Single.zip(
                Single.fromCallable(() -> (2 + 2)).subscribeOn(Schedulers.io()), /// поток 2
                Single.fromCallable(() -> (1 + 2)).subscribeOn(Schedulers.io()), /// поток 3
                ((it, it2) -> it + it2)
        )
                .subscribeOn(Schedulers.io()) /// поток 1
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {
                            System.out.println("!!!!" + it); //// it = 3
                        },
                        throwable -> {
                            throwable.getMessage();
                        });
        List<Integer> list = new ArrayList();
        list.add(0);
        list.add(1);
        list.add(2);
        List<Integer> list2 = new ArrayList();
        list.add(0);
        list.add(1);
        list.add(2);
        Single.zip(
                Single.fromCallable(() -> list).subscribeOn(Schedulers.io()), /// поток 2
                Single.fromCallable(() -> list2).subscribeOn(Schedulers.io()), /// поток 3
                ((it, it2) -> it.addAll(it2))
        )
                .subscribeOn(Schedulers.io()) /// поток 1
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {
                            System.out.println("!!!!" + it); //// it = 3
                        },
                        throwable -> {
                            throwable.getMessage();
                        });
        List<Integer> list3 = new ArrayList();
        list.add(0);
        list.add(1);
        list.add(2);
        List<String> list4 = new ArrayList();
        list.add(0);
        list.add(1);
        list.add(2);
        Single.zip(
                Single.fromCallable(() -> list3).subscribeOn(Schedulers.io()), /// поток 2
                Single.fromCallable(() -> list4).subscribeOn(Schedulers.io()), /// поток 3
                ((it, it2) -> new Pair<List<Integer>, List<String>>(it, it2))
        )
                .subscribeOn(Schedulers.io()) /// поток 1
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> {

                            System.out.println("!!!!" + it.first); //// list3
                            System.out.println("!!!!" + it.second); //// list4
                        },
                        throwable -> {
                            throwable.getMessage();
                        });
    }
}
