package ru.olegovich.lapiec.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.Iterator;
import java.util.List;

import ru.olegovich.domain.data.entity.enums.ConstantsKt;

public class MyUtil {

    public static <T> void filter(List<T> list, Predicate<T> predicate) {
        if (list == null) return;
        Iterator<T> iterator = list.iterator();
        while (iterator.hasNext()) {
            T item = iterator.next();
            if (!predicate.satisfies(item)) iterator.remove();
        }
    }

    public interface Predicate<T> {
        boolean satisfies(T item);
    }

    public static Intent openFacebookIntent (Context context){
        try {
            int versionCode = context
                    .getPackageManager()
                    .getPackageInfo(ConstantsKt.FACEBOOK_PACKAGE, 0).versionCode;
            if (versionCode >= 3002850){
                return new Intent(Intent.ACTION_VIEW, Uri.parse(ConstantsKt.FACEBOOK_NEW_URL));
            } else {
                return new Intent(Intent.ACTION_VIEW, Uri.parse(ConstantsKt.FACEBOOK_OLD_URL));
            }
        }
        catch (Exception e){
            return new Intent(Intent.ACTION_VIEW, Uri.parse(ConstantsKt.FACEBOOK_DEFAULT_URL));
        }
    }

    public static Intent openInstagramIntent (){
        return new Intent(Intent.ACTION_VIEW, Uri.parse(ConstantsKt.INSTAGRAM_URL));
    }

    public static Intent openEmailIntent (){
        return new Intent(Intent.ACTION_SEND);
    }

}
