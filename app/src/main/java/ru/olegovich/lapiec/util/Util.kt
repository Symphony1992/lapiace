package ru.olegovich.lapiec.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Build.*
import android.text.Html
import android.text.Html.FROM_HTML_MODE_COMPACT
import android.text.Spanned
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import java.util.*
import kotlin.math.roundToInt

/** Util Int, Long, Float, Double */

inline val Int.dpToPx get() = (this * (Resources.getSystem().displayMetrics.density + 0.25F)).roundToInt()

inline val Float.dpToPx get() = this * (Resources.getSystem().displayMetrics.density + 0.25F)

inline val Int.pxToDp get() = (this / (Resources.getSystem().displayMetrics.density + 0.25F)).roundToInt()

inline val Float.pxToDp get() = this / (Resources.getSystem().displayMetrics.density + 0.25F)

/**---------------------------------------------all-----------------------------------------------*/

fun ImageView.load(content: Any) = Glide
        .with(this)
        .load(content)
        .into(this)

fun TextView.empty() {
    text = ""
}

fun TextView.text(resId: Int) {
    text = context.getString(resId)
}

fun TextView.text(resId: Int, vararg formatArgs: Any) {
    text = context.getString(resId, *formatArgs)
}

fun String.fromHtml(flag: Int = -1): Spanned = if (VERSION.SDK_INT >= VERSION_CODES.N) {
        Html.fromHtml(this, if (flag == -1) FROM_HTML_MODE_COMPACT else flag)
    } else {
        Html.fromHtml(this)
    }

fun Context.toast(resId: Int) = Toast
        .makeText(this, resId, Toast.LENGTH_LONG)
        .show()

fun Context.toast(text: String) = Toast
        .makeText(this, text, Toast.LENGTH_LONG)
        .show()

/**-------------------------------------------OTHERS----------------------------------------------*/

val Context.inputMethodManager get() = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

fun Context.intent(cls: Class<*>) = Intent(this, cls)

fun View.hideKeyboard() = context
        .inputMethodManager
        .hideSoftInputFromWindow(windowToken, 0)

fun Long.toCalendar(): Calendar = Calendar.getInstance().apply { timeInMillis = this@toCalendar }




